﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// A warning box that can be displayed in the editor window.
/// </summary>
public class MASUIWarningBox
{
    // The background texture of the warning box.
    private static Texture2D s_backgroud;

    // The text to display.
    private string m_text;

    /// <summary>
    /// Create a warning box.
    /// </summary>
    /// <param name="text">The text to display in the warning box.</param>
    public MASUIWarningBox(string text = "")
    {
        SetText(text);
    }

    /// <summary>
    /// Set the text to display in the warning box.
    /// </summary>
    /// <param name="text"></param>
    public void SetText(string text)
    {
        m_text = text;
    }

    /// <summary>
    /// Draw the warning box in the editor window..
    /// </summary>
    public void Draw()
    {
        if (!s_backgroud)
        {
            s_backgroud = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            s_backgroud.SetPixel(0, 0, new Color(0.85f, 0.85f, 0.85f));
            s_backgroud.Apply();
        }

        GUIStyle style = new GUIStyle(EditorStyles.label);
        style.normal.background = s_backgroud;        
        style.alignment = TextAnchor.MiddleLeft;
        style.normal.textColor = new Color(0.512f, 0.343f, 0.025f);
        style.margin = new RectOffset(20, 5, 5, 5);
        style.padding = new RectOffset(3, 3, 3, 3);
        style.wordWrap = true;
        style.fontStyle = FontStyle.Bold;

        GUIContent content = new GUIContent();
        content.image = MASResources.WarningIconTexture;
        content.text = m_text;

        GUILayout.Label(content, style);
    }
}