﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// A tooltip that can be displayed in the editor window.
/// </summary>
public class MASUITooltip
{
    /// <summary>
    /// Create a tooltip.
    /// </summary>
    /// <param name="text">The text to display in the tooltip.</param>
    public MASUITooltip(string text)
    {
        GUIStyle style = new GUIStyle(EditorStyles.label);
        style.margin = new RectOffset(20, 0, 0, 0);
        style.padding = new RectOffset(0, 0, 0, 0);
        style.border = new RectOffset(0, 0, 0, 0);
        style.fixedWidth = 16;
        GUIContent content = new GUIContent();
        content.tooltip = text;
        content.image = MASResources.QuestionMarkIconTexture;

        GUILayout.Label(content, style);
    }
}