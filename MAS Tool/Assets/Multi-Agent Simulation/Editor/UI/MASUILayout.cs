﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Provides several UI helper functions and UI elements with
/// pre-set styling and layout for the MAS Tool editor.
/// </summary>
public class MASUILayout
{
    /// <summary>
    /// Add vertical spacing between UI elements.
    /// </summary>
    /// <param name="lines">Number of spaces</param>
    public static void AddVerticalPadding(int lines = 1)
    {
        if (lines <= 0)
        {
            return;
        }

        for (int i = 0; i < lines; ++i)
        {
            EditorGUILayout.Separator();
        }
    }

    /// <summary>
    /// Add a label to the editor window.
    /// </summary>
    /// <param name="text">The text to display.</param>
    public static void Label(string text)
    {
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.padding.left = 20;
        labelStyle.padding.bottom = 3;
        labelStyle.normal.textColor = Color.white;
        labelStyle.wordWrap = true;

        GUILayout.Label(text, labelStyle);
    }

    /// <summary>
    /// Add a label to the editor window.
    /// </summary>
    /// <param name="content">The label conent.</param>
    public static void Label(GUIContent content)
    {
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.padding.left = 20;
        labelStyle.padding.bottom = 3;
        labelStyle.normal.textColor = Color.white; ;
        labelStyle.wordWrap = true;

        GUILayout.Label(content, labelStyle);
    }

    /// <summary>
    /// Add a toolbar button to the editor window.
    /// </summary>
    /// <param name="alignment">Alignment of the text in the toolbar button.</param>
    /// <returns>Toolbar button.</returns>
    public static GUIStyle ToolbarButton(TextAnchor alignment = TextAnchor.MiddleCenter)
    {
        GUIStyle toolbarButton = new GUIStyle(EditorStyles.toolbarButton);

        toolbarButton.normal.background = MASResources.TabButtonOnNormalTexture;
        toolbarButton.onNormal.background = MASResources.TabButtonTexture;
        toolbarButton.active.background = MASResources.TabButtonActiveTexture;
        toolbarButton.onActive.background = MASResources.TabButtonActiveTexture;
        toolbarButton.hover.background = MASResources.TabButtonHoverTexture;

        toolbarButton.normal.textColor = Color.white;
        toolbarButton.onNormal.textColor = Color.white;
        toolbarButton.active.textColor = Color.white;
        toolbarButton.onActive.textColor = Color.white;
        toolbarButton.hover.textColor = Color.white;
        toolbarButton.onHover.textColor = Color.white;

        toolbarButton.fontSize = 14;
        toolbarButton.fontStyle = FontStyle.Normal;
        toolbarButton.fixedHeight = 40;
        toolbarButton.alignment = alignment;

        if (alignment == TextAnchor.MiddleLeft)
        {
            toolbarButton.padding.left = 20;
        }

        return toolbarButton;
    }

    /// <summary>
    /// Supported button types that have custom icons.
    /// </summary>
    public enum ButtonType
    {
        None,
        Confirm,
        Critical,
        Remove,
        Save,
        Open,
        Add
    }

    /// <summary>
    /// Add a button to the editor window.
    /// </summary>
    /// <param name="text">The text to display on the button.</param>
    /// <param name="width">The width in pixels of the button.</param>
    /// <param name="height">The height in pixels of the button.</param>
    /// <param name="buttonType">An optional button type.</param>
    /// <returns>true if the button was clicked, otherwise false.</returns>
    public static bool Button(string text, float width, float height, ButtonType buttonType = ButtonType.None)
    {
        GUILayoutOption[] options = new GUILayoutOption[2];
        options[0] = GUILayout.Width(width);
        options[1] = GUILayout.Height(height);

        return Button(text, options, buttonType);
    }

    /// <summary>
    /// Add a button to the editor window.
    /// </summary>
    /// <param name="text">The text to display on the button.</param>
    /// <param name="options">Layout options for the button.</param>
    /// <param name="buttonType">An optional button type.</param>
    /// <returns>true if the button was clicked, otherwise false.</returns>
    public static bool Button(string text, GUILayoutOption[] options, ButtonType buttonType = ButtonType.None)
    {
        GUIContent content = new GUIContent();
        content.text = text;

        if (buttonType == ButtonType.Confirm)
        {
            content.image = MASResources.ConfirmIconTexture;
        }
        else if (buttonType == ButtonType.Critical)
        {
            content.image = MASResources.CriticalIconTexture;
        }
        else if (buttonType == ButtonType.Remove)
        {
            content.image = MASResources.RemoveIconTexture;
        }
        else if (buttonType == ButtonType.Save)
        {
            content.image = MASResources.SaveIconTexture;
        }
        else if (buttonType == ButtonType.Open)
        {
            content.image = MASResources.OpenIconTexture;
        }
        else if (buttonType == ButtonType.Add)
        {
            content.image = MASResources.AddIconTexture;
        }

        GUIStyle style = new GUIStyle();
        style.normal.background = MASResources.ButtonTexture;
        style.onNormal.background = MASResources.ButtonTexture;
        style.active.background = MASResources.ButtonActiveTexture;
        style.onActive.background = MASResources.ButtonActiveTexture;
        style.hover.background = MASResources.ButtonHoverTexture;
        style.onHover.background = MASResources.ButtonHoverTexture;
        style.normal.textColor = Color.white;
        style.onNormal.textColor = Color.white;
        style.active.textColor = Color.white;
        style.onActive.textColor = Color.white;
        style.hover.textColor = Color.white;
        style.onHover.textColor = Color.white;
        style.alignment = TextAnchor.MiddleCenter;
        style.fontSize = 12;
        style.margin = new RectOffset(20, 5, 0, 0);
        style.padding = new RectOffset(3, 3, 3, 3);

        return GUILayout.Button(content, style, options);
    }

    /// <summary>
    /// Add an image button to the editor window.
    /// </summary>
    /// <param name="image">Texture of image to display on the button.</param>
    /// <param name="width">The width in pixels of the button.</param>
    /// <param name="height">The height in pixels of the button.</param>
    /// <returns>true if the button was clicked, otherwise false.</returns>
    public static bool ImageButton(Texture2D image, float width, float height)
    {
        GUIContent content = new GUIContent();
        content.image = image;

        GUIStyle style = new GUIStyle();
        style.normal.background = MASResources.ButtonTexture;
        style.onNormal.background = MASResources.ButtonTexture;
        style.active.background = MASResources.ButtonActiveTexture;
        style.onActive.background = MASResources.ButtonActiveTexture;
        style.hover.background = MASResources.ButtonHoverTexture;
        style.onHover.background = MASResources.ButtonHoverTexture;
        style.normal.textColor = Color.white;
        style.onNormal.textColor = Color.white;
        style.active.textColor = Color.white;
        style.onActive.textColor = Color.white;
        style.hover.textColor = Color.white;
        style.onHover.textColor = Color.white;
        style.alignment = TextAnchor.MiddleCenter;
        style.fontSize = 12;
        style.margin = new RectOffset(5, 5, 0, 0);
        style.padding = new RectOffset(3, 3, 3, 3);

        GUILayoutOption[] options = new GUILayoutOption[2];
        options[0] = GUILayout.Width(width);
        options[1] = GUILayout.Height(height);

        return GUILayout.Button(content, style, options);
    }

    public static bool ListButton(string text, float width, float height, bool selected)
    {
        GUIContent content = new GUIContent();
        content.text = text;

        GUIStyle style = new GUIStyle();
        style.normal.background = selected ? MASResources.SectionFooterTexture : MASResources.ButtonTexture;
        style.onNormal.background = selected ? MASResources.SectionFooterTexture : MASResources.ButtonTexture;
        style.active.background = selected ? MASResources.SectionFooterTexture : MASResources.ButtonActiveTexture;
        style.onActive.background = selected ? MASResources.SectionFooterTexture : MASResources.ButtonActiveTexture;
        style.hover.background = selected ? MASResources.SectionFooterTexture : MASResources.ButtonHoverTexture;
        style.onHover.background = selected ? MASResources.SectionFooterTexture : MASResources.ButtonHoverTexture;
        style.normal.textColor = Color.white;
        style.onNormal.textColor = Color.white;
        style.active.textColor = Color.white;
        style.onActive.textColor = Color.white;
        style.hover.textColor = Color.white;
        style.onHover.textColor = Color.white;
        style.alignment = TextAnchor.MiddleLeft;
        style.fontSize = 12;
        style.margin = new RectOffset(5, 5, 3, 3);
        style.padding = new RectOffset(6, 3, 3, 3);

        GUILayoutOption[] options = new GUILayoutOption[2];
        options[0] = GUILayout.Width(width);
        options[1] = GUILayout.Height(height);

        return GUILayout.Button(content, style, options);
    }

    public static string TextField(string label, string text)
    {
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.fixedWidth = 150;
        labelStyle.padding.left = 20;
        labelStyle.padding.bottom = 3;
        labelStyle.normal.textColor = Color.white; ;
        GUILayoutOption[] labelOptions = new GUILayoutOption[1];
        labelOptions[0] = GUILayout.ExpandWidth(false);
        GUIStyle fieldStyle = new GUIStyle(EditorStyles.textField);
        fieldStyle.margin.right = 5;
        fieldStyle.fixedWidth = 200;
        fieldStyle.normal.textColor = new Color(0.047f, 0.176f, 0.278f);

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label(label, labelStyle, labelOptions);
        text = EditorGUILayout.TextField(text, fieldStyle);
        EditorGUILayout.EndHorizontal();

        return text;
    }

    /// <summary>
    /// Add a float field to the editor window.
    /// </summary>
    /// <param name="label">The text to display at the side of the float field.</param>
    /// <param name="value">The value to assign to the float field.</param>
    /// <returns>The current value of the float field.</returns>
    public static float FloatField(string label, float value)
    {
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.fixedWidth = 150;
        labelStyle.padding.left = 20;
        labelStyle.padding.bottom = 3;
        labelStyle.normal.textColor = Color.white; ;
        GUILayoutOption[] labelOptions = new GUILayoutOption[1];
        labelOptions[0] = GUILayout.ExpandWidth(false);
        GUIStyle fieldStyle = new GUIStyle(EditorStyles.textField);
        fieldStyle.margin.right = 5;
        fieldStyle.fixedWidth = 60;
        fieldStyle.normal.textColor = new Color(0.047f, 0.176f, 0.278f);

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label(label, labelStyle, labelOptions);        
        value = EditorGUILayout.FloatField("", value, fieldStyle);        
        EditorGUILayout.EndHorizontal();

        return value;
    }

    /// <summary>
    /// Add a Vector3 field to the editor window.
    /// </summary>
    /// <param name="label">The text to display at the side of the Vector3 field.</param>
    /// <param name="value">The value to assign to the Vector3 field.</param>
    /// <returns>The current value of the Vector3 field.</returns>
    public static Vector3 Vector3Field(string label, Vector3 value)
    {
        EditorStyles.label.normal.textColor = Color.white; ;

        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.fixedWidth = 150;
        labelStyle.padding.left = 20;
        labelStyle.padding.bottom = 3;
        labelStyle.normal.textColor = Color.white; ;
        GUILayoutOption[] labelOptions = new GUILayoutOption[1];
        labelOptions[0] = GUILayout.ExpandWidth(false);

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label(label, labelStyle, labelOptions);
        GUILayoutOption[] fieldOptions = new GUILayoutOption[1];
        fieldOptions[0] = GUILayout.MaxWidth(300);
        Vector3 val = EditorGUILayout.Vector3Field("", value, fieldOptions);
        EditorGUILayout.Space();
        EditorGUILayout.EndHorizontal();

        EditorStyles.label.normal.textColor = Color.black;

        return val;
    }

    /// <summary>
    /// Add a toggle button to the editor window.
    /// </summary>
    /// <param name="label">The text to display at the side of the toggle button.</param>
    /// <param name="value">true = on, false = off</param>
    /// <returns>true if on, false if off</returns>
    public static bool Toggle(string label, bool value)
    {
        EditorGUILayout.BeginHorizontal();
        GUIContent content = new GUIContent();
        content.text = "Loop Destinations";

        GUIStyle toggleStyle = new GUIStyle();
        toggleStyle.normal.background = MASResources.ToggleOffTexture;
        toggleStyle.onNormal.background = MASResources.ToggleOnTexture;
        toggleStyle.fixedWidth = 36;
        toggleStyle.fixedHeight = 18;
        toggleStyle.margin = new RectOffset(20, 0, 0, 0);

        value = GUILayout.Toggle(value, "", toggleStyle);
        
        GUIStyle labelStyle = new GUIStyle();
        labelStyle.normal.textColor = Color.white;
        labelStyle.margin = new RectOffset(5, 0, 0, 0);
        GUILayout.Label(label, labelStyle);
        EditorGUILayout.EndHorizontal();

        return value;
    }

    /// <summary>
    /// Add a slider to the editor window.
    /// </summary>
    /// <param name="label">The text to display at the side of the slider.</param>
    /// <param name="value">The value to assign to the slider.</param>
    /// <param name="leftValue">The min value of the slider.</param>
    /// <param name="rightValue">The max value of the slider.</param>
    /// <returns>The current value of the slider.</returns>
    public static float Slider(string label, float value, float leftValue, float rightValue)
    {
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.fixedWidth = 150;
        labelStyle.padding.left = 20;
        labelStyle.padding.bottom = 3;
        labelStyle.normal.textColor = Color.white;

        GUILayoutOption[] labelOptions = new GUILayoutOption[1];
        labelOptions[0] = GUILayout.ExpandWidth(false);

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label(label, labelStyle, labelOptions);

        GUIStyle sliderStyle = new GUIStyle();
        sliderStyle.normal.background = MASResources.SliderTexture;
        sliderStyle.fixedWidth = 235;
        sliderStyle.fixedHeight = 14;
        GUIStyle thumbStyle = new GUIStyle();
        thumbStyle.normal.background = MASResources.SliderThumbTexture;
        thumbStyle.fixedWidth = 14;
        sliderStyle.margin = new RectOffset(0, 0, 3, 3);

        EditorGUI.BeginChangeCheck();
        value = GUILayout.HorizontalSlider(value, leftValue, rightValue, sliderStyle, thumbStyle);
        if (EditorGUI.EndChangeCheck())
        {
            GUI.FocusControl(null);
        }        

        GUIStyle fieldStyle = new GUIStyle(EditorStyles.textField);
        fieldStyle.margin = new RectOffset(5, 0, 0, 0);
        fieldStyle.fixedWidth = 60;
        fieldStyle.stretchWidth = false;
        
        GUILayoutOption[] fieldOptions = new GUILayoutOption[1];
        fieldOptions[0] = GUILayout.MaxWidth(fieldStyle.fixedWidth);
        value = EditorGUILayout.FloatField("", value, fieldStyle, fieldOptions);
        EditorGUILayout.EndHorizontal();

        return value;
    }

    /// <summary>
    /// Add a enum popup to the editor window.
    /// </summary>
    /// <param name="label">The text to display at the side of the enum popup.</param>
    /// <param name="value">The value to assign to the enum popup.</param>
    /// <returns>The currently selected value of the enum.</returns>
    public static System.Enum EnumPopup(string label, System.Enum value)
    {
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.fixedWidth = 150;
        labelStyle.padding.left = 20;
        labelStyle.padding.bottom = 3;
        labelStyle.normal.textColor = Color.white;
        GUILayoutOption[] labelOptions = new GUILayoutOption[1];
        labelOptions[0] = GUILayout.ExpandWidth(false);

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label(label, labelStyle, labelOptions);
        GUIStyle popupStyle = new GUIStyle();
        popupStyle.normal.textColor = Color.white;
        popupStyle.normal.background = MASResources.EnumPopupTexture;
        popupStyle.fixedWidth = 200;
        popupStyle.padding = new RectOffset(5, 5, 1, 1);
        popupStyle.margin = new RectOffset(0, 0, 0, 0);
        value = EditorGUILayout.EnumPopup(value, popupStyle);
        EditorGUILayout.EndHorizontal();

        return value;
    }

    /// <summary>
    /// Add a gameObject field to the editor window.
    /// </summary>
    /// <param name="label">The text to display at the side of the gameObject field.</param>
    /// <param name="gameObject">The gameObject to assign to the gameObject field.</param>
    /// <param name="allowSceneObjects">Flag to determine if gameObject's from the scene are allowed
    /// to be placed in the gameObject field.</param>
    /// <returns>gameObject assigned to the gameObject field.</returns>
    public static GameObject GameObjectField(string label, GameObject gameObject, bool allowSceneObjects)
    {
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.fixedWidth = 150;
        labelStyle.padding.left = 20;
        labelStyle.padding.bottom = 3;
        labelStyle.normal.textColor = Color.white;
        GUILayoutOption[] labelOptions = new GUILayoutOption[1];
        labelOptions[0] = GUILayout.ExpandWidth(false);

        GUILayoutOption[] objectFieldOptions = new GUILayoutOption[1];
        objectFieldOptions[0] = GUILayout.Width(250);

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label(label, labelStyle, labelOptions);
        gameObject = EditorGUILayout.ObjectField(gameObject, typeof(GameObject), allowSceneObjects, objectFieldOptions) as GameObject;
        EditorGUILayout.EndHorizontal();

        return gameObject;
    }
}
