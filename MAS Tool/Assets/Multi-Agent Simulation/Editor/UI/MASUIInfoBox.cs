﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// An information box that can be displayed in an editor window.
/// </summary>
public class MASUIInfoBox
{
    // The background texture of the info box.
    private static Texture2D s_backgroud;

    // The text to display.
    private string m_text;

    /// <summary>
    /// Create an info box.
    /// </summary>
    /// <param name="text">Text to display.</param>
    public MASUIInfoBox(string text = "")
    {
        SetText(text);
    }

    /// <summary>
    /// Set the text to display in the info box.
    /// </summary>
    /// <param name="text"></param>
    public void SetText(string text)
    {
        m_text = text;
    }

    /// <summary>
    /// Draw the info box in the editow window.
    /// </summary>
    public void Draw()
    {
        if (!s_backgroud)
        {
            s_backgroud = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            s_backgroud.SetPixel(0, 0, new Color(0.85f, 0.85f, 0.85f));
            s_backgroud.Apply();
        }

        GUIStyle style = new GUIStyle(EditorStyles.label);
        style.normal.background = s_backgroud;
        style.fixedWidth = MASToolEditor.WindowWidth - 25;
        style.alignment = TextAnchor.MiddleLeft;
        style.normal.textColor = new Color(0.047f, 0.176f, 0.278f);
        style.margin = new RectOffset(20, 5, 5, 5);
        style.padding = new RectOffset(3, 3, 3, 3);
        style.wordWrap = true;

        GUIContent content = new GUIContent();
        content.image = MASResources.InfoIconTexture;
        content.text = m_text;

        GUILayout.Label(content, style);
    }
}