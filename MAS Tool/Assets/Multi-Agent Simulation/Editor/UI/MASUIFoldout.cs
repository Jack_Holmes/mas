﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// An animated folfout sections that can be used to toggle the display of content
/// in an editor window.
/// </summary>
public class MASUIFoldout
{
    // Flag to determine if the content should be displayed.
    UnityEditor.AnimatedValues.AnimBool m_showContent;

    bool m_isOpen = false;

    /// <summary>
    /// Create a foldout section
    /// </summary>
    /// <param name="show">Default display state of the foldout.</param>
    public MASUIFoldout(bool show=false)
    {
        m_showContent = new UnityEditor.AnimatedValues.AnimBool(show);
    }

    /// <summary>
    /// Begin the foldout section.
    /// </summary>
    /// <remarks>Any content in the foldout should be placed after the BeginGroup
    /// and before EndGroup.</remarks>
    /// <param name="groupTitle">The title to display at the top of the foldout.</param>
    /// <returns></returns>
    public bool BeginGroup(string groupTitle)
    {
        GUIStyle style = new GUIStyle(EditorStyles.foldout);
        GUIStyleState slate = new GUIStyleState();
        slate.background = MASResources.FoldoutTexture;
        slate.textColor = Color.white;
        GUIStyleState slate2 = new GUIStyleState();
        slate2.background = MASResources.FoldoutOpenTexture;
        slate2.textColor = Color.white;
        GUIStyleState slate3 = new GUIStyleState();
        slate3.background = MASResources.FoldoutActiveTexture;
        slate3.textColor = Color.white;

        GUIContent content = new GUIContent();

        if (m_showContent.target)
        {
            style.active = slate3;
            style.onActive = slate3;
            style.normal = slate2;
            style.onNormal = slate2;
            style.hover = slate2;
            style.onHover = slate2;
            style.focused = slate2;
            style.onFocused = slate2;

            content.image = MASResources.FoldoutArrowOpenTexture;
        }
        else
        {
            style.active = slate3;
            style.onActive = slate3;
            style.normal = slate;
            style.onNormal = slate;
            style.hover = slate;
            style.onHover = slate;
            style.focused = slate;
            style.onFocused = slate;

            content.image = MASResources.FoldoutArrowCloseTexture;
        }

        style.fixedWidth = MASToolEditor.WindowWidth;
        style.fixedHeight = 18;
        style.margin.bottom = 9;
        style.fontSize = 14;
        style.fontStyle = FontStyle.Bold;
        style.margin = new RectOffset(0, 0, 0, 8);

        content.text = groupTitle;

        m_showContent.target = EditorGUILayout.Foldout(m_showContent.target, content, true, style);       

        m_isOpen = EditorGUILayout.BeginFadeGroup(m_showContent.faded);
        return m_isOpen;
    }

    /// <summary>
    /// End the foldout group.
    /// </summary>
    public void EndGroup()
    {
        if (m_isOpen)
        {
            EditorGUILayout.Separator();
            EditorGUILayout.Separator();
        }     
        
        EditorGUILayout.EndFadeGroup();
    }
}