﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Reset obstacles editor window.
/// </summary>
public class MASResetObstacleEditor : EditorWindow
{
    // Flags which determine if the corresponding agent property will be reset.
    private bool m_resetAll = true;
    private bool m_resetPosition = false;
    private bool m_resetSize = false;

    private static MASObstaclesPanel.ObstaclePrimitiveType s_primitiveType;

    /// <summary>
    /// Display the editor window.
    /// </summary>
    public static void ShowWindow(MASObstaclesPanel.ObstaclePrimitiveType primitiveType)
    {
        string title = primitiveType == MASObstaclesPanel.ObstaclePrimitiveType.Block ? "Reset Block Obstacle Settings" : "Reset Cylinder Obstacle Settings";
        var window = GetWindowWithRect<MASResetObstacleEditor>(new Rect(0, 0, 300, 140), true, title);
        Extensions.CenterOnMainWin(window);
        s_primitiveType = primitiveType;
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, position.width, position.height), MASResources.BackgroundTexture);


        MASUILayout.AddVerticalPadding();
        m_resetAll = MASUILayout.Toggle("Reset All", m_resetAll);

        GUI.enabled = !m_resetAll;

        MASUILayout.AddVerticalPadding(2);
        m_resetPosition = MASUILayout.Toggle("Reset Position", m_resetPosition);

        MASUILayout.AddVerticalPadding();
        m_resetSize = MASUILayout.Toggle("Reset Size", m_resetSize);

        GUI.enabled = true;

        MASUILayout.AddVerticalPadding(4);
        EditorGUILayout.BeginHorizontal();
        if (MASUILayout.Button("Cancel", 100, 20))
        {
            Close();
        }

        if (MASUILayout.Button("Reset", 100, 20))
        {
            ResetSettings();
            Close();
        }
        EditorGUILayout.EndHorizontal();
    }
    /// <summary>
    /// Reset the obstacle settings.
    /// </summary>
    void ResetSettings()
    {
        MASToolEditor masTool = GetWindow<MASToolEditor>();
        if (!masTool)
        {
            return;
        }

        if (m_resetAll)
        {
            if (s_primitiveType == MASObstaclesPanel.ObstaclePrimitiveType.Block)
            {
                // Reset all properrties.
                MASBlockObstacleProxy blockProxy = new MASBlockObstacleProxy();

                // Update the block proxy.
                masTool.SetBlockObstacleProxy(blockProxy);
            }
            else if (s_primitiveType == MASObstaclesPanel.ObstaclePrimitiveType.Cyclinder)
            {
                // Reset all properrties.
                MASCylinderObstacleProxy cylinderProxy = new MASCylinderObstacleProxy();

                // Update the cylinder proxy.
                masTool.SetCylinderObstacleProxy(cylinderProxy);
            }
        }
        else
        {
            if (s_primitiveType == MASObstaclesPanel.ObstaclePrimitiveType.Block)
            {
                // Reset individual properties.
                MASBlockObstacleProxy blockProxy = masTool.GetBlockObstacleProxy();
                if (m_resetPosition)
                {
                    blockProxy.Position = new Vector3(0, 0, 0);
                }

                if (m_resetSize)
                {
                    blockProxy.Size = Vector3.one;
                }

                // Update the block proxy.
                masTool.SetBlockObstacleProxy(blockProxy);
            }
            else if (s_primitiveType == MASObstaclesPanel.ObstaclePrimitiveType.Cyclinder)
            {
                // Reset individual properties.
                MASCylinderObstacleProxy cylinderProxy = masTool.GetCylinderObstacleProxy();
                if (m_resetPosition)
                {
                    cylinderProxy.Position = new Vector3(0, 0, 0);
                }

                if (m_resetSize)
                {
                    cylinderProxy.Size = Vector3.one;
                }

                // Update the cylinder proxy.
                masTool.SetCylinderObstacleProxy(cylinderProxy);
            }
        }
    }
}