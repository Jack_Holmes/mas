﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Obstacles panel for MAS Tool editor.
/// </summary>
public class MASObstaclesPanel
{
    // Hold a proxies of the block and cylinder obstacles.
    MASBlockObstacleProxy m_blockObstacleProxy = new MASBlockObstacleProxy();
    MASCylinderObstacleProxy m_cylinderObstacleProxy = new MASCylinderObstacleProxy();

    // Foldout sections
    MASUIFoldout m_obstacleFoldout = new MASUIFoldout(true);

    /// <summary>
    /// Supported primitive types for obstacles.
    /// </summary>
    public enum ObstaclePrimitiveType
    {
        Block,
        Cyclinder
    }
    ObstaclePrimitiveType m_obstaclePrimitiveType = ObstaclePrimitiveType.Block;

    /// <summary>
    /// Update the obstacles panel.
    /// </summary>
    public void Update()
    {
        if (!Selection.activeObject)
        {
            return;
        }

        // Update the block obstacle proxy with the selected block obstacle
        MASBlockObstacle block = Selection.activeGameObject.GetComponent<MASBlockObstacle>();
        if (block)
        {
            m_blockObstacleProxy.Position = block.transform.position;
            m_blockObstacleProxy.Size = block.transform.localScale;
            m_blockObstacleProxy.NavMeshBake = block.NavMeshBake;
            m_obstaclePrimitiveType = ObstaclePrimitiveType.Block;

            if (block.transform.hasChanged && block.NavMeshBake)
            {
                block.transform.hasChanged = false;
                MASToolEditor.NavMeshDirty(true);
            }
        }
        else
        {
            // Update the cylinder obstacle proxy with the selected cylinder obstacle
            MASCylinderObstacle cylinder = Selection.activeGameObject.GetComponent<MASCylinderObstacle>();
            if (cylinder)
            {
                m_cylinderObstacleProxy.Position = cylinder.transform.position;
                m_cylinderObstacleProxy.Size = cylinder.transform.localScale;
                m_cylinderObstacleProxy.NavMeshBake = cylinder.NavMeshBake;
                m_obstaclePrimitiveType = ObstaclePrimitiveType.Cyclinder;

                if (cylinder.transform.hasChanged && cylinder.NavMeshBake)
                {                        
                    MASToolEditor.NavMeshDirty(true);
                }
            }
        }            
        
    }

    /// <summary>
    /// Draw the obstacles panel.
    /// </summary>
    /// <param name="parentWindow">Editor window this panel is a child of.</param>
    public void Draw(EditorWindow parentWindow)
    {
        if (m_obstacleFoldout.BeginGroup(" Obstacle Settings"))
        {
            MASUILayout.AddVerticalPadding();

            ObstaclePrimitiveType prevPrimitiveType = m_obstaclePrimitiveType;

            EditorGUI.BeginChangeCheck();
            m_obstaclePrimitiveType = (ObstaclePrimitiveType)MASUILayout.EnumPopup("Primitive Type", m_obstaclePrimitiveType);
            if (EditorGUI.EndChangeCheck())
            {
                if (Selection.activeGameObject)
                {
                    if (prevPrimitiveType == ObstaclePrimitiveType.Block)
                    {
                        // Get the block component to verify that we have block obstacle selected
                        MASBlockObstacle block = Selection.activeGameObject.GetComponent<MASBlockObstacle>();
                        if (block)
                        {
                            MASController MAS = Object.FindObjectOfType<MASController>();
                            if (MAS)
                            {
                                m_cylinderObstacleProxy.Position = m_blockObstacleProxy.Position;
                                m_cylinderObstacleProxy.Size = m_blockObstacleProxy.Size;
                                m_cylinderObstacleProxy.NavMeshBake = m_blockObstacleProxy.NavMeshBake;

                                MAS.AddCylinderObstacle(
                                    m_cylinderObstacleProxy.Position,
                                    m_cylinderObstacleProxy.Size,
                                    m_cylinderObstacleProxy.NavMeshBake);

                                Object.DestroyImmediate(block.gameObject);
                                MASToolEditor.NavMeshDirty(true);
                            }
                        }
                    }
                    else if (prevPrimitiveType == ObstaclePrimitiveType.Cyclinder)
                    {
                        // Get the cylinder component to verify that we have block obstacle selected
                        MASCylinderObstacle cylinder = Selection.activeGameObject.GetComponent<MASCylinderObstacle>();
                        if (cylinder)
                        {
                            MASController MAS = Object.FindObjectOfType<MASController>();
                            if (MAS)
                            {
                                m_blockObstacleProxy.Position = m_cylinderObstacleProxy.Position;
                                m_blockObstacleProxy.Size = m_cylinderObstacleProxy.Size;
                                m_blockObstacleProxy.NavMeshBake = m_cylinderObstacleProxy.NavMeshBake;

                                MAS.AddBlockObstacle(
                                    m_blockObstacleProxy.Position,
                                    m_blockObstacleProxy.Size,
                                    m_blockObstacleProxy.NavMeshBake);

                                Object.DestroyImmediate(cylinder.gameObject);
                                MASToolEditor.NavMeshDirty(true);
                            }
                        }
                    }
                }
            }

            if (m_obstaclePrimitiveType == ObstaclePrimitiveType.Block)
            {
                DrawBlockObstacleTab(parentWindow);
            }
            else if (m_obstaclePrimitiveType == ObstaclePrimitiveType.Cyclinder)
            {
                DrawCylinderObstacleTab(parentWindow);
            }
        }
        m_obstacleFoldout.EndGroup();

        if (MASUILayout.Button("Bake Environment", 180, 30))
        {
            UnityEditor.AI.NavMeshBuilder.BuildNavMesh();
            MASToolEditor.NavMeshDirty(false);
        }
    }

    /// <summary>
    /// Invoked by the MAS Tool editor when the user changes the selection in the scene view.
    /// </summary>
    public void OnSelectionChange()
    {
        if (!Selection.activeGameObject)
        {
            return;
        }

        // Keep the correct primitive type selected if we change the obstacle selection in the scene view.        
        MASBlockObstacle block = Selection.activeGameObject.GetComponent<MASBlockObstacle>();
        if (block)
        {
            m_obstaclePrimitiveType = ObstaclePrimitiveType.Block;

            return;
        }

        MASCylinderObstacle cylinder = Selection.activeGameObject.GetComponent<MASCylinderObstacle>();
        if (cylinder)
        {
            m_obstaclePrimitiveType = ObstaclePrimitiveType.Cyclinder;

            return;
        }        
    }

    /// <summary>
    /// Draw the obstacles tab for a block obstacle.
    /// </summary>
    /// <param name="parentWindow">Editor window this panel is a child of.</param>
    private void DrawBlockObstacleTab(EditorWindow parentWindow)
    {
        MASUILayout.AddVerticalPadding();
        // Block obstacle position
        EditorGUI.BeginChangeCheck();
        m_blockObstacleProxy.Position = MASUILayout.Vector3Field("Position", m_blockObstacleProxy.Position);
        if (EditorGUI.EndChangeCheck())
        {
            UpdateSelectedBlock();
        }

        // Block obstacle size
        MASUILayout.AddVerticalPadding();
        EditorGUI.BeginChangeCheck();
        m_blockObstacleProxy.Size = MASUILayout.Vector3Field("Size", m_blockObstacleProxy.Size);
        if (EditorGUI.EndChangeCheck())
        {
            UpdateSelectedBlock();
        }

        MASUILayout.AddVerticalPadding(2);
        EditorGUI.BeginChangeCheck();
        m_blockObstacleProxy.NavMeshBake = MASUILayout.Toggle("Include in NavMesh Bake", m_blockObstacleProxy.NavMeshBake);
        if (EditorGUI.EndChangeCheck())
        {
            UpdateSelectedBlock();
        }

        GUIStyle blockObstacleFooterArea = new GUIStyle();
        blockObstacleFooterArea.normal.background = MASResources.SectionFooterTexture;
        GUILayout.BeginArea(new Rect(0, parentWindow.position.height - 80, MASToolEditor.WindowWidth, 40), blockObstacleFooterArea);

        // Add block obstacle
        MASUILayout.AddVerticalPadding();
        EditorGUILayout.BeginHorizontal();
        if (MASUILayout.Button(" Add New Block Obstacle", 180, 30, MASUILayout.ButtonType.Confirm))
        {
            AddNewBlock();
        }

        // Reset block settings        
        EditorGUILayout.Space();
        if (MASUILayout.Button(" Reset Block Settings", 160, 30, MASUILayout.ButtonType.Critical))
        {
            MASResetObstacleEditor.ShowWindow(m_obstaclePrimitiveType);
        }
        EditorGUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    /// <summary>
    /// Draw the obstacles tab for a cylinder obstacle.
    /// </summary>
    /// <param name="parentWindow">Editor window this panel is a child of.</param>
    private void DrawCylinderObstacleTab(EditorWindow parentWindow)
    {
        MASUILayout.AddVerticalPadding();
        // Cylinder obstacle position
        EditorGUI.BeginChangeCheck();
        m_cylinderObstacleProxy.Position = MASUILayout.Vector3Field("Position", m_cylinderObstacleProxy.Position);
        if (EditorGUI.EndChangeCheck())
        {
            UpdateSelectedCylinder();
        }

        // Cylinder obstacle size
        MASUILayout.AddVerticalPadding();
        EditorGUI.BeginChangeCheck();
        m_cylinderObstacleProxy.Size = MASUILayout.Vector3Field("Size", m_cylinderObstacleProxy.Size);
        if (EditorGUI.EndChangeCheck())
        {
            UpdateSelectedCylinder();
        }

        // NavMesh bake
        MASUILayout.AddVerticalPadding(2);
        EditorGUI.BeginChangeCheck();
        m_cylinderObstacleProxy.NavMeshBake = MASUILayout.Toggle("Include in NavMesh Bake", m_cylinderObstacleProxy.NavMeshBake);
        if (EditorGUI.EndChangeCheck())
        {
            UpdateSelectedCylinder();
        }        

        GUIStyle cylinderObstacleFooterArea = new GUIStyle();
        cylinderObstacleFooterArea.normal.background = MASResources.SectionFooterTexture;
        GUILayout.BeginArea(new Rect(0, parentWindow.position.height - 80, MASToolEditor.WindowWidth, 40), cylinderObstacleFooterArea);

        // Add cylinder obstacle
        MASUILayout.AddVerticalPadding();
        EditorGUILayout.BeginHorizontal();
        if (MASUILayout.Button(" Add New Cylinder Obstacle", 200, 30, MASUILayout.ButtonType.Confirm))
        {
            AddNewCylinder();
        }

        // Reset cylinder settings        
        EditorGUILayout.Space();
        if (MASUILayout.Button(" Reset Cylinder Settings", 170, 30, MASUILayout.ButtonType.Critical))
        {
            MASResetObstacleEditor.ShowWindow(m_obstaclePrimitiveType);
        }
        EditorGUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    /// <summary>
    /// Add a new block obstacle to the scene using the current obstacle panel properties.
    /// </summary>
    private void AddNewBlock()
    {
        MASController masController = Object.FindObjectOfType<MASController>();
        if (!masController)
        {
            return;
        }
        
        masController.AddBlockObstacle(
            m_blockObstacleProxy.Position,
            m_blockObstacleProxy.Size,
            m_blockObstacleProxy.NavMeshBake);

        if (m_blockObstacleProxy.NavMeshBake)
        {
            MASToolEditor.NavMeshDirty(true);
        }        
    }

    /// <summary>
    /// Add a new cylinder obstacle to the scene using the current obstacle panel properties.
    /// </summary>
    private void AddNewCylinder()
    {
        MASController masController = Object.FindObjectOfType<MASController>();
        if (!masController)
        {
            return;
        }

        masController.AddCylinderObstacle(
            m_cylinderObstacleProxy.Position,
            m_cylinderObstacleProxy.Size,
            m_cylinderObstacleProxy.NavMeshBake);

        if (m_cylinderObstacleProxy.NavMeshBake)
        {
            MASToolEditor.NavMeshDirty(true);
        }    
    }

    /// <summary>
    /// Update the currently selected block obstacle in the scene view with the proxy.
    /// </summary>
    private void UpdateSelectedBlock()
    {
        if (!Selection.activeGameObject)
        {
            return;
        }
        
        MASBlockObstacle block = Selection.activeGameObject.GetComponent<MASBlockObstacle>();
        if (!block)
        {
            return;            
        }

        block.transform.position = m_blockObstacleProxy.Position;
        block.transform.localScale = m_blockObstacleProxy.Size;
        block.NavMeshBake = m_blockObstacleProxy.NavMeshBake;
        MASToolEditor.NavMeshDirty(true);
    }

    /// <summary>
    /// Update the currently selected cylinder obstacle in the scene view with the proxy.
    /// </summary>
    private void UpdateSelectedCylinder()
    {
        if (!Selection.activeGameObject)
        {
            return;
        }

        MASCylinderObstacle cylinder = Selection.activeGameObject.GetComponent<MASCylinderObstacle>();
        if (!cylinder)
        {
            return;
        }

        cylinder.transform.position = m_cylinderObstacleProxy.Position;
        cylinder.transform.localScale = m_cylinderObstacleProxy.Size;
        cylinder.NavMeshBake = m_cylinderObstacleProxy.NavMeshBake;
        MASToolEditor.NavMeshDirty(true);
    }

    /// <summary>
    /// Update the current block obstacle proxy.
    /// </summary>
    /// <param name="blockProxy">The new block obstacle proxy.</param>
    public void SetBlockObstacleProxy(MASBlockObstacleProxy blockProxy)
    {
        m_blockObstacleProxy = blockProxy;
        UpdateSelectedBlock();
    }

    /// <summary>
    /// Get the current block obstacle proxy.
    /// </summary>
    /// <returns>Block obstacle proxy.</returns>
    public MASBlockObstacleProxy GetBlockObstacleProxy()
    {
        return m_blockObstacleProxy;
    }

    /// <summary>
    /// Update the current cylinder obstacle proxy.
    /// </summary>
    /// <param name="cylinderProxy">The new cylinder obstacle proxy.</param>
    public void SetCylinderObstacleProxy(MASCylinderObstacleProxy cylinderProxy)
    {
        m_cylinderObstacleProxy = cylinderProxy;
        UpdateSelectedCylinder();
    }

    /// <summary>
    /// Get the current cylinder obstacle proxy.
    /// </summary>
    /// <returns>Cylinder obstacle proxy.</returns>
    public MASCylinderObstacleProxy GetCylinderObstacleProxy()
    {
        return m_cylinderObstacleProxy;
    }
}