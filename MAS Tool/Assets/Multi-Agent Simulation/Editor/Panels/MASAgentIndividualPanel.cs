﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Agent panel for MAS Tool editor.
/// </summary>
public class MASAgentIndividualPanel
{
    // Hold a proxy of the agent representing the current editor properties
    // of the agent.
    MASAgentProxy m_agentProxy = new MASAgentProxy();

    // Foldout sections
    MASUIFoldout m_agentSettingsFoldout = new MASUIFoldout(true);
    MASUIFoldout m_agentDestinationsFoldout = new MASUIFoldout(true);
    MASUIFoldout m_agentAdvanceFoldout = new MASUIFoldout();

    // Scrollable-regions scroll positions
    Vector2 m_agentScrollPos = new Vector2();
    Vector2 m_goalPosScrollPos = new Vector2();

    /// <summary>
    /// Draw the agent panel.
    /// </summary>
    /// <param name="parentWindow">Editor window this panel is a child of.</param>
    public void Draw(EditorWindow parentWindow)
    {
        m_agentScrollPos = EditorGUILayout.BeginScrollView(m_agentScrollPos, GUILayout.Width(MASToolEditor.WindowWidth), GUILayout.Height(parentWindow.position.height - 190));

        MASUILayout.AddVerticalPadding(1);
        if (MASUILayout.Button(" Load Agent Preset", 150, 30, MASUILayout.ButtonType.Open))
        {
            MASLoadAgentPresetEditor.ShowWindow(this);
        }

        MASUILayout.AddVerticalPadding(1);
        if (m_agentSettingsFoldout.BeginGroup(" Agent Settings"))
        {
            // Agent position
            MASUILayout.AddVerticalPadding(2);
            EditorGUI.BeginChangeCheck();
            m_agentProxy.Position = MASUILayout.Vector3Field("Position", m_agentProxy.Position);
            if (EditorGUI.EndChangeCheck())
            {
                UpdateSelectedAgent();
            }

            // Agent radius
            MASUILayout.AddVerticalPadding(3);
            EditorGUI.BeginChangeCheck();
            m_agentProxy.Radius = MASUILayout.Slider("Radius", m_agentProxy.Radius, 0.1f, 10.0f);
            if (EditorGUI.EndChangeCheck())
            {
                UpdateSelectedAgent();
            }

            // Agent max speed
            EditorGUI.BeginChangeCheck();
            m_agentProxy.MaxSpeed = MASUILayout.Slider("Max speed", m_agentProxy.MaxSpeed, 0.0f, 10.0f);
            if (EditorGUI.EndChangeCheck())
            {
                UpdateSelectedAgent();
            }

            if (m_agentProxy.MaxSpeed == 0)
            {
                MASUIWarningBox speedWarn = new MASUIWarningBox(" The agent will not move with max a speed of zero");
                speedWarn.Draw();
            }

            EditorGUI.BeginChangeCheck();
            GameObject characterPrefab = MASUILayout.GameObjectField("Character", m_agentProxy.CharacterPrefab, false);
            if (Selection.activeGameObject)
            {
                MASAgent agent = Selection.activeGameObject.GetComponent<MASAgent>();
                if (characterPrefab != null)
                {
                    m_agentProxy.CharacterPrefab = characterPrefab;
                    if (EditorGUI.EndChangeCheck())
                    {
                        if (agent)
                        {
                            agent.CharacterPrefab = m_agentProxy.CharacterPrefab;
                        }
                    }
                }
            }

            m_agentProxy.ShowAdvancedSettings = m_agentAdvanceFoldout.BeginGroup(" Advanced Settings");
            if (m_agentProxy.ShowAdvancedSettings)
            {
                MASUILayout.AddVerticalPadding(2);
                // Agent neighbour distance            
                EditorGUILayout.BeginHorizontal();
                new MASUITooltip("The maximum distance to other agents this agent takes into account in the navigation. The larger this number, the longer the running time of the simulation. If the number is too low, the simulation will not be safe.");
                EditorGUI.BeginChangeCheck();
                m_agentProxy.NeighborDist = MASUILayout.Slider("Neighbour Distance", m_agentProxy.NeighborDist, 0, 50);
                if (EditorGUI.EndChangeCheck())
                {
                    UpdateSelectedAgent();
                }
                EditorGUILayout.EndHorizontal();

                // Agent max neighbours
                EditorGUILayout.BeginHorizontal();
                new MASUITooltip("The maximum number of other agents this agent takes into account in the navigation. The larger this number, the longer the running time of the simulation. If the number is too low, the simulation will not be safe.");
                EditorGUI.BeginChangeCheck();
                m_agentProxy.MaxNeighbors = (int)MASUILayout.Slider("Max Neighbours", m_agentProxy.MaxNeighbors, 0, 50);
                if (EditorGUI.EndChangeCheck())
                {
                    UpdateSelectedAgent();
                }
                EditorGUILayout.EndHorizontal();

                // Agent time horizon
                EditorGUILayout.BeginHorizontal();
                new MASUITooltip("The minimal amount of time for which this agent's velocities that are computed by the simulation are safe with respect to other agents. The larger this number, the sooner this agent will respond to the presence of other agents, but the less freedom this agent has in choosing its velocities.");
                EditorGUI.BeginChangeCheck();
                m_agentProxy.TimeHorizon = MASUILayout.Slider("Time Horizon", m_agentProxy.TimeHorizon, 1, 100);
                if (EditorGUI.EndChangeCheck())
                {
                    UpdateSelectedAgent();
                }
                EditorGUILayout.EndHorizontal();

                // Agent time horizon obstacles
                EditorGUILayout.BeginHorizontal();
                new MASUITooltip("The minimal amount of time for which this agent's velocities that are computed by the simulation are safe with respect to obstacles. The larger this number, the sooner this agent will respond to the presence of obstacles, but the less freedom this agent has in choosing its velocities.");
                EditorGUI.BeginChangeCheck();
                m_agentProxy.TimeHorizonObst = MASUILayout.Slider("Obstacle Time Horizon", m_agentProxy.TimeHorizonObst, 1, 100);
                if (EditorGUI.EndChangeCheck())
                {
                    UpdateSelectedAgent();
                }
                EditorGUILayout.EndHorizontal();
            }
            m_agentAdvanceFoldout.EndGroup();
        }
        m_agentSettingsFoldout.EndGroup();

        
        if (m_agentDestinationsFoldout.BeginGroup(" Destinations"))
        {
            MASUILayout.AddVerticalPadding();
            MASUIInfoBox inf = new MASUIInfoBox("An agent will travel to its assigned destinations during the simulation");
            inf.Draw();

            MASUILayout.AddVerticalPadding();
            // Add/Remove goal positions buttons        
            EditorGUILayout.BeginHorizontal();
            if (MASUILayout.Button(" Add Destination", 130, 20, MASUILayout.ButtonType.Add))
            {
                AddGoalPosition();
            }
            EditorGUILayout.EndHorizontal();
            MASUILayout.AddVerticalPadding();

            GUIStyle destAreaStyle = new GUIStyle();
            destAreaStyle.normal.background = MASResources.ScrollViewTexture;
            destAreaStyle.margin = new RectOffset(0, 0, 0, 0);
            destAreaStyle.margin = new RectOffset(20, 5, 5, 5);
            destAreaStyle.padding = new RectOffset(5, 5, 5, 5);

            m_goalPosScrollPos = EditorGUILayout.BeginScrollView(m_goalPosScrollPos, destAreaStyle, GUILayout.Width(MASToolEditor.WindowWidth - 40), GUILayout.Height(95));

            bool isCrowdAgent = false;
            if (Selection.activeGameObject)
            {
                MASAgent agent = Selection.activeGameObject.GetComponent<MASAgent>();
                if (agent)
                {
                    MASCrowd crowd = agent.GetComponentInParent<MASCrowd>();
                    if (crowd)
                    {
                        isCrowdAgent = true;
                    }
                }
            }

            // Agent goal positions
            for (int i = 0; i < m_agentProxy.GoalPositions.Count; ++i)
            {
                EditorGUILayout.BeginHorizontal();
                GUIStyle style = new GUIStyle();
                style.fixedWidth = 16;
                style.fixedHeight = 16;
                GUILayout.Label(MASResources.RoadmapIconTexture, style);

                EditorGUI.BeginChangeCheck();
                m_agentProxy.GoalPositions[i] = MASUILayout.Vector3Field("Destination " + (i + 1), m_agentProxy.GoalPositions[i]);
                if (EditorGUI.EndChangeCheck())
                {
                    UpdateSelectedAgent();
                }

                if (!isCrowdAgent)
                {
                    if (MASUILayout.Button("", 22, 22, MASUILayout.ButtonType.Remove))
                    {
                        RemoveGoalPosition(i);
                    }
                }

                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndScrollView();

            // Loop Goal Positions            
            EditorGUI.BeginChangeCheck();
            m_agentProxy.LoopGoalPositions = MASUILayout.Toggle("Loop Destinations", m_agentProxy.LoopGoalPositions);
            if (EditorGUI.EndChangeCheck())
            {
                UpdateSelectedAgent();
            }
        }
        m_agentDestinationsFoldout.EndGroup();

        EditorGUILayout.EndScrollView();

        GUIStyle agentFooterArea = new GUIStyle();
        agentFooterArea.normal.background = MASResources.SectionFooterTexture;
        GUILayout.BeginArea(new Rect(0, parentWindow.position.height - 80, MASToolEditor.WindowWidth, 40), agentFooterArea);

        // Add agent
        MASUILayout.AddVerticalPadding();
        EditorGUILayout.BeginHorizontal();

        
        if (MASUILayout.Button(" Add New Agent", 150, 30, MASUILayout.ButtonType.Confirm))
        {
            AddNewAgent();
        }

        
        // Save preset
        if (MASUILayout.Button(" Save As Preset", 140, 30, MASUILayout.ButtonType.Save))
        {
            MASSaveAgentPresetEditor.ShowWindow(m_agentProxy);                      
        }

        //Reset Agent
        EditorGUILayout.Space();
        if (MASUILayout.Button(" Reset Agent", 130, 30, MASUILayout.ButtonType.Critical))
        {
            MASResetAgentEditor.ShowWindow();
        }
        EditorGUILayout.EndHorizontal();

        GUILayout.EndArea();        
    }

    /// <summary>
    /// Update the agent panel.
    /// </summary>
    public void Update()
    {
        if (Selection.activeGameObject)
        {
            // Update the agent proxy with the selected agent
            MASAgent agent = Selection.activeGameObject.GetComponent<MASAgent>();
            if (agent)
            {
                m_agentProxy.Position = agent.transform.localPosition;

                m_agentProxy.GoalPositions.Clear();
                foreach (MASGoalPositionHandle handle in agent.GetComponentsInChildren<MASGoalPositionHandle>())
                {
                    m_agentProxy.GoalPositions.Add(handle.GoalPosition);
                }
               
                m_agentProxy.Radius = agent.Radius;
                m_agentProxy.MaxSpeed = agent.MaxSpeed;
                m_agentProxy.NeighborDist = agent.NeighborDist;
                m_agentProxy.MaxNeighbors = agent.MaxNeighbors;
                m_agentProxy.TimeHorizon = agent.TimeHorizon;
                m_agentProxy.TimeHorizonObst = agent.TimeHorizonObst;
                m_agentProxy.LoopGoalPositions = agent.LoopGoalPositions;
                m_agentProxy.CharacterPrefab = agent.CharacterPrefab;                

                return;
            }
        }        
    }

    /// <summary>
    /// Update the agent proxy.
    /// </summary>
    /// <param name="proxy">The new agent proxy.</param>
    public void SetAgentProxy(MASAgentProxy proxy)
    {
        m_agentProxy = proxy;
        UpdateSelectedAgent();
    }

    /// <summary>
    /// Update the agent proxy using an agent preset.
    /// </summary>
    /// <param name="proxy">The new agent proxy.</param>
    public void SetAgentProxyFromPreset(MASAgentProxy proxy)
    {
        m_agentProxy.Radius          = proxy.Radius;
        m_agentProxy.MaxSpeed        = proxy.MaxSpeed;
        m_agentProxy.CharacterPrefab = proxy.CharacterPrefab;        
        m_agentProxy.NeighborDist    = proxy.NeighborDist;
        m_agentProxy.MaxNeighbors    = proxy.MaxNeighbors;
        m_agentProxy.TimeHorizon     = proxy.TimeHorizon;
        m_agentProxy.TimeHorizonObst = proxy.TimeHorizonObst;

        UpdateSelectedAgent();
    }

    /// <summary>
    /// Get the current agent proxy.
    /// </summary>
    /// <returns>Agent proxy.</returns>
    public MASAgentProxy GetAgentProxy()
    {
        return m_agentProxy;
    }

    /// <summary>
    /// Add a new agent to the simulation from the agent panel.
    /// </summary>
    private void AddNewAgent()
    {
        MASController MAS = Object.FindObjectOfType<MASController>();
        if (MAS)
        {
            GameObject obj = MAS.AddAgent(m_agentProxy);
            if (obj == null)
            {
                return;
            }            

            MASAgent agent = obj.GetComponent<MASAgent>();
            if (!agent)
            {
                return;
            }

            if (m_agentProxy.CharacterPrefab == null)
            {
                return;
            }               
                
            agent.CharacterPrefab = m_agentProxy.CharacterPrefab;
        }
    }

    /// <summary>
    /// Update the agent that is selected in the scene view, with the settings in the editor window.
    /// </summary>
    private void UpdateSelectedAgent()
    {
        if (Selection.activeGameObject)
        {
            MASAgent agent = Selection.activeGameObject.GetComponent<MASAgent>();
            if (agent)
            {
                agent.transform.localPosition = m_agentProxy.Position;

                agent.RecalculateGoalPositions(m_agentProxy.GoalPositions);                
                
                agent.Radius = m_agentProxy.Radius;
                agent.MaxSpeed = m_agentProxy.MaxSpeed;
                agent.NeighborDist = m_agentProxy.NeighborDist;
                agent.MaxNeighbors = m_agentProxy.MaxNeighbors;
                agent.TimeHorizon = m_agentProxy.TimeHorizon;
                agent.TimeHorizonObst = m_agentProxy.TimeHorizonObst;
                agent.LoopGoalPositions = m_agentProxy.LoopGoalPositions;
                agent.CharacterPrefab = m_agentProxy.CharacterPrefab;
            }
        }
    }

    /// <summary>
    /// Add a new agent destination.
    /// </summary>
    private void AddGoalPosition()
    {
        Vector3 goalPos = m_agentProxy.Position;
        if (m_agentProxy.GoalPositions.Count > 0)
        {
            goalPos = m_agentProxy.GoalPositions[m_agentProxy.GoalPositions.Count - 1];            
        }

        goalPos.x += m_agentProxy.Radius * 2;

        m_agentProxy.GoalPositions.Add(goalPos);
        UpdateSelectedAgent();
    }

    /// <summary>
    /// Remove an agent destination from the list of current destinations.
    /// </summary>
    /// <param name="pos">Destination index.</param>
    private void RemoveGoalPosition(int pos)
    {
        if (m_agentProxy.GoalPositions.Count > 0 && pos < m_agentProxy.GoalPositions.Count)
        {
            m_agentProxy.GoalPositions.RemoveAt(pos);
            UpdateSelectedAgent();
        }
    }
}