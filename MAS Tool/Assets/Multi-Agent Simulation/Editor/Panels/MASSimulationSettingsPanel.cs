﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Simualtion settings panel for the MAS Tool editor.
/// </summary>
public class MASSimulationSettingsPanel
{
    // The rate of the simulation.
    float m_simulationTimeStep = MASSimulationSettings.TimeStepDefault;

    // Foldout sections
    MASUIFoldout m_simFoldout = new MASUIFoldout(true);
    MASUIFoldout m_simActionsFoldout = new MASUIFoldout(true);

    /// <summary>
    /// Draw the simulation settings panel.
    /// </summary>
    public void Draw()
    {
        if (m_simFoldout.BeginGroup(" Simulation Settings"))
        {
            // Simulation timestep
            MASUILayout.AddVerticalPadding(2);
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            m_simulationTimeStep = MASUILayout.FloatField("Time Step", m_simulationTimeStep);
            if (EditorGUI.EndChangeCheck())
            {
                if (m_simulationTimeStep < 0)
                {
                    m_simulationTimeStep = 0;
                }

                PlayerPrefs.SetFloat("MAS_TimeStep", m_simulationTimeStep);
            }

            if (MASUILayout.Button("Reset Time Step", 120, 25))
            {
                m_simulationTimeStep = MASSimulationSettings.TimeStepDefault;
                PlayerPrefs.SetFloat("MAS_TimeStep", m_simulationTimeStep);
            }
            EditorGUILayout.EndHorizontal();

            if (m_simulationTimeStep == 0)
            {
                MASUIWarningBox warning = new MASUIWarningBox("Agents will not be able to move will a timestep of 0.");
                warning.Draw();
            }

            MASUIInfoBox timeStepInfo = new MASUIInfoBox("Alter the rate of the simulation");
            timeStepInfo.Draw();
        }
        m_simFoldout.EndGroup();

        if (m_simActionsFoldout.BeginGroup(" Actions"))
        {
            // Destroy simulation
            MASUILayout.AddVerticalPadding(2);
            EditorGUILayout.BeginHorizontal();
            if (MASUILayout.Button(" Destroy Simulation", 150, 25, MASUILayout.ButtonType.Critical))
            {                
                MASRemoveControllerEditor.ShowWindow();
            }
            EditorGUILayout.EndHorizontal();
        }
        m_simActionsFoldout.EndGroup();
    }
}