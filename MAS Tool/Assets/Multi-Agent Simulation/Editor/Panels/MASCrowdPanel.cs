﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Agent crowd panel for MAS Tool editor.
/// </summary>
public class MASCrowdPanel
{
    // Hold a proxy of the crowd representing the current editor
    // properties of the crowd.
    MASCrowdProxy m_crowdProxy = new MASCrowdProxy();

    // Foldout sections    
    MASUIFoldout m_crowdSettingsFoldout = new MASUIFoldout(true);
    MASUIFoldout m_agentPresetsFoldout = new MASUIFoldout(true);
    MASUIFoldout m_crowdDestinationsFoldout = new MASUIFoldout(true);    
    
    // Scrollable-regions scroll positions
    Vector2 m_crowdScrollPos = new Vector2();
    Vector2 m_agentPresetsScollPos = new Vector2();
    Vector2 m_crowdGoalPosScrollPos = new Vector2(); 

    /// <summary>
    /// Draw the crowd panel.
    /// </summary>
    /// <param name="parentWindow">Editor window this panel is a child of.</param>
    public void Draw(EditorWindow parentWindow)
    {       
        m_crowdScrollPos = EditorGUILayout.BeginScrollView(m_crowdScrollPos, GUILayout.Width(MASToolEditor.WindowWidth), GUILayout.Height(parentWindow.position.height - 190));
        DrawCrowdSettings();
        DrawAgentPresets();
        DrawDestinations();
        DrawFooter(parentWindow);
    }

    /// <summary>
    /// Add a new agent proxy to the corwd presets list.
    /// </summary>
    /// <param name="agentProxy">Agent proxy to add.</param>
    public void AddAgentProxy(MASAgentProxy agentProxy)
    {
        foreach (MASAgentProxy proxy in m_crowdProxy.AgentProxies)
        {
            if (proxy.PresetName == agentProxy.PresetName)
            {
                // Agent preset already part of crowd
                return;
            }
        }

        m_crowdProxy.AgentProxies.Add(agentProxy);
        UpdateSelectedCrowd();
    }

    /// <summary>
    /// Remove an agent proxy from the crowd agent presets list.
    /// </summary>
    /// <param name="pos">Index to remove at.</param>
    private void RemovedAgentProxy(int pos)
    {
        if (m_crowdProxy.AgentProxies.Count > 0 && pos < m_crowdProxy.AgentProxies.Count)
        {
            m_crowdProxy.AgentProxies.RemoveAt(pos);
            UpdateSelectedCrowd();
        }
    }

    /// <summary>
    ///  Draw the crowd settings section in the crowd panel.
    /// </summary>
    void DrawCrowdSettings()
    {
        if (m_crowdSettingsFoldout.BeginGroup(" Crowd Settings"))
        {
            MASUILayout.AddVerticalPadding();
            // Agent crowd position
            EditorGUI.BeginChangeCheck();
            m_crowdProxy.Position = MASUILayout.Vector3Field("Position", m_crowdProxy.Position);
            if (EditorGUI.EndChangeCheck())
            {
                UpdateSelectedCrowd();
            }
            MASUILayout.AddVerticalPadding();

            // Agent crowd density
            EditorGUI.BeginChangeCheck();
            m_crowdProxy.Density = (int)MASUILayout.Slider("Density", m_crowdProxy.Density, 1, 100);
            if (EditorGUI.EndChangeCheck())
            {
                UpdateSelectedCrowd();
            }

            // Agent crowd spread
            EditorGUI.BeginChangeCheck();
            m_crowdProxy.Spread = MASUILayout.Slider("Spread", m_crowdProxy.Spread, 1, 50);
            if (EditorGUI.EndChangeCheck())
            {
                UpdateSelectedCrowd();
            }
        }
        m_crowdSettingsFoldout.EndGroup();
    }

    /// <summary>
    /// Draw the agent presets section in the crowd panel.
    /// </summary>
    void DrawAgentPresets()
    {
        if (m_agentPresetsFoldout.BeginGroup(" Agent Presets"))
        {
            MASUILayout.AddVerticalPadding();
            // Add/Remove agent presets buttons        
            EditorGUILayout.BeginHorizontal();
            if (MASUILayout.Button(" Add Agent", 100, 20, MASUILayout.ButtonType.Add))
            {
                MASLoadAgentPresetEditor.ShowWindow(this);
            }
            EditorGUILayout.EndHorizontal();
            MASUILayout.AddVerticalPadding();

            GUIStyle areaStyle = new GUIStyle();
            areaStyle.normal.background = MASResources.ScrollViewTexture;
            areaStyle.margin = new RectOffset(0, 0, 0, 0);
            areaStyle.margin = new RectOffset(20, 5, 5, 5);
            areaStyle.padding = new RectOffset(5, 5, 5, 5);

            m_agentPresetsScollPos = EditorGUILayout.BeginScrollView(m_agentPresetsScollPos, areaStyle, GUILayout.Width(MASToolEditor.WindowWidth - 40), GUILayout.Height(95));

            // Crowd agent presets
            for (int i = 0; i < m_crowdProxy.AgentProxies.Count; ++i)
            {
                EditorGUILayout.BeginHorizontal();              

                GUIStyle style = new GUIStyle();                
                style.fixedWidth = 16;
                style.fixedHeight = 16;
                GUILayout.Label(MASResources.AgentIconTexture, style);

                MASUILayout.Label(m_crowdProxy.AgentProxies[i].PresetName);

                if (MASUILayout.Button("", 22, 22, MASUILayout.ButtonType.Remove))
                {
                    RemovedAgentProxy(i);
                }                

                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndScrollView();

            if (m_crowdProxy.AgentProxies.Count <= 0)
            {
                MASUIWarningBox warn = new MASUIWarningBox("A crowd must have at least 1 agent preset assigned.");
                warn.Draw();
            }
        }
        m_agentPresetsFoldout.EndGroup();        
    }

    /// <summary>
    /// Draw the destinations section in the crowd panel.
    /// </summary>
    void DrawDestinations()
    {
        if (m_crowdDestinationsFoldout.BeginGroup(" Destinations"))
        {
            MASUILayout.AddVerticalPadding();
            MASUIInfoBox inf = new MASUIInfoBox(" A crowd will travel to its assigned destinations during the simulation");
            inf.Draw();

            MASUILayout.AddVerticalPadding();
            // Add/Remove crowd goal positions buttons        
            EditorGUILayout.BeginHorizontal();
            if (MASUILayout.Button(" Add Destination", 130, 20, MASUILayout.ButtonType.Add))
            {
                AddCrowdGoalPosition();
            }
            EditorGUILayout.EndHorizontal();
            MASUILayout.AddVerticalPadding();

            GUIStyle destAreaStyle = new GUIStyle();
            destAreaStyle.normal.background = MASResources.ScrollViewTexture;
            destAreaStyle.margin = new RectOffset(0, 0, 0, 0);
            destAreaStyle.margin = new RectOffset(20, 5, 5, 5);
            destAreaStyle.padding = new RectOffset(5, 5, 5, 5);

            m_crowdGoalPosScrollPos = EditorGUILayout.BeginScrollView(m_crowdGoalPosScrollPos, destAreaStyle, GUILayout.Width(MASToolEditor.WindowWidth - 40), GUILayout.Height(95));

            // Crowd agent goal positions
            for (int i = 0; i < m_crowdProxy.GoalPositions.Count; ++i)
            {
                EditorGUILayout.BeginHorizontal();
                GUIStyle style = new GUIStyle();
                style.fixedWidth = 16;
                style.fixedHeight = 16;
                GUILayout.Label(MASResources.RoadmapIconTexture, style);

                EditorGUI.BeginChangeCheck();
                m_crowdProxy.GoalPositions[i] = MASUILayout.Vector3Field("Destination " + (i + 1), m_crowdProxy.GoalPositions[i]);
                if (EditorGUI.EndChangeCheck())
                {
                    UpdateSelectedCrowd();
                }

                //.enabled = m_crowdProxy.GoalPositions.Count > 1;
                if (MASUILayout.Button("", 22, 22, MASUILayout.ButtonType.Remove))
                {
                    RemoveCrowdGoalPosition(i);
                }

                //GUI.enabled = true;

                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndScrollView();

            // Loop Goal Positions            
            EditorGUI.BeginChangeCheck();
            m_crowdProxy.LoopGoalPositions = MASUILayout.Toggle("Loop Destinations", m_crowdProxy.LoopGoalPositions);
            if (EditorGUI.EndChangeCheck())
            {
                UpdateSelectedCrowd();
            }
        }
        m_crowdDestinationsFoldout.EndGroup();
        EditorGUILayout.EndScrollView();
    }

    /// <summary>
    /// Draw the footer at the bottom of the crowd panel.
    /// </summary>
    /// <param name="parentWindow">Editor window this panel is a child of.</param>
    void DrawFooter(EditorWindow parentWindow)
    {
        GUIStyle crowdFooterAreaStyle = new GUIStyle();
        crowdFooterAreaStyle.normal.background = MASResources.SectionFooterTexture;
        GUILayout.BeginArea(new Rect(0, parentWindow.position.height - 80, MASToolEditor.WindowWidth, 40), crowdFooterAreaStyle);

        if (m_crowdProxy.AgentProxies.Count <= 0)
        {
            GUI.enabled = false;
        }

        // Add crowd
        MASUILayout.AddVerticalPadding();
        EditorGUILayout.BeginHorizontal();
        if (MASUILayout.Button(" Add New Crowd", 150, 30, MASUILayout.ButtonType.Confirm))
        {
            AddNewCrowd();
        }

        if (!Application.isPlaying)
        {
            GUI.enabled = true;
        }        

        // Reset Crowd        
        EditorGUILayout.Space();
        if (MASUILayout.Button(" Reset Crowd", 150, 30, MASUILayout.ButtonType.Critical))
        {
            MASResetCrowdEditor.ShowWindow();
        }
        EditorGUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    /// <summary>
    /// Update the panel.
    /// </summary>
    public void Update()
    {
        if (!Selection.activeGameObject)
        {
            return;
        }

        MASCrowd crowd = Selection.activeGameObject.GetComponent<MASCrowd>();
        if (!crowd)
        {
            return;
        }

        // Update the crowd proxy with the selected crowd        
        m_crowdProxy.Position = crowd.transform.position;
        m_crowdProxy.AgentProxies = crowd.AgentProxies;

        m_crowdProxy.GoalPositions.Clear();
        foreach (MASCrowdGoalPositionHandle handle in crowd.GetComponentsInChildren<MASCrowdGoalPositionHandle>())
        {
            m_crowdProxy.GoalPositions.Add(handle.GoalPosition);
        }
        
        m_crowdProxy.LoopGoalPositions = crowd.LoopGoalPositions;        
        m_crowdProxy.Density = crowd.Density;
        m_crowdProxy.Spread = crowd.Spread;
    }

    /// <summary>
    /// Update the current crowd proxy.
    /// </summary>
    /// <param name="crowdProxy">The new crowd proxy.</param>
    public void SetCrowdProxy(MASCrowdProxy crowdProxy)
    {
        m_crowdProxy = crowdProxy;
        UpdateSelectedCrowd();
    }

    /// <summary>
    /// Get the current crowd proxy.
    /// </summary>
    /// <returns>Crowd proxy.</returns>
    public MASCrowdProxy GetCrowdProxy()
    {
        return m_crowdProxy;
    }

    /// <summary>
    /// Add a new crowd to the simualtion from the crowd panel.
    /// </summary>
    private void AddNewCrowd()
    {
        // Add a new crowd to the scene using the current crowd proxy
        MASController masController = Object.FindObjectOfType<MASController>();
        if (!masController)
        {
            return;
        }

        GameObject obj = masController.AddCrowd(m_crowdProxy);
        if (!obj)
        {
            return;
        }
    }

    /// <summary>
    /// Update the crowd that is selected in the scene view, with the settings in the editor window.
    /// </summary>
    private void UpdateSelectedCrowd()
    {
        // Update the currently selected crowd with the current crowd proxy
        if (!Selection.activeGameObject)
        {
            return;
        }

        // Get ref to crowd
        MASCrowd crowd = Selection.activeGameObject.GetComponent<MASCrowd>();
        if (!crowd)
        {
            return;
        }

        if (m_crowdProxy.AgentProxies.Count <= 0)
        {
            Object.DestroyImmediate(crowd.gameObject);
            return;
        }

        // Update crowd settings
        crowd.transform.position = m_crowdProxy.Position;
        crowd.AgentProxies = m_crowdProxy.AgentProxies;
        crowd.OnDensityChanged(m_crowdProxy);
        crowd.OnSpreadChanged(m_crowdProxy.Spread);
        crowd.OnAgentProxiesChanged();
        crowd.Spread = m_crowdProxy.Spread;
        crowd.OnGoalPositionsChanged(m_crowdProxy.GoalPositions);        
        crowd.LoopGoalPositions = m_crowdProxy.LoopGoalPositions;        

        // Update the agents in the crowd
        foreach (MASAgent agent in crowd.GetComponentsInChildren<MASAgent>())
        {             
            agent.LoopGoalPositions = m_crowdProxy.LoopGoalPositions;            
        }

        // Force repaint to show correct debug draws/gizmos
        SceneView.RepaintAll();
    }

    /// <summary>
    /// Add new crowd destination.
    /// </summary>
    private void AddCrowdGoalPosition()
    {
        Vector3 goalPos = m_crowdProxy.Position;
        if (m_crowdProxy.GoalPositions.Count > 0)
        {
            goalPos = m_crowdProxy.GoalPositions[m_crowdProxy.GoalPositions.Count - 1];
        }

        goalPos.x += m_crowdProxy.Spread * 2;
        m_crowdProxy.GoalPositions.Add(goalPos);
        UpdateSelectedCrowd();
    }

    /// <summary>
    /// Remove a crowd destination from the list of current destinations.
    /// </summary>
    /// <param name="pos">Destination index.</param>
    private void RemoveCrowdGoalPosition(int pos)
    {
        if (m_crowdProxy.GoalPositions.Count > 0 && pos < m_crowdProxy.GoalPositions.Count)
        {
            m_crowdProxy.GoalPositions.RemoveAt(pos);
            UpdateSelectedCrowd();
        }
    }    
}