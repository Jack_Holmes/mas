﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Reset agent editor window.
/// </summary>
public class MASResetAgentEditor : EditorWindow
{
    // Flags which determine if the corresponding agent property will be reset.
    private bool m_resetAll = true;
    private bool m_resetPosition = false;
    private bool m_resetRadius = false;
    private bool m_resetMaxSpeed = false;    
    private bool m_resetCharacterPrefab = false;
    private bool m_resetDestinations = false;
    private bool m_resetAdvancedSettings = false;

    /// <summary>
    /// Display the editor window.
    /// </summary>
    public static void ShowWindow()
    {
        var window = GetWindowWithRect<MASResetAgentEditor>(new Rect(0, 0, 300, 230), true, "Reset Agent Settings");
        Extensions.CenterOnMainWin(window);
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, position.width, position.height), MASResources.BackgroundTexture);

        MASUILayout.AddVerticalPadding();
        m_resetAll = MASUILayout.Toggle("Reset All", m_resetAll);

        GUI.enabled = !m_resetAll;

        MASUILayout.AddVerticalPadding(2);
        m_resetPosition = MASUILayout.Toggle("Reset Position", m_resetPosition);

        MASUILayout.AddVerticalPadding();
        m_resetRadius = MASUILayout.Toggle("Reset Radius", m_resetRadius);

        MASUILayout.AddVerticalPadding();
        m_resetMaxSpeed = MASUILayout.Toggle("Reset Max Speed", m_resetMaxSpeed);
        
        MASUILayout.AddVerticalPadding();
        m_resetCharacterPrefab = MASUILayout.Toggle("Reset Character", m_resetCharacterPrefab);

        MASUILayout.AddVerticalPadding();
        m_resetDestinations = MASUILayout.Toggle("Reset Destinations", m_resetDestinations);

        MASUILayout.AddVerticalPadding();
        m_resetAdvancedSettings = MASUILayout.Toggle("Reset Advanced Settings", m_resetAdvancedSettings);

        GUI.enabled = true;

        MASUILayout.AddVerticalPadding(4);
        EditorGUILayout.BeginHorizontal();
        if (MASUILayout.Button("Cancel", 100, 20))
        {
            Close();
        }

        if (MASUILayout.Button("Reset", 100, 20))
        {
            ResetSettings();
            Close();
        }
        EditorGUILayout.EndHorizontal();
    }

    /// <summary>
    /// Reset the agent settings.
    /// </summary>
    void ResetSettings()
    {
        MASToolEditor masTool = GetWindow<MASToolEditor>();
        if (!masTool)
        {
            return;
        }
        
        if (m_resetAll)
        {
            // Reset all properrties.
            MASAgentProxy agentProxy = new MASAgentProxy();
            agentProxy.GoalPositions = new List<Vector3>();            

            // Update the agent proxy.
            masTool.SetAgentProxy(agentProxy);
        }
        else
        {
            // Reset individual properties.
            MASAgentProxy agentProxy = masTool.GetAgentProxy();
            if (m_resetPosition)
            {
                agentProxy.Position = new Vector3(0, 0, 0);
            }

            if (m_resetRadius)
            {
                agentProxy.Radius = MASAgent.RadiusDefault;
            }

            if (m_resetMaxSpeed)
            {
                agentProxy.MaxSpeed = MASAgent.MaxSpeedDeault;
            }

            if (m_resetCharacterPrefab)
            {
                agentProxy.CharacterPrefab = null;
            }

            if (m_resetDestinations)
            {
                agentProxy.GoalPositions = new List<Vector3>();
            }

            if (m_resetAdvancedSettings)
            {
                agentProxy.NeighborDist = MASAgent.NeighborDistDefault;
                agentProxy.MaxNeighbors = MASAgent.MaxNeighborsDefault;
                agentProxy.TimeHorizon = MASAgent.TimeHorizonDefault;
                agentProxy.TimeHorizonObst = MASAgent.TimeHorizonObstDefault;
            }

            // Update the agent proxy.
            masTool.SetAgentProxy(agentProxy);
        }
       
    }
}