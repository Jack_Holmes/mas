﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Manages agent destination scene handles.
/// </summary>
[CustomEditor(typeof(MASGoalPositionHandle))]
public class MASGoalPositionHandleEditor : Editor
{    
    protected virtual void OnSceneGUI()
    {
        MASGoalPositionHandle handle = (MASGoalPositionHandle)target;

        EditorGUI.BeginChangeCheck();
        Vector3 newGoalPosition = Handles.PositionHandle(handle.GoalPosition, Quaternion.identity);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(handle, "Change agent destination");
            handle.GoalPosition = newGoalPosition;
            handle.UpdateHandle();
        }
    }
}
