﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Manages crowd destination scene handles.
/// </summary>
[CustomEditor(typeof(MASCrowdGoalPositionHandle)), CanEditMultipleObjects]
public class MASCrowdGoalPositionHandleEditor : Editor
{
    protected virtual void OnSceneGUI()
    {
        MASCrowdGoalPositionHandle handle = (MASCrowdGoalPositionHandle)target;

        Vector3 oldGoalPos = handle.GoalPosition;

        EditorGUI.BeginChangeCheck();
        Vector3 newGoalPosition = Handles.PositionHandle(handle.GoalPosition, Quaternion.identity);
        if (EditorGUI.EndChangeCheck())
        {

            Undo.RecordObject(handle, "Change crowd destination");
            handle.GoalPosition = newGoalPosition;           
            handle.Translate(newGoalPosition - oldGoalPos);   
        }
    }

    private void OnEnable()
    {
        Undo.undoRedoPerformed += OnUndoRedo;
    }

    private void OnDestroy()
    {
        Undo.undoRedoPerformed -= OnUndoRedo;
    }

    /// <summary>
    /// UndoRedo callback.
    /// </summary>
    public void OnUndoRedo()
    {
        // Manual Undo/redo required to handle all child destination handles.
        MASCrowdGoalPositionHandle handle = (MASCrowdGoalPositionHandle)target;
        handle.RecalculateAgentDestinationHandles();
    }
}
