﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// An information box that adds a MAS Controller to the active scene.
/// </summary>
public class MASAddControllerEditor : EditorWindow
{
    /// <summary>
    /// Display the editor window.
    /// </summary>
    public static void ShowWindow()
    {
        var window = GetWindowWithRect<MASAddControllerEditor>(new Rect(0, 0, 400, 110), true, "Add MAS Controller");
        Extensions.CenterOnMainWin(window);
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, position.width, position.height), MASResources.BackgroundTexture);

        GUILayout.BeginHorizontal();
        GUIStyle style = new GUIStyle(EditorStyles.label);
        style.margin = new RectOffset(20, 0, 10, 0);
        style.padding = new RectOffset(0, 0, 0, 0);
        style.border = new RectOffset(0, 0, 0, 0);
        style.stretchHeight = false;
        style.stretchWidth = false;
        GUILayout.Label(MASResources.InfoDialogIconTexture, style);

        MASUILayout.Label("Add simulation controller to the current scene?");
        GUILayout.EndHorizontal();

        MASUILayout.AddVerticalPadding(8);

        GUILayout.BeginHorizontal();
        EditorGUILayout.Space();
        if (MASUILayout.Button("Add", 100, 20))
        {
            Close();

            GameObject mas = new GameObject();
            mas.name = "MAS_Controller";
            mas.AddComponent<MASController>();
            PrefabUtility.InstantiatePrefab(mas);

            GameObject region = GameObject.CreatePrimitive(PrimitiveType.Quad);
            region.name = "MAS_Region";
            region.transform.Rotate(new Vector3(90, 0, 0));
            region.transform.localScale += new Vector3(100, 100, 0);
            region.transform.parent = mas.transform;
            region.AddComponent<MASRegion>();
            MeshRenderer renderer = region.GetComponent<MeshRenderer>();
            if (renderer)
            {
                renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                renderer.receiveShadows = false;
                Material mat = Resources.Load("Materials/WalkableRegion", typeof(Material)) as Material;
                renderer.material = mat;
            }
            int areaIndex = UnityEngine.AI.NavMesh.GetAreaFromName("Walkable");
            GameObjectUtility.SetNavMeshArea(region, areaIndex);
            GameObjectUtility.SetStaticEditorFlags(region, StaticEditorFlags.NavigationStatic | StaticEditorFlags.OffMeshLinkGeneration);
            PrefabUtility.InstantiatePrefab(region);

            UnityEditor.SceneManagement.EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            UnityEditor.AI.NavMeshBuilder.BuildNavMesh();
        }

        if (MASUILayout.Button("Cancel", 100, 20))
        {
            Close();            
        }
        GUILayout.EndHorizontal();
    }
}