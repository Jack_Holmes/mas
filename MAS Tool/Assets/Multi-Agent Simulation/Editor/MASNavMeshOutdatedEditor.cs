﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// An information box that is presented to the user that informs them the NavMesh needs updating.
/// </summary>
public class MASNavMeshOutdatedEditor : EditorWindow
{
    /// <summary>
    /// Display the editor window.
    /// </summary>
    public static void ShowWindow()
    {
        var window = GetWindowWithRect<MASNavMeshOutdatedEditor>(new Rect(0, 0, 400, 110), true, "NavMesh Outdated");
        Extensions.CenterOnMainWin(window);
    }    

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, position.width, position.height), MASResources.BackgroundTexture);

        GUILayout.BeginHorizontal();
        GUIStyle style = new GUIStyle(EditorStyles.label);
        style.margin = new RectOffset(20, 0, 10, 0);
        style.padding = new RectOffset(0, 0, 0, 0);
        style.border = new RectOffset(0, 0, 0, 0);
        style.stretchHeight = false;
        style.stretchWidth = false;
        GUILayout.Label(MASResources.InfoDialogIconTexture, style);

        MASUILayout.Label("Some settings have changed that have caused the NavMesh to be outdated. This may cause unreliable behaviour. Would you like to re-bake the NavMesh now?");
        GUILayout.EndHorizontal();

        MASUILayout.AddVerticalPadding(4);

        GUILayout.BeginHorizontal();
        EditorGUILayout.Space();
        if (MASUILayout.Button("Yes", 100, 20))
        {
            // Rebuild the NavMesh before continuing to play the current scene.
            UnityEditor.AI.NavMeshBuilder.BuildNavMesh();
            MASToolEditor.NavMeshDirty(false);
            Close();
            EditorApplication.ExecuteMenuItem("Edit/Play");
        }

        if (MASUILayout.Button("No", 100, 20))
        {
            Close();
            EditorApplication.ExecuteMenuItem("Edit/Play");
        }
        GUILayout.EndHorizontal();
    }
}