﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Editor window to load agent presets
/// </summary>
public class MASLoadAgentPresetEditor : EditorWindow
{
    // The window width and height of editor window.
    private static int m_windowWidth = 300;
    private static int m_windowHeight = 600;

    // Sections sizes
    private int m_agentPresetListWidth = 300;
    private int m_agentPresetListHeight = 400;
    private int m_seperatorHeight = 5;
    private int m_previewWindowHeight = 165;

    // Scroll positon of the agent presets list
    Vector2 m_scrollPos = new Vector2();

    List<GameObject> m_gameObjects = new List<GameObject>();
    int m_selectedIndex = 0;
    bool m_folderEmpty = false;
    // Gameobject to display the agent preset character
    GameObject m_gameObject;

    // Editor used for preview window
    Editor m_gameObjectEditor;

    // Agent proxy that is to be loaded
    MASAgentProxy m_agentProxy;
    string m_agentPresetName = string.Empty;

    // Reference to the agent panel that requests load
    private static MASAgentIndividualPanel s_agentPanel;
    private static MASCrowdPanel s_crowdPanel;

    enum RequestedPanel
    {
        Agent,
        Crowd
    }
    static RequestedPanel s_requestedPanel;

    /// <summary>
    /// Show the editor window
    /// </summary>
    /// <param name="agentPanel">Agent panel that is opening the window.</param>
    public static void ShowWindow(MASAgentIndividualPanel agentPanel)
    {
        var window = GetWindowWithRect<MASLoadAgentPresetEditor>(new Rect(0, 0, m_windowWidth, m_windowHeight), true, "Load Agent Preset");
        Extensions.CenterOnMainWin(window);        

        s_agentPanel = agentPanel;
        s_requestedPanel = RequestedPanel.Agent;
    }

    /// <summary>
    /// Show the editor window.
    /// </summary>
    /// <param name="crowdPanel">Crowd panel that is opening the window.</param>
    public static void ShowWindow(MASCrowdPanel crowdPanel)
    {
        var window = GetWindowWithRect<MASLoadAgentPresetEditor>(new Rect(0, 0, m_windowWidth, m_windowHeight), true, "Load Agent Preset");
        Extensions.CenterOnMainWin(window);

        s_crowdPanel = crowdPanel;
        s_requestedPanel = RequestedPanel.Crowd;
    }

    private void OnGUI()
    {
        if (!m_folderEmpty && m_gameObjects.Count <= 0)
        {
            LoadGameObjects();
        }

        GUI.DrawTexture(new Rect(0, 0, position.width, position.height), MASResources.BackgroundTexture);       
        DrawAgentPresetsList();
        DrawPreviewWindow();       
        DrawFooter();
    }

    /// <summary>
    /// Draw the list that displays the available agent presets (that have been previously created).
    /// </summary>
    void DrawAgentPresetsList()
    {
        GUIStyle areaStyle = new GUIStyle();
        areaStyle.normal.background = MASResources.SectionFooterTexture;
        GUILayout.BeginArea(new Rect(0, m_agentPresetListHeight, m_agentPresetListWidth, m_seperatorHeight), areaStyle);
        GUILayout.EndArea();

        string path = "Assets/Multi-Agent Simulation/Resources/AgentPresets";
        if (!AssetDatabase.IsValidFolder(path))
        {
            return;
        }       

        m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos, GUILayout.Width(m_agentPresetListWidth), GUILayout.Height(m_agentPresetListHeight));
        
        for (int i = 0; i < m_gameObjects.Count; ++i)
        {
            MASAgentPreset agentPreset = m_gameObjects[i].GetComponent<MASAgentPreset>();
            if (agentPreset == null)
            {
                continue;
            }

            if (agentPreset.AgentProxy == null)
            {
                continue;
            }

            if (MASUILayout.ListButton(agentPreset.AgentProxy.PresetName, m_agentPresetListWidth - 30, 20, i == m_selectedIndex))
            {
                m_selectedIndex = i;
                m_agentPresetName = agentPreset.AgentProxy.PresetName;
                m_gameObject = agentPreset.AgentProxy.CharacterPrefab;
                if (m_gameObjectEditor != null)
                {
                    DestroyImmediate(m_gameObjectEditor);
                }
                m_gameObjectEditor = Editor.CreateEditor(m_gameObject);
                m_agentProxy = agentPreset.AgentProxy;
            }
        }

        EditorGUILayout.EndScrollView();
    }

    void LoadGameObjects()
    {
        m_gameObjects.Clear();

        string path = "Assets/Multi-Agent Simulation/Resources/AgentPresets";
        if (!AssetDatabase.IsValidFolder(path))
        {
            m_folderEmpty = true;
            return;
        }

        var guids = AssetDatabase.FindAssets("", new string[] { path });
        if (guids.Length <= 0)
        {
            m_folderEmpty = true;
            return;
        }

        foreach (var guid in guids)
        {
            string filename = System.IO.Path.GetFileNameWithoutExtension(AssetDatabase.GUIDToAssetPath(guid));
            GameObject prefab = Resources.Load("AgentPresets/" + filename, typeof(GameObject)) as GameObject;
            m_gameObjects.Add(prefab);
        }

        // Select the first available preset
        for (int i = 0; i < m_gameObjects.Count; ++i)
        {
            MASAgentPreset agentPreset = m_gameObjects[i].GetComponent<MASAgentPreset>();
            if (agentPreset == null)
            {
                continue;
            }

            if (agentPreset.AgentProxy == null)
            {
                continue;
            }

            m_selectedIndex = i;
            m_agentPresetName = agentPreset.AgentProxy.PresetName;
            m_gameObject = agentPreset.AgentProxy.CharacterPrefab;
            if (m_gameObjectEditor != null)
            {
                DestroyImmediate(m_gameObjectEditor);
            }
            m_gameObjectEditor = Editor.CreateEditor(m_gameObject);
            m_agentProxy = agentPreset.AgentProxy;
            return;            
        }
    }

    /// <summary>
    /// Draw the selected agent preset and its properties to a preview window
    /// </summary>
    void DrawPreviewWindow()
    {        
        GUIStyle areaStyle = new GUIStyle();        
        GUILayout.BeginArea(new Rect(0, m_agentPresetListHeight + m_seperatorHeight, m_agentPresetListWidth, m_previewWindowHeight), areaStyle);
        if (m_gameObject != null && m_gameObjectEditor != null)
        {          
            m_gameObjectEditor.OnPreviewGUI(GUILayoutUtility.GetRect(m_windowWidth - m_agentPresetListWidth, m_previewWindowHeight), GUIStyle.none);

            if (m_agentProxy != null)
            {
                float leftMargin = 10;
                float ySep = 15;
                GUIStyle labelStyle = new GUIStyle();
                labelStyle.normal.textColor = new Color(0.258f, 0.525f, 0.957f);
                labelStyle.fontSize = 13;
                labelStyle.fontStyle = FontStyle.BoldAndItalic;
                Handles.Label(new Vector3(leftMargin, 0.7f * ySep, 0), m_agentPresetName, labelStyle);
                labelStyle.normal.textColor = Color.white;
                labelStyle.fontSize = 12;
                labelStyle.fontStyle = FontStyle.Normal;
                Handles.Label(new Vector3(leftMargin, 2 * ySep, 0), "Radius: " + m_agentProxy.Radius.ToString(), labelStyle);
                Handles.Label(new Vector3(leftMargin, 3 * ySep, 0), "Max Speed: " + m_agentProxy.MaxSpeed.ToString(), labelStyle);                
                Handles.Label(new Vector3(leftMargin, 4 * ySep, 0), "Character: " + m_agentProxy.CharacterPrefab.name.ToString(), labelStyle);
            }
        }

        GUILayout.EndArea();
    }

    /// <summary>
    /// Draw the editor window footer area
    /// </summary>
    void DrawFooter()
    {
        GUIStyle areaStyle = new GUIStyle();        
        GUILayout.BeginArea(new Rect(0, (m_agentPresetListHeight + m_seperatorHeight + m_previewWindowHeight), m_agentPresetListWidth, m_windowHeight - (m_agentPresetListHeight + m_previewWindowHeight)), areaStyle);
        areaStyle.normal.background = MASResources.SectionFooterTexture;

        MASUILayout.AddVerticalPadding();
        EditorGUILayout.BeginHorizontal();        
        if (MASUILayout.Button("Cancel", 100, 17))
        {
            Close();
        }

        if (MASUILayout.Button("Load", 100, 17))
        {
            LoadAgentPreset();
            Close();
        }
        EditorGUILayout.EndHorizontal();

        GUILayout.EndArea();
    }

    /// <summary>
    /// Attempt to load agent preset into agent panel
    /// </summary>
    void LoadAgentPreset()
    {
        if (s_requestedPanel == RequestedPanel.Agent && s_agentPanel == null)
        {
            return;
        }
        else if (s_requestedPanel == RequestedPanel.Crowd && s_crowdPanel == null)
        {
            return;
        }

        if (m_agentPresetName == string.Empty)
        {
            return;
        }

        GameObject prefab = Resources.Load("AgentPresets/" + m_agentPresetName, typeof(GameObject)) as GameObject;
        if (prefab == null)
        {
            return;
        }

        GameObject instance = Instantiate(prefab) as GameObject;
        if (instance == null)
        {
            return;
        }

        MASAgentPreset agentPreset = instance.GetComponent<MASAgentPreset>();
        if (agentPreset == null)
        {
            return;
        }

        if (agentPreset.AgentProxy == null)
        {
            return;
        }

        if (s_requestedPanel == RequestedPanel.Agent)
        {
            s_agentPanel.SetAgentProxyFromPreset(agentPreset.AgentProxy);            
        }
        else if (s_requestedPanel == RequestedPanel.Crowd)
        {
            s_crowdPanel.AddAgentProxy(agentPreset.AgentProxy);
        }

        DestroyImmediate(instance);
    }
}