﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Reset agent crowd editor window.
/// </summary>
public class MASResetCrowdEditor : EditorWindow
{
    // Flags which determine if the corresponding crowd property will be reset.
    private bool m_resetAll = true;
    private bool m_resetPosition = false;
    private bool m_resetDensity = false;
    private bool m_resetSpread = false;    
    private bool m_resetDestinations = false;   

    /// <summary>
    /// Display the editor window.
    /// </summary>
    public static void ShowWindow()
    {
        var window = GetWindowWithRect<MASResetCrowdEditor>(new Rect(0, 0, 300, 180), true, "Reset Agent Crowd Settings");
        Extensions.CenterOnMainWin(window);
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, position.width, position.height), MASResources.BackgroundTexture);

        MASUILayout.AddVerticalPadding();
        m_resetAll = MASUILayout.Toggle("Reset All", m_resetAll);

        GUI.enabled = !m_resetAll;

        MASUILayout.AddVerticalPadding(2);
        m_resetPosition = MASUILayout.Toggle("Reset Position", m_resetPosition);

        MASUILayout.AddVerticalPadding();
        m_resetDensity = MASUILayout.Toggle("Reset Density", m_resetDensity);

        MASUILayout.AddVerticalPadding();
        m_resetSpread = MASUILayout.Toggle("Reset Spread", m_resetSpread);        

        MASUILayout.AddVerticalPadding();
        m_resetDestinations = MASUILayout.Toggle("Reset Destinations", m_resetDestinations);

        GUI.enabled = true;

        MASUILayout.AddVerticalPadding(4);
        EditorGUILayout.BeginHorizontal();
        if (MASUILayout.Button("Cancel", 100, 20))
        {
            Close();
        }

        if (MASUILayout.Button("Reset", 100, 20))
        {
            ResetSettings();
            Close();
        }
        EditorGUILayout.EndHorizontal();
    }

    /// <summary>
    /// Reset the crowd settings.
    /// </summary>
    void ResetSettings()
    {
        MASToolEditor masTool = GetWindow<MASToolEditor>();
        if (!masTool)
        {
            return;
        }
        
        if (m_resetAll)
        {
            // Reset all properties
            MASCrowdProxy crowdProxy = new MASCrowdProxy();
            crowdProxy.GoalPositions = new List<Vector3>();            

            // Update the crowd proxy.
            masTool.SetCrowdProxy(crowdProxy);
        }
        else
        {
            // Reset individual properties.
            MASCrowdProxy crowdProxy = masTool.GetCrowdProxy();
            if (crowdProxy == null)
            {
                return;
            }

            if (m_resetPosition)
            {
                crowdProxy.Position = new Vector3(0, 0, 0);
            }

            if (m_resetDensity)
            {
                crowdProxy.Density = 15;
            }

            if (m_resetSpread)
            {
                crowdProxy.Spread = 5;
            } 

            if (m_resetDestinations)
            {
                crowdProxy.GoalPositions = new List<Vector3>();                
            }
           
            // Update the crowd proxy.
            masTool.SetCrowdProxy(crowdProxy);
        }        
    }
}