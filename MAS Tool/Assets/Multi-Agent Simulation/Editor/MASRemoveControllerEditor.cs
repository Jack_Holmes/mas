﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// An information box that removes a MAS Controller from the active scene.
/// </summary>
public class MASRemoveControllerEditor : EditorWindow
{
    /// <summary>
    /// Display the editor window.
    /// </summary>
    public static void ShowWindow()
    {
        var window = GetWindowWithRect<MASRemoveControllerEditor>(new Rect(0, 0, 400, 110), true, "Destroy Simulation");
        Extensions.CenterOnMainWin(window);
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, position.width, position.height), MASResources.BackgroundTexture);
        GUILayout.BeginHorizontal();
        GUIStyle style = new GUIStyle(EditorStyles.label);
        style.margin = new RectOffset(20, 0, 10, 0);
        style.padding = new RectOffset(0, 0, 0, 0);
        style.border = new RectOffset(0, 0, 0, 0);
        style.stretchHeight = false;
        style.stretchWidth = false;
        GUILayout.Label(MASResources.CriticalIconTexture, style);

        MASUILayout.Label("You are attempting to destroy the current simulation and all of its components. This operation is not reversible. Do you wish to proceed?");
        GUILayout.EndHorizontal();

        MASUILayout.AddVerticalPadding(6);

        GUILayout.BeginHorizontal();
        EditorGUILayout.Space();
        if (MASUILayout.Button("Destroy", 100, 20))
        {
            Close();

            MASController masController = FindObjectOfType<MASController>();
            if (masController)
            {
                DestroyImmediate(masController.gameObject);
            }
        }

        if (MASUILayout.Button("Cancel", 100, 20))
        {
            Close();
        }
        GUILayout.EndHorizontal();
    }
}