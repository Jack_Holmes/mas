﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Holds resources used by the MAS Tool editor.
/// </summary>
public static class MASResources
{
    public static Texture2D BackgroundTexture;
    public static Texture2D BannerTexture;
    public static Texture2D IconTexture;

    // Footers
    public static Texture2D FooterTexture;
    public static Texture2D SectionFooterTexture;

    // Views
    public static Texture2D ScrollViewTexture;

    // Controls
    public static Texture2D PlayControlTexture;
    public static Texture2D StepControlTexture;
    public static Texture2D ResumeControlTexture;
    public static Texture2D PauseControlTexture;
    public static Texture2D StopControlTexture;

    // Icons
    public static Texture2D SimulationIconTexture;
    public static Texture2D AgentIconTexture;
    public static Texture2D CrowdIconTexture;
    public static Texture2D ObstacleIconTexture;
    public static Texture2D RoadmapIconTexture;
    public static Texture2D InfoIconTexture;
    public static Texture2D InfoDialogIconTexture;
    public static Texture2D QuestionMarkIconTexture;
    public static Texture2D ConfirmIconTexture;
    public static Texture2D CriticalIconTexture;
    public static Texture2D RemoveIconTexture;
    public static Texture2D WarningIconTexture;
    public static Texture2D SaveIconTexture;
    public static Texture2D OpenIconTexture;
    public static Texture2D AddIconTexture;

    // Button
    public static Texture2D ButtonTexture;
    public static Texture2D ButtonActiveTexture;
    public static Texture2D ButtonHoverTexture;

    // Toolbar Button
    public static Texture2D TabButtonTexture;
    public static Texture2D TabButtonOnNormalTexture;
    public static Texture2D TabButtonActiveTexture;
    public static Texture2D TabButtonHoverTexture;

    // Foldout
    public static Texture2D FoldoutTexture;
    public static Texture2D FoldoutOpenTexture;
    public static Texture2D FoldoutActiveTexture;
    public static Texture2D FoldoutArrowCloseTexture;
    public static Texture2D FoldoutArrowOpenTexture;

    // Toggle
    public static Texture2D ToggleOffTexture;
    public static Texture2D ToggleOnTexture;

    // Slider
    public static Texture2D SliderTexture;
    public static Texture2D SliderThumbTexture;

    // Popup
    public static Texture2D EnumPopupTexture;

    // Help Images
    public static Texture2D HelpResetTimeStep;
    public static Texture2D HelpAddAgent;
    public static Texture2D HelpAddDestination;
    public static Texture2D HelpAddDestination2;
    public static Texture2D HelpAddDestination3;
    public static Texture2D HelpRemoveDestination;
    public static Texture2D HelpSaveAgentPreset;
    public static Texture2D HelpAgentPresetName;
    public static Texture2D HelpLoadAgentPreset;
    public static Texture2D HelpLoadAgentPresetWindow;
    public static Texture2D HelpResetAgent;
    public static Texture2D HelpResetAgentWindow;
    public static Texture2D HelpAddCrowd;
    public static Texture2D HelpCrowdAddAgentPreset;
    public static Texture2D HelpCrowdAgentsList;
    public static Texture2D HelpCrowdAddDestination;
    public static Texture2D HelpCrowdAddDestination2;
    public static Texture2D HelpCrowdAddDestination3;
    public static Texture2D HelpResetCrowd;
    public static Texture2D HelpResetCrowdWindow;

    public static void Load()
    {
        string imgPath = "Images/";
        string UIPath = "UI/";

        BackgroundTexture = Resources.Load(imgPath + "background", typeof(Texture2D)) as Texture2D;
        BannerTexture = Resources.Load(imgPath + "banner", typeof(Texture2D)) as Texture2D;
        IconTexture = Resources.Load(imgPath + "icon", typeof(Texture2D)) as Texture2D;
        FooterTexture = Resources.Load(UIPath + "footer", typeof(Texture2D)) as Texture2D;

        ScrollViewTexture = Resources.Load(UIPath + "scroll_view", typeof(Texture2D)) as Texture2D;
        SectionFooterTexture = Resources.Load(UIPath + "section_footer", typeof(Texture2D)) as Texture2D;

        PlayControlTexture = Resources.Load(UIPath + "play", typeof(Texture2D)) as Texture2D;
        StepControlTexture = Resources.Load(UIPath + "step", typeof(Texture2D)) as Texture2D;
        ResumeControlTexture = Resources.Load(UIPath + "resume", typeof(Texture2D)) as Texture2D;
        PauseControlTexture = Resources.Load(UIPath + "pause", typeof(Texture2D)) as Texture2D;
        StopControlTexture = Resources.Load(UIPath + "stop", typeof(Texture2D)) as Texture2D;

        SimulationIconTexture = Resources.Load(UIPath + "simulation_icon", typeof(Texture2D)) as Texture2D;
        AgentIconTexture = Resources.Load(UIPath + "agent_icon", typeof(Texture2D)) as Texture2D;
        CrowdIconTexture = Resources.Load(UIPath + "crowd_icon", typeof(Texture2D)) as Texture2D;
        ObstacleIconTexture = Resources.Load(UIPath + "obstacle_icon", typeof(Texture2D)) as Texture2D;
        RoadmapIconTexture = Resources.Load(UIPath + "roadmap_icon", typeof(Texture2D)) as Texture2D;
        InfoIconTexture = Resources.Load(UIPath + "info", typeof(Texture2D)) as Texture2D;
        InfoDialogIconTexture = Resources.Load(UIPath + "info_dialog", typeof(Texture2D)) as Texture2D;
        QuestionMarkIconTexture = Resources.Load(UIPath + "question_mark", typeof(Texture2D)) as Texture2D;
        ConfirmIconTexture = Resources.Load(UIPath + "confirm", typeof(Texture2D)) as Texture2D;
        CriticalIconTexture = Resources.Load(UIPath + "critical", typeof(Texture2D)) as Texture2D;
        RemoveIconTexture = Resources.Load(UIPath + "remove", typeof(Texture2D)) as Texture2D;
        WarningIconTexture = Resources.Load(UIPath + "warning", typeof(Texture2D)) as Texture2D;
        SaveIconTexture = Resources.Load(UIPath + "save", typeof(Texture2D)) as Texture2D;
        OpenIconTexture = Resources.Load(UIPath + "open", typeof(Texture2D)) as Texture2D;
        AddIconTexture = Resources.Load(UIPath + "add_icon", typeof(Texture2D)) as Texture2D;

        ButtonTexture = Resources.Load(UIPath + "button", typeof(Texture2D)) as Texture2D; ;
        ButtonActiveTexture = Resources.Load(UIPath + "button_active", typeof(Texture2D)) as Texture2D;
        ButtonHoverTexture = Resources.Load(UIPath + "button_hover", typeof(Texture2D)) as Texture2D;

        TabButtonTexture = Resources.Load(UIPath + "tab_button", typeof(Texture2D)) as Texture2D; ;
        TabButtonOnNormalTexture = Resources.Load(UIPath + "tab_button_onnormal", typeof(Texture2D)) as Texture2D; ;
        TabButtonActiveTexture = Resources.Load(UIPath + "tab_button_active", typeof(Texture2D)) as Texture2D; ;
        TabButtonHoverTexture = Resources.Load(UIPath + "tab_button_hover", typeof(Texture2D)) as Texture2D; ;

        FoldoutTexture = Resources.Load(UIPath + "foldout", typeof(Texture2D)) as Texture2D;
        FoldoutOpenTexture = Resources.Load(UIPath + "foldout_open", typeof(Texture2D)) as Texture2D;
        FoldoutActiveTexture = Resources.Load(UIPath + "foldout_active", typeof(Texture2D)) as Texture2D;
        FoldoutArrowCloseTexture = Resources.Load(UIPath + "arrow_close", typeof(Texture2D)) as Texture2D;
        FoldoutArrowOpenTexture = Resources.Load(UIPath + "arrow_open", typeof(Texture2D)) as Texture2D;

        ToggleOffTexture = Resources.Load(UIPath + "toggle_off", typeof(Texture2D)) as Texture2D;
        ToggleOnTexture = Resources.Load(UIPath + "toggle_on", typeof(Texture2D)) as Texture2D;

        SliderTexture = Resources.Load(UIPath + "slider", typeof(Texture2D)) as Texture2D;
        SliderThumbTexture = Resources.Load(UIPath + "slider_thumb", typeof(Texture2D)) as Texture2D;

        EnumPopupTexture = Resources.Load(UIPath + "enum_popup", typeof(Texture2D)) as Texture2D;

        HelpResetTimeStep = Resources.Load(imgPath + "Help/reset_time_step", typeof(Texture2D)) as Texture2D;
        HelpAddAgent = Resources.Load(imgPath + "Help/add_agent", typeof(Texture2D)) as Texture2D;
        HelpAddDestination = Resources.Load(imgPath + "Help/add_destination", typeof(Texture2D)) as Texture2D;
        HelpAddDestination2 = Resources.Load(imgPath + "Help/add_destination_2", typeof(Texture2D)) as Texture2D;
        HelpAddDestination3 = Resources.Load(imgPath + "Help/add_destination_3", typeof(Texture2D)) as Texture2D;
        HelpRemoveDestination = Resources.Load(imgPath + "Help/remove_destination", typeof(Texture2D)) as Texture2D;
        HelpSaveAgentPreset = Resources.Load(imgPath + "Help/save_agent_preset", typeof(Texture2D)) as Texture2D;
        HelpAgentPresetName = Resources.Load(imgPath + "Help/agent_preset_name", typeof(Texture2D)) as Texture2D;
        HelpLoadAgentPreset = Resources.Load(imgPath + "Help/load_agent_preset", typeof(Texture2D)) as Texture2D;
        HelpLoadAgentPresetWindow = Resources.Load(imgPath + "Help/load_agent_preset_window", typeof(Texture2D)) as Texture2D;
        HelpResetAgent = Resources.Load(imgPath + "Help/reset_agent", typeof(Texture2D)) as Texture2D;
        HelpResetAgentWindow = Resources.Load(imgPath + "Help/reset_agent_window", typeof(Texture2D)) as Texture2D;
        HelpAddCrowd = Resources.Load(imgPath + "Help/add_crowd", typeof(Texture2D)) as Texture2D;
        HelpCrowdAddAgentPreset = Resources.Load(imgPath + "Help/crowd_add_agent_preset", typeof(Texture2D)) as Texture2D;
        HelpCrowdAgentsList = Resources.Load(imgPath + "Help/crowd_agents_list", typeof(Texture2D)) as Texture2D;
        HelpCrowdAddDestination = Resources.Load(imgPath + "Help/add_crowd_destination", typeof(Texture2D)) as Texture2D;
        HelpCrowdAddDestination2 = Resources.Load(imgPath + "Help/add_crowd_destination_2", typeof(Texture2D)) as Texture2D;
        HelpCrowdAddDestination3 = Resources.Load(imgPath + "Help/add_crowd_destination_3", typeof(Texture2D)) as Texture2D;
        HelpResetCrowd = Resources.Load(imgPath + "Help/reset_crowd", typeof(Texture2D)) as Texture2D;
        HelpResetCrowdWindow = Resources.Load(imgPath + "Help/reset_crowd_window", typeof(Texture2D)) as Texture2D;
    }
}