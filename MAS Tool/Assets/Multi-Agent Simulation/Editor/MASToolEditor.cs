﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// The main editor window for the MAS Tool.
/// </summary>
public class MASToolEditor : EditorWindow
{
    // The window width and height of the editor window.
    public static int WindowWidth = 512;
    public static int WindowHeight = 780;    

    // Keeps track of the active main tab
    enum ToolTab
    {
        Simulation,
        Agent,
        Crowd,
        Obstacles
    }
    ToolTab m_toolTab = ToolTab.Agent;  

    // Panels  
    MASSimulationSettingsPanel m_simulationSettingsPanel = new MASSimulationSettingsPanel();    
    MASAgentIndividualPanel m_agentPanel = new MASAgentIndividualPanel();
    MASCrowdPanel m_crowdPanel = new MASCrowdPanel();      
    MASObstaclesPanel m_obstaclesPanel = new MASObstaclesPanel();  
 

    // Keep track of the selected object in the scene view
    GameObject m_selectedObject;

    static bool s_navMeshDirty;

    // Entry point to show editor window from toolbar
    [MenuItem("Tools/Multi-agent Simulation (MAS)/Editor")]
    public static void ShowWindow()
    {
        var window = GetWindowWithRect<MASToolEditor>(new Rect(0, 0, WindowWidth, WindowHeight), false, "MAS Tool");
        window.titleContent = new GUIContent("MAS Tool", MASResources.IconTexture, "Crowd Simulation for Unity");
    }

    bool m_isPlaying = false;
    
    enum DialogDisplayStatus
    {
        Required,
        Showing,
        NotRequired
    }
    DialogDisplayStatus m_navMeshDialogStatus = DialogDisplayStatus.Required;

    public void Update()
    {
        // When the application starts check if the NavMesh is outdated and give the 
        // user a chance to rebake it.
        if (Application.isPlaying && !m_isPlaying)
        {
            if (m_navMeshDialogStatus == 0 && s_navMeshDirty)
            {
                EditorApplication.ExecuteMenuItem("Edit/Play");
                MASNavMeshOutdatedEditor.ShowWindow();
                m_isPlaying = false;
                m_navMeshDialogStatus = DialogDisplayStatus.Showing;
            }
            else
            {
                m_isPlaying = true;
                m_navMeshDialogStatus = DialogDisplayStatus.NotRequired;
            }            
        }
        else if (!Application.isPlaying && m_isPlaying)
        {
            m_isPlaying = false;

            if (m_navMeshDialogStatus == DialogDisplayStatus.NotRequired)
            {
                m_navMeshDialogStatus = DialogDisplayStatus.Required;
            }
            else if (m_navMeshDialogStatus == DialogDisplayStatus.Showing)
            {
                m_navMeshDialogStatus = DialogDisplayStatus.NotRequired;
            }
        }
    }

    public void OnInspectorUpdate()
    {
        Repaint();

        if (m_selectedObject)
        { 
            if (m_toolTab == ToolTab.Agent)
            {
                m_agentPanel.Update();
            }
            else if (m_toolTab == ToolTab.Crowd)
            {
                m_crowdPanel.Update();
            }
            else if (m_toolTab == ToolTab.Obstacles)
            {
                m_obstaclesPanel.Update();
            }
        }
    }

    public void OnEnable()
    {
        MASResources.Load();       

        s_navMeshDirty = PlayerPrefs.GetInt("MAS_NavMeshDirty", 0) == 0 ? false : true;        
    }

    public void OnSelectionChange()
    {
        // Keep reference to selected object
        m_selectedObject = Selection.activeGameObject;

        // Update editor tab based on selected object
        if (m_selectedObject)
        {
            MASController MAS = m_selectedObject.GetComponent<MASController>();
            if (MAS)
            {
                m_toolTab = ToolTab.Simulation;
            }

            MASAgent agent = m_selectedObject.GetComponent<MASAgent>();
            if (agent)
            {
                m_toolTab = ToolTab.Agent;               

                return;
            }

            MASCrowd crowd = m_selectedObject.GetComponent<MASCrowd>();
            if (crowd)
            {
                m_toolTab = ToolTab.Crowd;

                return;
            }

            MASBlockObstacle block = m_selectedObject.GetComponent<MASBlockObstacle>();
            if (block)
            {
                m_toolTab = ToolTab.Obstacles;
                m_obstaclesPanel.OnSelectionChange();

                return;
            }

            MASCylinderObstacle cylinder = m_selectedObject.GetComponent<MASCylinderObstacle>();
            if (cylinder)
            {
                m_toolTab = ToolTab.Obstacles;                
                m_obstaclesPanel.OnSelectionChange();

                return;
            }
        }
    }

    private void OnGUI()
    {
        DrawBackground();
        DrawBanner();

        MASController masController = FindObjectOfType<MASController>();
        if (masController)
        {
            DrawToolbar();

            if (Application.isPlaying)
            {
                GUI.enabled = false;
            }

            DrawActiveTab();
            GUI.enabled = true;
            DrawFooter();
        }
        else
        {
            MASUILayout.AddVerticalPadding(2);
            MASUILayout.Label("No simulation controller present");
            MASUILayout.AddVerticalPadding();
            if (MASUILayout.Button("Get Started", 180, 26))
            {
                MASAddControllerEditor.ShowWindow();
            }

            MASUILayout.AddVerticalPadding();
            if (MASUILayout.Button("Help", 180, 26))
            {
                ShowHelp();
            }
        }
    }

    private void DrawBackground()
    {
        GUI.DrawTexture(new Rect(0, 0, position.width, position.height), MASResources.BackgroundTexture);       
    }

    private void DrawBanner()
    {
        GUIStyle style = new GUIStyle(EditorStyles.label);
        style.margin = new RectOffset(0, 0, 0, 0);
        style.padding = new RectOffset(0, 0, 0, 0);
        style.border = new RectOffset(0, 0, 0, 0);
        style.stretchHeight = false;
        style.stretchWidth = false;

        GUILayout.Label(MASResources.BannerTexture, style);
    }

    private void DrawToolbar()
    {
        GUIStyle toolbarButton = MASUILayout.ToolbarButton();
        GUILayoutOption[] options = new GUILayoutOption[1];
        options[0] = GUILayout.MaxWidth(128);

        GUILayout.BeginHorizontal();

        GUIContent simContent = new GUIContent();
        simContent.text = " Simulation";
        simContent.image = MASResources.SimulationIconTexture;
        if (GUILayout.Toggle(m_toolTab == ToolTab.Simulation, simContent, toolbarButton, options))
        {
            m_toolTab = ToolTab.Simulation;
        }

        GUIContent agentContent = new GUIContent();
        agentContent.text = " Agent";
        agentContent.image = MASResources.AgentIconTexture;
        if (GUILayout.Toggle(m_toolTab == ToolTab.Agent, agentContent, toolbarButton, options))
        {
            m_toolTab = ToolTab.Agent;
        }

        GUIContent crowdContent = new GUIContent();
        crowdContent.text = " Crowd";
        crowdContent.image = MASResources.CrowdIconTexture;
        if (GUILayout.Toggle(m_toolTab == ToolTab.Crowd, crowdContent, toolbarButton, options))
        {
            m_toolTab = ToolTab.Crowd;

            if (m_selectedObject)
            {
                // If the user selects the crowd tab when an agent that belongs to a crowd 
                // is currently selected. Then auto-select its parent crowd.
                MASAgent agent = m_selectedObject.GetComponent<MASAgent>();
                if (agent)
                {
                    MASCrowd crowd = agent.GetComponentInParent<MASCrowd>();
                    if (crowd)
                    {
                        Selection.activeGameObject = crowd.gameObject;
                    }
                }
            }
        }

        GUIContent obstacleContent = new GUIContent();
        obstacleContent.text = " Obstacles";
        obstacleContent.image = MASResources.ObstacleIconTexture;
        if (GUILayout.Toggle(m_toolTab == ToolTab.Obstacles, obstacleContent, toolbarButton, options))
        {
            m_toolTab = ToolTab.Obstacles;
        }
        GUILayout.EndHorizontal();
    }

    private void DrawActiveTab()
    {
        switch (m_toolTab)
        {
            case ToolTab.Simulation:                
                m_simulationSettingsPanel.Draw();
                break;
            case ToolTab.Agent:
                m_agentPanel.Draw(this);
                break;
            case ToolTab.Crowd:
                m_crowdPanel.Draw(this);
                break;
            case ToolTab.Obstacles:                
                m_obstaclesPanel.Draw(this);
                break;
            default: break;
        }
    }

    private void DrawFooter()
    {
        GUIStyle areaStyle = new GUIStyle();
        areaStyle.normal.background = MASResources.FooterTexture;
        GUILayout.BeginArea(new Rect(0, position.height - 40, WindowWidth, 40), areaStyle);

        // Run/Stop
        MASUILayout.AddVerticalPadding();
        EditorGUILayout.BeginHorizontal();

        if (!EditorApplication.isPlaying)
        {
            GUIContent content = new GUIContent();
            content.image = MASResources.PlayControlTexture;

            if (MASUILayout.ImageButton(MASResources.PlayControlTexture, 30, 30))
            {
                EditorApplication.ExecuteMenuItem("Edit/Play");
            }
        }
        else
        {
            if (MASUILayout.ImageButton(MASResources.StepControlTexture, 30, 30))
            {
                EditorApplication.ExecuteMenuItem("Edit/Step");
            }
            if (MASUILayout.ImageButton(
                EditorApplication.isPaused ?
                MASResources.ResumeControlTexture :
                MASResources.PauseControlTexture, 30, 30))
            {
                EditorApplication.ExecuteMenuItem("Edit/Pause");
            }
            if (MASUILayout.ImageButton(MASResources.StopControlTexture, 30, 30))
            {
                EditorApplication.ExecuteMenuItem("Edit/Play");
            }
        }

        EditorGUILayout.Space();
        if (MASUILayout.Button("Help", 60, 26))
        {
            ShowHelp();
        }

        EditorGUILayout.EndHorizontal();

        GUILayout.EndArea();
    }

    public void SetAgentProxy(MASAgentProxy agentProxy)
    {
        m_agentPanel.SetAgentProxy(agentProxy);
    }    

    public MASAgentProxy GetAgentProxy()
    {
        return m_agentPanel.GetAgentProxy();    
    }

    public void SetCrowdProxy(MASCrowdProxy crowdProxy)
    {
        m_crowdPanel.SetCrowdProxy(crowdProxy);
    }

    public MASCrowdProxy GetCrowdProxy()
    {
        return m_crowdPanel.GetCrowdProxy();
    }

    public void SetBlockObstacleProxy(MASBlockObstacleProxy blockProxy)
    {
        m_obstaclesPanel.SetBlockObstacleProxy(blockProxy);
    }

    public MASBlockObstacleProxy GetBlockObstacleProxy()
    {
        return m_obstaclesPanel.GetBlockObstacleProxy();
    }

    public void SetCylinderObstacleProxy(MASCylinderObstacleProxy cylinderProxy)
    {
        m_obstaclesPanel.SetCylinderObstacleProxy(cylinderProxy);
    }

    public MASCylinderObstacleProxy GetCylinderObstacleProxy()
    {
        return m_obstaclesPanel.GetCylinderObstacleProxy();
    }

    private void ShowHelp()
    {
        if (m_toolTab == ToolTab.Simulation)
        {
            MASHelpEditor.ShowWindow(MASHelpEditor.MenuTab.Simulation);
        }
        else if (m_toolTab == ToolTab.Agent)
        {
            MASHelpEditor.ShowWindow(MASHelpEditor.MenuTab.Agent);            
        }
        else if (m_toolTab == ToolTab.Crowd)
        {
            MASHelpEditor.ShowWindow(MASHelpEditor.MenuTab.Crowd);
        }
        else if (m_toolTab == ToolTab.Obstacles)
        {
            MASHelpEditor.ShowWindow(MASHelpEditor.MenuTab.Obstacles);
        }        
        else
        {
            MASHelpEditor.ShowWindow();
        }
    }

    public static void NavMeshDirty(bool dirty)
    {
        s_navMeshDirty = dirty;
        PlayerPrefs.SetInt("MAS_NavMeshDirty", s_navMeshDirty ? 1 : 0);
    }
}