﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Editor window to save current agent properties into a preset.
/// </summary>
public class MASSaveAgentPresetEditor : EditorWindow
{
    private static MASAgentProxy s_agentProxy;

    string m_presetName = string.Empty;

    /// <summary>
    /// Display the editor window.
    /// </summary>
    /// <param name="agentProxy">Agent proxy to use to create the preset.</param>
    public static void ShowWindow(MASAgentProxy agentProxy)
    {
        var window = GetWindowWithRect<MASSaveAgentPresetEditor>(new Rect(0, 0, 370, 130), true, "Save Agent As Preset");
        Extensions.CenterOnMainWin(window);

        s_agentProxy = agentProxy;        
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, position.width, position.height), MASResources.BackgroundTexture);

        string presetPath = "Assets/Multi-Agent Simulation/Resources/AgentPresets";

        // Preset name
        MASUILayout.AddVerticalPadding(1);        
        m_presetName = MASUILayout.TextField("Preset Name", m_presetName);        

        bool disableSave = false;
        if (m_presetName == string.Empty)
        {
            // Check a name has been provided
            MASUIWarningBox box = new MASUIWarningBox("Please provide a name.");
            box.Draw();
            disableSave = true;
        }
        else if (m_presetName.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) != -1)
        {        
            // Check name contains valid filename characters
            MASUIWarningBox box = new MASUIWarningBox("Name contains invalid characters.");
            box.Draw();
            disableSave = true;
        }
        else if (AssetDatabase.LoadAssetAtPath(presetPath + "/" + m_presetName + ".prefab", typeof(GameObject)))
        {
            // Check for existing agent presets with the same name
            MASUIWarningBox box = new MASUIWarningBox("An agent preset with that name already exists. Saving will overwrite the existing preset.");
            box.Draw();
        }

        // Cancel
        MASUILayout.AddVerticalPadding(5);
        EditorGUILayout.BeginHorizontal();
        if (MASUILayout.Button("Cancel", 130, 20))
        {
            Close();
        }

        // Save
        GUI.enabled = !disableSave;
        if (MASUILayout.Button("Save", 130, 20))
        {
            if (s_agentProxy == null)
            {
                return;
            }

            // Create folders if they don't already exist
            if (!AssetDatabase.IsValidFolder("Assets/Multi-Agent Simulation"))
            {
                AssetDatabase.CreateFolder("Assets", "Multi-Agent Simulation");
            }

            if (!AssetDatabase.IsValidFolder("Assets/Multi-Agent Simulation/Resources"))
            {
                AssetDatabase.CreateFolder("Assets/Multi-Agent Simulation", "Resources");
            }

            if (!AssetDatabase.IsValidFolder(presetPath))
            {
                AssetDatabase.CreateFolder("Assets/Multi-Agent Simulation/Resources", "AgentPresets");
            }

            // Create prefab
            GameObject agentPresetGameObject = new GameObject();
            MASAgentPreset preset = agentPresetGameObject.AddComponent<MASAgentPreset>();
            preset.AgentProxy = s_agentProxy;
            preset.AgentProxy.PresetName = m_presetName;
            GameObject prefab = PrefabUtility.CreatePrefab(presetPath + "/" + m_presetName + ".prefab", agentPresetGameObject);
            prefab.GetComponent<MASAgentPreset>().AgentProxy.CharacterPrefab = s_agentProxy.CharacterPrefab;
            DestroyImmediate(agentPresetGameObject);
            
            Close();
        }
        GUI.enabled = true;
        EditorGUILayout.EndHorizontal();
    }
}