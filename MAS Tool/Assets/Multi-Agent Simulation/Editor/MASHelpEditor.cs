﻿using UnityEngine;
using UnityEditor;

public class MASHelpEditor : EditorWindow
{
    // The window width and height of the help editor window.
    private static int m_windowWidth = 1024;
    private static int m_windowHeight = 800;

    // The width of the help window navigation pane.
    private int m_navMenuWidth = 250;

    // Scrollable-region scroll positions.
    private Vector2 m_navMenuScrollPos = new Vector2();
    private Vector2 m_mainSectionScrollPos = new Vector2();

    // Tabs to display in the menu window.
    public enum MenuTab
    {
        Simulation,
        Agent,
        Crowd,
        Obstacles,
        About
    }
    static MenuTab s_menuTab = MenuTab.Simulation;

    [MenuItem("Tools/Multi-agent Simulation (MAS)/Help")]
    public static void ShowWindow()
    {
        ShowWindow(MenuTab.Simulation);
    }

    //[MenuItem("Tools/Multi-agent Simulation (MAS)/Demos/Open Shopping Center Demo")]
    //public static void OpenShoppingCenterDemo()
    //{        
    //    UnityEditor.SceneManagement.EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
    //    UnityEditor.SceneManagement.EditorSceneManager.OpenScene("Assets/Multi-agent Simulation/Scenes/ShoppingCenterDemo.unity");
    //}

    /// <summary>
    /// Show the help window.
    /// </summary>
    /// <param name="menuTab">Optionally set a tab to display immediately when the window opens.</param>
    public static void ShowWindow(MenuTab menuTab = MenuTab.Simulation)
    {
        var window = GetWindowWithRect<MASHelpEditor>(new Rect(0, 0, m_windowWidth, m_windowHeight), true, "MAS Help");
        window.titleContent = new GUIContent("MAS Help", MASResources.IconTexture, "Crowd Simulation for Unity");
        Extensions.CenterOnMainWin(window);

        s_menuTab = menuTab;
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, position.width, position.height), MASResources.BackgroundTexture);

        EditorGUILayout.BeginHorizontal();
        DrawNavigationPanel();
        DrawMainSection();
        EditorGUILayout.EndHorizontal();
    }

    /// <summary>
    /// Draw the navigation panel which displays the tabs.
    /// </summary>
    private void DrawNavigationPanel()
    {
        GUIStyle areaStyle = new GUIStyle();
        areaStyle.normal.background = MASResources.SectionFooterTexture;
        GUILayout.BeginArea(new Rect(0, 0, m_navMenuWidth, m_windowHeight), areaStyle);
        GUILayout.EndArea();

        m_navMenuScrollPos = EditorGUILayout.BeginScrollView(m_navMenuScrollPos, GUILayout.Width(m_navMenuWidth), GUILayout.Height(m_windowHeight));

        GUIStyle toolbarButton = MASUILayout.ToolbarButton(TextAnchor.MiddleLeft);
        GUILayoutOption[] options = new GUILayoutOption[1];
        options[0] = GUILayout.MaxWidth(m_navMenuWidth);

        GUIContent tabContent = new GUIContent();
        tabContent.text = " Simulation";
        tabContent.image = MASResources.SimulationIconTexture;

        MenuTab prevMenuTab = s_menuTab;

        if (GUILayout.Toggle(s_menuTab == MenuTab.Simulation, tabContent, toolbarButton, options))
        {
            s_menuTab = MenuTab.Simulation;
        }

        tabContent.text = " Agent";
        tabContent.image = MASResources.AgentIconTexture;
        if (GUILayout.Toggle(s_menuTab == MenuTab.Agent, tabContent, toolbarButton, options))
        {
            s_menuTab = MenuTab.Agent;
        }

        tabContent.text = " Crowd";
        tabContent.image = MASResources.CrowdIconTexture;
        if (GUILayout.Toggle(s_menuTab == MenuTab.Crowd, tabContent, toolbarButton, options))
        {
            s_menuTab = MenuTab.Crowd;
        }

        tabContent.text = " Obstacles";
        tabContent.image = MASResources.ObstacleIconTexture;
        if (GUILayout.Toggle(s_menuTab == MenuTab.Obstacles, tabContent, toolbarButton, options))
        {
            s_menuTab = MenuTab.Obstacles;
        }

        tabContent.text = " About";
        tabContent.image = MASResources.QuestionMarkIconTexture;
        if (GUILayout.Toggle(s_menuTab == MenuTab.About, tabContent, toolbarButton, options))
        {
            s_menuTab = MenuTab.About;
        }

        if (prevMenuTab != s_menuTab)
        {
            // Reset scroll position when changing tabs
            m_mainSectionScrollPos.y = 0;
        }

        EditorGUILayout.EndScrollView();        
    }

    /// <summary>
    /// Draw the main section of the help window.
    /// </summary>
    private void DrawMainSection()
    {
        m_mainSectionScrollPos = EditorGUILayout.BeginScrollView(m_mainSectionScrollPos, GUILayout.Width(m_windowWidth - m_navMenuWidth), GUILayout.Height(m_windowHeight + 15), GUILayout.ExpandWidth(false));

        switch (s_menuTab)
        {
            case MenuTab.Simulation:
                DrawSimulationHelp();
                break;
            case MenuTab.Agent:
                DrawAgentHelp();
                break;
            case MenuTab.Crowd:
                DrawCrowdHelp();
                break;
            case MenuTab.Obstacles:
                DrawObstaclesHelp();
                break;
            case MenuTab.About:
                DrawAboutSection();
                break;
            default:
                DrawAboutSection();
                break;
        }

        EditorGUILayout.EndScrollView();
    }

    private void SectionTitle(string text)
    {
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.fontSize = 26;
        labelStyle.fontStyle = FontStyle.Bold;
        labelStyle.normal.textColor = new Color(0.558f, 0.825f, 1.0f);
        GUILayout.Label(text, labelStyle);
    }

    private void SubSectionTitle(string text)
    {
        MASUILayout.AddVerticalPadding();
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.fontSize = 16;
        labelStyle.fontStyle = FontStyle.BoldAndItalic;
        labelStyle.margin.left = 10;
        labelStyle.normal.textColor = new Color(0.258f, 0.525f, 0.957f);
        GUILayout.Label(text, labelStyle);
    }

    private void SubSectionBody(string text)
    {
        MASUILayout.AddVerticalPadding();
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.fontSize = 14;
        labelStyle.fontStyle = FontStyle.Bold;
        labelStyle.normal.textColor = Color.white;
        labelStyle.margin.left = 20;
        GUILayout.Label(text, labelStyle);
    }

    private void SubSectionImage(Texture2D image)
    {
        MASUILayout.AddVerticalPadding();
        GUIContent content = new GUIContent();
        content.image = image;
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.margin.left = 20;
        GUILayout.Label(content, labelStyle);
    }

    private void SubSectionRemark(string text)
    {
        MASUILayout.AddVerticalPadding();
        GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
        labelStyle.fontSize = 12;
        labelStyle.fontStyle = FontStyle.Italic;
        labelStyle.normal.textColor = Color.grey;
        labelStyle.margin.left = 20;
        GUILayout.Label(text, labelStyle);
    }

    /// <summary>
    /// Draw the simualtion help section.
    /// </summary>
    private void DrawSimulationHelp()
    {
        SectionTitle("Simulation Settings");
        SubSectionTitle("Time Step");
        SubSectionBody("The time step value allows you to alter the rate the simulation progresses. The" +
            "\ngreater this value the faster the simulation will progress.");
        SubSectionRemark("This value must be non-negative and a value of 0 will result in the agents unable not move.");

        SubSectionBody("You can reset the default time step value by clicking \"Reset Time Step\"");
        SubSectionImage(MASResources.HelpResetTimeStep);

        MASUILayout.AddVerticalPadding(4);
        SectionTitle("Actions");
        SubSectionTitle("Destroy Simulation");
        SubSectionBody("This will remove the MAS simulation from the currently active scene. This action\ncannot be undone.");
    }

    /// <summary>
    /// Draw the agent help section.
    /// </summary>
    private void DrawAgentHelp()
    {
        SectionTitle("Agent Settings");
        SubSectionTitle("Position");
        SubSectionBody("The start position of the agent when the simulation begins.");
        SubSectionRemark("The agent y-position will snap to the MASRegion y-position. This is to ensure all agents are aligned with the MASRegion.");
        SubSectionTitle("Radius");
        SubSectionBody("The radius of the circular constraint that represents the bounds of the agent.");
        SubSectionTitle("Max Speed");
        SubSectionBody("The maxium speed an agent can reach during the simulation.");
        SubSectionTitle("Character");
        SubSectionBody("A prefab can be optionally assigned to an agent. The prefab will be instantiated as a child\nonce the agent has been added to the scene.");

        MASUILayout.AddVerticalPadding(4);
        SectionTitle("Adding an Agent to the Scene");
        SubSectionBody("Click the \"Add New Agent\" button located at the bottom of the Agent panel.");
        SubSectionImage(MASResources.HelpAddAgent);
        SubSectionBody("The agent will be automatically selected in the scene view and placed in the frame of the\ncamera. With the agent selected you can continue to edit its properties from the editor\nwindow.");

        MASUILayout.AddVerticalPadding(4);
        SectionTitle("Managing Agent Destinations");
        SubSectionBody("Destinations allow you to specify key locations an agent must travel to during the simulation.");
        SubSectionTitle("Adding a Destination");
        SubSectionBody("Step 1: Click the \"Add\" button located at the top of the Destination section.");
        SubSectionImage(MASResources.HelpAddDestination);
        SubSectionBody("Step 2: Notice a new destination has been added in the box below.");
        SubSectionImage(MASResources.HelpAddDestination2);        
        SubSectionBody("With the agent selected in the scene view the agent will also have a position handle for all\nof its destinations. You can drag the position handle to change the destination.");
        SubSectionImage(MASResources.HelpAddDestination3);
        SubSectionTitle("Removing a Destination");
        SubSectionBody("Step 1: Click the cross located to the right of a destination you wish to remove.");
        SubSectionRemark("You will not have the option to remove agent destinations if the agent part of a crowd.");
        SubSectionImage(MASResources.HelpRemoveDestination);
        SubSectionTitle("Looping Destinations");
        SubSectionBody("The \"Loop Destinations\" toggle allows you to specify whether an agent will repeat travelling\nto its destinations.");

        MASUILayout.AddVerticalPadding(4);
        SectionTitle("Agent Presets");
        SubSectionBody("Agent presets allow you to save an agent to a library. They can be used to quickly load\npreconfigured agents or creating a crowd.");
        SubSectionTitle("Saving an Agent as a Preset");
        SubSectionBody("Step 1: Click the \"Save As Preset\" button located at the bottom of the Agent panel.");
        SubSectionImage(MASResources.HelpSaveAgentPreset);
        SubSectionBody("Step 2: A window will appear asking to name the agent preset. Provide a name and click\n\"Save\".");
        SubSectionRemark("Providing a name of an existing agent preset will overwrite the existing preset.");
        SubSectionImage(MASResources.HelpAgentPresetName);
        SubSectionBody("Now the preset has been added to the library it can be loaded as an individual or form part\nof a crowd.");
        SubSectionTitle("Loading an Agent Preset");        
        SubSectionBody("Step 1: Click the \"Load Agent Preset\" button located at the top of the Agent panel.");
        SubSectionImage(MASResources.HelpLoadAgentPreset);
        SubSectionBody("Step 2: A window will appear displaying a list of all previously saved agent presets. Click the\npreset you wish to load and click the \"Load\" button located at the bottom of the window.");
        SubSectionImage(MASResources.HelpLoadAgentPresetWindow);
        SubSectionBody("This will automatically load the agent properties into the agent panel.");

        MASUILayout.AddVerticalPadding(4);
        SectionTitle("Advanced Settings");
        SubSectionTitle("Neighbour Distance");
        SubSectionBody("The maximum distance to other agents this agent takes into account in the navigation.");
        SubSectionRemark("The larger this number, the longer the running time of the simulation. If the number is too low, the simulation will not be\nsafe.");
        SubSectionTitle("Max Neighbours");
        SubSectionBody("The maximum number of other agents this agent takes into account in the\nnavigation.");
        SubSectionRemark("The larger this number, the longer the running time of the simulation. If the number is too low, the simulation will not be\nsafe.");
        SubSectionTitle("Time Horizon");
        SubSectionBody("The larger this number, the sooner this agent will respond to the presence of other agents,\nbutthe less freedom this agent has in choosing its velocities.");        
        SubSectionTitle("Obstacle Time Horizon");
        SubSectionBody("The larger this number, the sooner this agent will respond to the presence of other obstacles,\nbut the less freedom this agent has in choosing its velocities.");

        MASUILayout.AddVerticalPadding(4);
        SectionTitle("Resetting Agent Properties");
        SubSectionBody("Step 1: Click the \"Reset Agent\" button located at the bottom of the Agent panel.");
        SubSectionImage(MASResources.HelpResetAgent);
        SubSectionBody("Step 2: A window will appear with several agent properties that can be reset. Toggle the\nproperties you wish to reset and click the \"Reset\" button at the bottom of the window.");
        SubSectionImage(MASResources.HelpResetAgentWindow);

        MASUILayout.AddVerticalPadding(5);
    }

    /// <summary>
    /// Draw the crowd help section.
    /// </summary>
    private void DrawCrowdHelp()
    {
        SectionTitle("Crowd Settings");
        SubSectionTitle("Position");
        SubSectionBody("The start position of the crowd when the simulation begins.");
        SubSectionTitle("Density");
        SubSectionBody("The number of agents in the crowd.");
        SubSectionTitle("Spread");
        SubSectionBody("Controls the distance between each agent in the crowd.");

        MASUILayout.AddVerticalPadding(4);
        SectionTitle("Creating a Crowd");
        SubSectionBody("A crowd is formed from one or more agent presets.");
        SubSectionBody("Step 1: Click the \"Add\" button located under the \"Agent Presets\" section in the Crowd panel.");
        SubSectionImage(MASResources.HelpCrowdAddAgentPreset);
        SubSectionBody("Step 2: A window will appear displaying a list of all previously saved agent presets. Click the\npreset you wish to load and click the \"Load\" button located at the bottom of the window.");
        SubSectionImage(MASResources.HelpLoadAgentPresetWindow);
        SubSectionBody("Step 3: Repeat steps 1 and 2 until all the desired agents have been added to the crowd.");
        SubSectionBody("A list of all the agents added to the crowd will populate the box in the \"Agent Presets\"\nsection.");
        SubSectionImage(MASResources.HelpCrowdAgentsList);

        MASUILayout.AddVerticalPadding(4);
        SectionTitle("Adding a Crowd to the Scene");
        SubSectionBody("Click the \"Add New Crowd\" button located at the bottom of the Agent panel.");
        SubSectionImage(MASResources.HelpAddCrowd);
        SubSectionBody("The crowd will be automatically selected in the scene view and placed in the frame of the\ncamera. With the crowd selected you can continue to edit its properties from the editor\nwindow.");
        SubSectionRemark("You must have at least 1 agent preset in the crowd or you will not be able to add a crowd to the scene.");

        MASUILayout.AddVerticalPadding(4);
        SectionTitle("Managing Crowd Destinations");
        SubSectionBody("Destinations allow you to specify key locations an crowd must travel to during the simulation.");
        SubSectionTitle("Adding a Destination");
        SubSectionBody("Step 1: Click the \"Add\" button located at the top of the Destination section.");
        SubSectionImage(MASResources.HelpCrowdAddDestination);
        SubSectionBody("Step 2: Notice a new destination has been added in the box below.");
        SubSectionImage(MASResources.HelpCrowdAddDestination2);
        SubSectionBody("With the crowd selected in the scene view the crowd will also have a position handle for all\nof its destinations. You can drag the position handle to change the destination.");
        SubSectionImage(MASResources.HelpCrowdAddDestination3);
        SubSectionTitle("Removing a Destination");
        SubSectionBody("Step 1: Click the cross located to the right of a destination you wish to remove.");
        SubSectionImage(MASResources.HelpRemoveDestination);
        SubSectionTitle("Looping Destinations");
        SubSectionBody("The \"Loop Destinations\" toggle allows you to specify whether an agent will repeat travelling\nto its destinations.");

        MASUILayout.AddVerticalPadding(4);
        SectionTitle("Resetting Crowd Properties");
        SubSectionBody("Step 1: Click the \"Reset Crowd\" button located at the bottom of the Crowd panel.");
        SubSectionImage(MASResources.HelpResetCrowd);
        SubSectionBody("Step 2: A window will appear with several crowd properties that can be reset. Toggle the\nproperties you wish to reset and click the \"Reset\" button at the bottom of the window.");
        SubSectionImage(MASResources.HelpResetCrowdWindow);

        MASUILayout.AddVerticalPadding(5);
    }

    /// <summary>
    /// Draw the obstacles help section.
    /// </summary>
    private void DrawObstaclesHelp()
    {
        SectionTitle("Obstacle Settings");
        SubSectionTitle("Primitive Type");
        SubSectionBody("The primitive the obstacle represents. An obstacle can either be a box or a cylinder.");
        SubSectionTitle("Position");
        SubSectionBody("The position of the obstacle in the scene.");
        SubSectionTitle("Size");
        SubSectionBody("Controls the dimensions of the obstacle.");
        SubSectionTitle("Include in NavMesh Bake.");
        SubSectionBody("The default behaviour of an obstacle is to be included in the NavMesh bake. This allows agents\nto create a path around the obstacle.");
        SubSectionRemark("If toggled off the simulation will prevent an agent from colliding with the obstacle, however a path will not be created\naround it.");
        SubSectionTitle("Bake Environment");
        SubSectionBody("Bake a Unity NavMesh used by the simulation."); ;

        MASUILayout.AddVerticalPadding(5);
    }

    /// <summary>
    /// Draw the about section.
    /// </summary>
    private void DrawAboutSection()
    {
        SectionTitle("About");
        SubSectionBody("Multi-agent Simulation (MAS) Tool created by Jack Holmes.");
        SubSectionBody("This tool was created to allow the authoring of real-time multi-agent simulation in the Unity\nenvironment.");
        SubSectionBody("MAS Tool depends on the RVO2 library developed by Jur van den Berg, Stephen J. Guy,\nJamie Snape, Ming C. Lin, and Dinesh Manocha, released under the Apache License, Version 2.0." +
            "\nFind out more information about RVO2 at http://gamma.cs.unc.edu/RVO2/");

        MASUILayout.AddVerticalPadding(5);
    }
}