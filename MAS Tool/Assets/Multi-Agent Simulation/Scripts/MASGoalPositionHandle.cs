﻿using UnityEngine;

/// <summary>
/// The position handle for a agents destinations. This allows them to be
/// manipulated in the scene view.
/// </summary>
[ExecuteInEditMode]
public class MASGoalPositionHandle : MonoBehaviour
{
    /// <summary>
    /// The world space position of this handle.
    /// </summary>
    [HideInInspector]
    public Vector3 GoalPosition
    {
        get
        {
            return m_goalPosition;
        }
        set
        {
            m_goalPosition = value;
        }
    }
   
    [SerializeField]
    private Vector3 m_goalPosition = new Vector3(0.0f, 0.0f, 0.0f);

    /// <summary>
    /// The index of the current agent destination.
    /// </summary>
    [HideInInspector]
    public int Index;

    /// <summary>
    /// Update the selected agent destination.
    /// </summary>
    public virtual void UpdateHandle()
    {
        MASAgent agent = GetComponent<MASAgent>();
        if (agent)
        {
            // Restrict the y component to the agents y position
            GoalPosition = new Vector3(GoalPosition.x, agent.transform.position.y, GoalPosition.z);

            // Update the selected destination
           // agent.GoalPositions[Index] = GoalPosition;
        }
    }

    /// <summary>
    /// Translate the selected destination relative to its current position.
    /// </summary>
    /// <param name="translation">The amount to translate by.</param>
    public virtual void Translate(Vector3 translation)
    {
        MASAgent agent = GetComponent<MASAgent>();
        if (agent)
        {
            GoalPosition += translation;
            //agent.GoalPositions[Index] = GoalPosition;            
        }
    }
}