﻿using UnityEngine;

/// <summary>
/// Represents a simulation obstacle for a block.
/// </summary>
public class MASBlockObstacle : MonoBehaviour
{
    /// <summary>
    /// Flag to determine whether or not the obstacle is included in the NavMesh bake.
    /// </summary>
    [SerializeField, HideInInspector]
    private bool m_navMeshBake = false;
    [HideInInspector]
    public bool NavMeshBake
    {
        get
        {
            return m_navMeshBake;
        }
        set
        {
            m_navMeshBake = value;
#if UNITY_EDITOR
            if (m_navMeshBake)
            {
                UnityEditor.GameObjectUtility.SetStaticEditorFlags(gameObject, UnityEditor.StaticEditorFlags.NavigationStatic);
                UnityEditor.GameObjectUtility.SetNavMeshArea(gameObject, UnityEngine.AI.NavMesh.GetAreaFromName("Not Walkable"));
            }
#endif
        }
    }

    private void Start()
    {
        // The mesh renderer is required so that the obstacle can be included in Unity's NavMesh bake.
        // However it must be removed when the game starts so that it is not visible in the game view.
        MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
        if (meshRenderer)
        {
            meshRenderer.enabled = false;
        }
    }
    
#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;        
        DebugExtension.DrawLocalCube(gameObject.transform, new Vector3(1, 1, 1), Color.red);
    }
#endif
}
