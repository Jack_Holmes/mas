﻿using UnityEngine;

/// <summary>
/// Represents a simulation obstacle for a cylinder.
/// </summary>
[ExecuteInEditMode]
public class MASCylinderObstacle : MonoBehaviour
{
    /// <summary>
    /// The resolution of the cylinder.
    /// </summary>
    const int m_numSides = 20;

#if UNITY_EDITOR
    /// <summary>
    /// A list of points which define the top face of the cylinder.
    /// </summary>
    [SerializeField, HideInInspector]
    private Vector3[] m_upperPoints = new Vector3[m_numSides + 1];
#endif

    /// <summary>
    /// A list of points which define the bottom face of the cylinder.
    /// </summary>
    /// <remarks>The lower points are used for the 2D representaion required by the simulation.</remarks>
    [SerializeField, HideInInspector]
    private Vector3[] m_lowerPoints = new Vector3[m_numSides + 1];
    [HideInInspector]
    public Vector3[] Points
    {
        get
        {
            return m_lowerPoints;
        }
    }

    /// <summary>
    /// Flag to determine whether or not the obstacle is included in the NavMesh bake.
    /// </summary>
    [SerializeField, HideInInspector]
    private bool m_navMeshBake = false;
    [HideInInspector]
    public bool NavMeshBake
    {
        get
        {
            return m_navMeshBake;
        }
        set
        {
            m_navMeshBake = value;
#if UNITY_EDITOR
            if (m_navMeshBake)
            {                
                UnityEditor.GameObjectUtility.SetStaticEditorFlags(gameObject, UnityEditor.StaticEditorFlags.NavigationStatic);
                UnityEditor.GameObjectUtility.SetNavMeshArea(gameObject, UnityEngine.AI.NavMesh.GetAreaFromName("Not Walkable"));
            }
#endif
        }
    }

    void Start()
    {
        if (Application.isPlaying)
        {
            // The mesh renderer is required so that the obstacle can be included in Unity's NavMesh bake.
            // However it must be removed when the game starts so that it is not visible in the game view.
            // In this case, an is playing check is required because the cyclinder executes in edit mode.
            MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
            if (meshRenderer)
            {
                meshRenderer.enabled = false;
            }
        }        
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (transform.hasChanged)
        {
            // Recalculate the cylinder points if the transform has been updated.
            // Note: The hasChanged property has to be managed manually.
            // See https://docs.unity3d.com/ScriptReference/Transform-hasChanged.html.            
            transform.hasChanged = false;
            CalculateCylinderPoints();
        }
    }
#endif

    /// <summary>
    /// Update the point lists that represent the cylinder bounds based on the gameobject's transform.
    /// </summary>
#if UNITY_EDITOR
    public void CalculateCylinderPoints()
    {
        int numSides = 20;

        UnityEditor.Handles.color = Color.green;
        for (int i = 0; i < numSides; ++i)
        {
            // Create cylinder circumference with scaling taken into account
            float x = transform.localScale.x / 2 * Mathf.Cos((2 * Mathf.PI * i / numSides));
            float y = transform.position.y;
            float z = transform.localScale.z / 2 * Mathf.Sin((2 * Mathf.PI * i / numSides));

            // Apply rotation
            Quaternion q = Quaternion.AngleAxis(transform.eulerAngles.y, Vector3.up);
            Vector3 point = q * new Vector3(x, y, z);

            // Translate point by offset
            point.x += transform.position.x;
            point.z += transform.position.z;

            m_lowerPoints[i].x = point.x;
            m_lowerPoints[i].y = point.y - transform.localScale.y;
            m_lowerPoints[i].z = point.z;

            m_upperPoints[i].x = point.x;
            m_upperPoints[i].y = point.y + transform.localScale.y;
            m_upperPoints[i].z = point.z;
        }

        // Connect the end to the start
        m_lowerPoints[numSides] = m_lowerPoints[0];
        m_upperPoints[numSides] = m_upperPoints[0];
    }
#endif

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        UnityEditor.Handles.color = Color.red;

        UnityEditor.Handles.DrawPolyLine(m_lowerPoints);
        UnityEditor.Handles.DrawPolyLine(m_upperPoints);

        for (int i = 0; i < m_lowerPoints.Length; ++i)
        {
            UnityEditor.Handles.DrawLine(m_lowerPoints[i], m_upperPoints[i]);
        }
    }
#endif
}
