﻿using UnityEngine;
using System.Collections.Generic;

public class MASCrowd : MonoBehaviour
{
    [HideInInspector]
    public List<MASAgentProxy> AgentProxies = new List<MASAgentProxy>();

    [HideInInspector]
    public bool LoopGoalPositions = MASAgent.LoopGoalPositionsDefault;

    /// <summary>
    /// The number of agents in the crowd.
    /// </summary>
    [SerializeField, HideInInspector]
    private int m_density = 15;
    [HideInInspector]
    public int Density
    {
        get
        {
            return m_density;
        }
    }

    /// <summary>
    /// Controls the distance between the agents.
    /// </summary>
    [SerializeField, HideInInspector]
    private float m_spread = 5;
    [HideInInspector]
    public float Spread
    {
        set
        {
            m_spread = value;

#if UNITY_EDITOR
            foreach (MASCrowdGoalPositionHandle handle in GetComponentsInChildren<MASCrowdGoalPositionHandle>())
            {
                handle.Radius = Spread;
            }

            UnityEditor.SceneView.RepaintAll();
#endif
        }
        get
        {
            return m_spread;
        }
    }

    private void OnDrawGizmos()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
    }

#if UNITY_EDITOR
    /// <summary>
    /// Add agents to the crowd
    /// </summary>
    /// <param name="crowdProxy">The crowd proxy that contains the agents to add.</param>
    public void AddAgents(MASCrowdProxy crowdProxy)
    {
        MASController masController = FindObjectOfType<MASController>();
        if (!masController)
        {
            Debug.LogWarning("MASController not found!");
            return;
        }

        if (AgentProxies.Count <= 0)
        {
            return;
        }
        
        for (int k = 1; k < crowdProxy.Density + 1; ++k)
        {
            MASAgentProxy agentProxy = AgentProxies[k % AgentProxies.Count];

            // Calculate the agent position in the crowd
            float seedOffset = agentProxy.Radius * Spread * 2;
            Vector2 seedPos = MASUtils.SunflowerSeedPosition(k, crowdProxy.Density, seedOffset);
            Vector3 agentPos = new Vector3(seedPos.x + transform.position.x, transform.position.y, seedPos.y + transform.position.z);

            // Add the individual destinations for the agent
            List<Vector3> goals = new List<Vector3>();
            foreach (MASCrowdGoalPositionHandle handle in GetComponentsInChildren<MASCrowdGoalPositionHandle>())
            {
                Vector3 goal = new Vector3(seedPos.x + handle.GoalPosition.x, handle.GoalPosition.y, seedPos.y + handle.GoalPosition.z);
                goals.Add(goal);
            }

            // Add the agent to the simulation
            agentProxy.Position = agentPos;
            agentProxy.GoalPositions = goals;
            GameObject agent = masController.AddAgent(agentProxy);

            agent.transform.parent = transform;
        }

        // Update the density of the crowd
        MASAgent[] agents = GetComponentsInChildren<MASAgent>();
        m_density = agents.Length;

        // Make the crowd the current selection in the scene view
        UnityEditor.Selection.activeGameObject = gameObject;
    }
#endif

    /// <summary>
    /// Invoked by the editor when the crowd density changes.    
    /// </summary>
    /// <remarks>This will cause positions of the agents in the crowd to be recalculated based
    /// on the new density.</remarks>
    /// <param name="crowdProxy">The new representation of the crowd.</param>
#if UNITY_EDITOR
    public void OnDensityChanged(MASCrowdProxy crowdProxy)
    {
        int deltaDensity = crowdProxy.Density - Density;
        int count = Mathf.Abs(deltaDensity);

        if (deltaDensity < 0)
        {
            // Density decreased: Remove agents.
            for (int i = 0; i < count; ++i)
            {
                RemoveAgent();
            }
        }
        else if (deltaDensity > 0)
        {
            // Density increased: Add agents.
            for (int i = 0; i < count; ++i)
            {
                AddAgent(crowdProxy);
            }
        }

        // Recalculate positions
        int agentIndex = 1;
        foreach (MASAgent agent in GetComponentsInChildren<MASAgent>())
        {
            // Calculate the agent position in the crowd
            float seedOffset = agent.Radius * Spread * 2;
            Vector2 seedPos = MASUtils.SunflowerSeedPosition(agentIndex, crowdProxy.Density, seedOffset);
            Vector3 agentPos = new Vector3(seedPos.x + transform.position.x, transform.position.y, seedPos.y + transform.position.z);

            agent.transform.position = agentPos;

            // Recalculate goal positions
            MASCrowdGoalPositionHandle[] goalHandles = GetComponentsInChildren<MASCrowdGoalPositionHandle>();
            List<Vector3> goalPositions = new List<Vector3>();
            foreach (MASCrowdGoalPositionHandle handle in goalHandles)
            {
                goalPositions.Add(new Vector3(seedPos.x + handle.GoalPosition.x, handle.GoalPosition.y, seedPos.y + handle.GoalPosition.z));
            }

            agent.RecalculateGoalPositions(goalPositions);

            //agent.RecalculateEditorHandles();

            ++agentIndex;
        }
    }
#endif

    /// <summary>
    /// Invoked by the editor when the crowd spread changes.    
    /// </summary>
    /// <remarks>This will recalculate the distance between agents in the crowd.</remarks>
    /// <param name="newSpread">The new seperation distance between agents.</param>
#if UNITY_EDITOR
    public void OnSpreadChanged(float newSpread)
    {
        float deltaSpread = newSpread - Spread;

        foreach (MASAgent agent in GetComponentsInChildren<MASAgent>())
        {
            Vector3 point = agent.transform.position;
            float xDist = point.x - transform.position.x;
            float zDist = point.z - transform.position.z;

            float xPerc = Mathf.Abs(xDist) / Spread;
            float yPerc = Mathf.Abs(zDist) / Spread;

            float translateX = xDist > 0 ? deltaSpread * xPerc : -deltaSpread * xPerc;
            float translateZ = zDist > 0 ? deltaSpread * yPerc : -deltaSpread * yPerc;
            point.x += translateX;
            point.z += translateZ;
            agent.transform.position = point;

            foreach (MASGoalPositionHandle handle in agent.GetComponentsInChildren<MASGoalPositionHandle>())
            {
                handle.Translate(new Vector3(translateX, 0, translateZ));
            }
        }
    }
#endif

#if UNITY_EDITOR
    /// <summary>
    /// Checks if the appropriate agent proxies are being used in the crowd.
    /// </summary>
    public void OnAgentProxiesChanged()
    {
        int agentIndex = 0;
        foreach (MASAgent agent in GetComponentsInChildren<MASAgent>())
        {
            MASAgentProxy agentProxy = AgentProxies[agentIndex % AgentProxies.Count];

            agent.Radius          = agentProxy.Radius;
            agent.MaxSpeed        = agentProxy.MaxSpeed;
            agent.CharacterPrefab = agentProxy.CharacterPrefab;            
            agent.NeighborDist    = agentProxy.NeighborDist;
            agent.MaxNeighbors    = agentProxy.MaxNeighbors;
            agent.TimeHorizon     = agentProxy.TimeHorizon;
            agent.TimeHorizonObst = agentProxy.TimeHorizonObst;

            ++agentIndex;
        }
    }
#endif

    /// <summary>
    /// Invoked by the editor when the crowd destinations are changed.
    /// </summary>
    /// <remarks></remarks>
    /// <param name="newGoalPositions">A list of new crowd destinations.</param>
    #if UNITY_EDITOR
    public void OnGoalPositionsChanged(List<Vector3> newGoalPositions)
    {
        if (newGoalPositions.Count < 0)
        {            
            return;
        }

        // Accumulate list of current goal positions
        List<Vector3> goalPositions = new List<Vector3>();
        foreach (MASCrowdGoalPositionHandle handle in GetComponentsInChildren<MASCrowdGoalPositionHandle>())
        {
            goalPositions.Add(handle.GoalPosition);
        }

        // Find the delta amount to determine if we are removing or adding goal positions
        int deltaGoalPositions = newGoalPositions.Count - goalPositions.Count;
        int count = Mathf.Abs(deltaGoalPositions);

        // Add/Remove goal positions
        if (deltaGoalPositions < 0)
        {
            for (int i = 0; i < count; ++i)
            {
                RemoveGoalPosition();
            }
        }
        else if (deltaGoalPositions > 0)
        {

            for (int i = 0; i < count; ++i)
            {
                Vector3 goalPos = newGoalPositions[goalPositions.Count + i];
                AddGoalPosition(goalPos);
            }
        }

        // Validity check: The current amount of handles should match the specifed new goal positions amount
        MASCrowdGoalPositionHandle[] handles = GetComponentsInChildren<MASCrowdGoalPositionHandle>();
        if (handles.Length != newGoalPositions.Count)
        {
            Debug.LogWarning("Validity check failed!");
            return;
        }

        // Update any previous goal positions whos' position may of changed
        for (int i = 0; i < handles.Length; ++i)
        {
            if (handles[i].GoalPosition != newGoalPositions[i])
            {
                handles[i].GoalPosition = newGoalPositions[i];
                foreach (MASAgent agent in GetComponentsInChildren<MASAgent>())
                {
                    Vector3 agentLocalPosition = agent.transform.localPosition;
                    Vector3 goal = new Vector3(
                        handles[i].GoalPosition.x + agentLocalPosition.x,
                        handles[i].GoalPosition.y,
                         handles[i].GoalPosition.z + agentLocalPosition.z);

                    agent.UpdateGoalPosition(handles[i].Index, goal);                   
                }
            }
        }
    }
#endif

    /// <summary>
    /// Add an agent to the crowd.
    /// </summary>
    /// <param name="crowdProxy">The representation of the crowd.</param>
#if UNITY_EDITOR
    private void AddAgent(MASCrowdProxy crowdProxy)
    {
        MASController masController = FindObjectOfType<MASController>();
        if (!masController)
        {
            Debug.LogWarning("MASController not found!");
            return;
        }

        int agentIndex = Density + 1;

        MASAgentProxy agentProxy = AgentProxies[agentIndex % AgentProxies.Count];

        // Calculate the agent position in the crowd
        float seedOffset = agentProxy.Radius * Spread * 2;
        Vector2 seedPos = MASUtils.SunflowerSeedPosition(agentIndex, crowdProxy.Density, seedOffset);
        Vector3 agentPos = new Vector3(seedPos.x + transform.position.x, transform.position.y, seedPos.y + transform.position.z);

        // Add the individual destinations for the agent
        List<Vector3> goals = new List<Vector3>();
        foreach (MASCrowdGoalPositionHandle handle in GetComponentsInChildren<MASCrowdGoalPositionHandle>())
        {
            Vector3 goal = new Vector3(seedPos.x + handle.GoalPosition.x, handle.GoalPosition.y, seedPos.y + handle.GoalPosition.z);
            goals.Add(goal);
        }
        
        agentProxy.Position = agentPos;
        agentProxy.GoalPositions = goals;        
        agentProxy.LoopGoalPositions = crowdProxy.LoopGoalPositions;        

        // Add the agent to the simulation
        GameObject agentObject = masController.AddAgent(agentProxy, false);

        agentObject.transform.parent = transform;
       
        // Update the crowd denstity
        MASAgent[] agents = GetComponentsInChildren<MASAgent>();
        m_density = agents.Length;

        UnityEditor.Selection.activeGameObject = gameObject;
    }
#endif

    /// <summary>
    /// Removes a individual agent from the crowd.
    /// </summary>    
#if UNITY_EDITOR
    private void RemoveAgent()
    {
        MASAgent[] agents = GetComponentsInChildren<MASAgent>();
        if (agents.Length <= 0)
        {
            return;
        }

        DestroyImmediate(agents[agents.Length - 1].gameObject);

        agents = GetComponentsInChildren<MASAgent>();
        m_density = agents.Length;
    }
#endif

    /// <summary>
    /// Add a new destination to the crowd.
    /// </summary>
    /// <remarks>The destination will be added to the end of the list of destinations.</remarks>
    /// <param name="goalPosition">The new destination.</param>
#if UNITY_EDITOR
    private void AddGoalPosition(Vector3 goalPosition)
    {
        MASCrowdGoalPositionHandle[] handles = GetComponentsInChildren<MASCrowdGoalPositionHandle>();

        // Create an editor handle so that the user can change the goal position from the scene view
        MASCrowdGoalPositionHandle handle = gameObject.AddComponent<MASCrowdGoalPositionHandle>();
        handle.Index = handles.Length; // This will be added to the end, so its index is the current length
        handle.Radius = Spread;
        handle.GoalPosition = goalPosition;

        foreach (MASAgent agent in GetComponentsInChildren<MASAgent>())
        {
            // Place the goal position of an individual agent in a crowd relative to the crowd position
            // taking into account it's local position as an offet.
            Vector3 agentLocalPosition = agent.transform.localPosition;
            Vector3 goal = new Vector3(
                handle.GoalPosition.x + agentLocalPosition.x,
                handle.GoalPosition.y,
                handle.GoalPosition.z + agentLocalPosition.z);

            agent.AddGoalPosition(goal);
        }
    }
#endif

    /// <summary>
    /// Remove a destination from the crowd.
    /// </summary>
    /// <remarks>Removes the last destination.</remarks>  
#if UNITY_EDITOR
    private void RemoveGoalPosition()
    {
        MASCrowdGoalPositionHandle[] handles = GetComponentsInChildren<MASCrowdGoalPositionHandle>();
        if (handles.Length <= 0)
        {
            return;
        }

        MASCrowdGoalPositionHandle lastHandle = handles[handles.Length - 1];

        foreach (MASAgent agent in GetComponentsInChildren<MASAgent>())
        {
            //foreach (MASGoalPositionHandle agentGoalHandle in agent.GetComponents<MASGoalPositionHandle>())
            //{
            //    if (agentGoalHandle.Index == lastHandle.Index)
            //    {
            //        DestroyImmediate(agentGoalHandle);
            //        agent.GoalPositions.RemoveAt(agent.GoalPositions.Count - 1);
            //    }
            //}

            agent.RemoveGoalPosition(handles.Length - 1);
        }

        DestroyImmediate(lastHandle);
    }
#endif
}
