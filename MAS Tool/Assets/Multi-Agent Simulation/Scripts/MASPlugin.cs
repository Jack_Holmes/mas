﻿using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
public struct MASVec2
{
    public float x;
    public float y;
}

public static class MASPlugin
{
#region REGION__MASPLUGIN_IMPORT
    /**
	* \brief      Create the RVO Simulator. This must be invoked
	*             before running the simulation.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern void MAS_Initialise();    

    /**
	* \brief      Destroys the RVO Simulator.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern void MAS_Shutdown();    

    /**
	* \brief      Lets the simulator perform a simulation step and updates the
	*             two-dimensional position and two-dimensional velocity of
	*             each agent.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern void MAS_StepSimulation();    

    /**
	* \brief      Adds a new agent to the simulation.
	* \param      position        The two-dimensional starting position of
	*                             this agent.
	* \param      neighborDist    The maximum distance (center point to
	*                             center point) to other agents this agent
	*                             takes into account in the navigation. The
	*                             larger this number, the longer the running
	*                             time of the simulation. If the number is too
	*                             low, the simulation will not be safe.
	*                             Must be non-negative.
	* \param      maxNeighbors    The maximum number of other agents this
	*                             agent takes into account in the navigation.
	*                             The larger this number, the longer the
	*                             running time of the simulation. If the
	*                             number is too low, the simulation will not
	*                             be safe.
	* \param      timeHorizon     The minimal amount of time for which this
	*                             agent's velocities that are computed by the
	*                             simulation are safe with respect to other
	*                             agents. The larger this number, the sooner
	*                             this agent will respond to the presence of
	*                             other agents, but the less freedom this
	*                             agent has in choosing its velocities.
	*                             Must be positive.
	* \param      timeHorizonObst The minimal amount of time for which this
	*                             agent's velocities that are computed by the
	*                             simulation are safe with respect to
	*                             obstacles. The larger this number, the
	*                             sooner this agent will respond to the
	*                             presence of obstacles, but the less freedom
	*                             this agent has in choosing its velocities.
	*                             Must be positive.
	* \param      radius          The radius of this agent.
	*                             Must be non-negative.
	* \param      maxSpeed        The maximum speed of this agent.
	*                             Must be non-negative.
	* \param      velocity        The initial two-dimensional linear velocity
	*                             of this agent (optional).
	* \return     The number of the agent.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern System.UInt64 MAS_AddAgentEx(MASVec2 position, float neighborDist, int maxNeighbors, float timeHorizon, float timeHorizonObst, float radius, float maxSpeed, MASVec2 velocity);       

    /**
	* \brief      Returns the count of agents in the simulation.
	* \return     The count of agents in the simulation.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern System.UInt64 MAS_GetNumAgents();

    /**
	* \brief      Returns the two-dimensional position of a specified
	*             agent.
	* \param      agentNo         The number of the agent whose
	*                             two-dimensional position is to be retrieved.
	* \return     The present two-dimensional position of the (center of the)
	*             agent.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern MASVec2 MAS_GetAgentPosition(System.UInt64 agentNo);

    /**
	* \brief      Returns the two-dimensional linear velocity of a
	*             specified agent.
	* \param      agentNo         The number of the agent whose
	*                             two-dimensional linear velocity is to be
	*                             retrieved.
	* \return     The present two-dimensional linear velocity of the agent.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern MASVec2 MAS_GetAgentVelocity(System.UInt64 agentNo);

    /**
	* \brief      Sets the two-dimensional preferred velocity of a
	*             specified agent.
	* \param      agentNo         The number of the agent whose
	*                             two-dimensional preferred velocity is to be
	*                             modified.
	* \param      prefVelocity    The replacement of the two-dimensional
	*                             preferred velocity.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern void MAS_SetAgentPrefVelocity(System.UInt64 agentNo, MASVec2 prefVelocity);

    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern void MAS_SetAgentVelocity(System.UInt64 agentNo, MASVec2 velocity);

    /**
	* \brief      Returns the two-dimensional preferred velocity of a
	*             specified agent.
	* \param      agentNo         The number of the agent whose
	*                             two-dimensional preferred velocity is to be
	*                             retrieved.
	* \return     The present two-dimensional preferred velocity of the agent.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern MASVec2 MAS_GetAgentPrefVelocity(System.UInt64 agentNo);

    /**
	* \brief      Sets the time step of the simulation.
	* \param      timeStep        The time step of the simulation.
	*                             Must be positive.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern void MAS_SetTimeStep(float timeStep);

    /**
	* \brief      Notifies the simulation new vertices are about to be
    *             added.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern void MAS_BeginAddObstacle();

    /**
	* \brief      Adds a vertex ot the obstacle.
    * \param      x        The vertex x position.
    * \param      y        The vertex y position.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern void MAS_AddObstacleVertex(float x, float y);

    /**
	* \brief      Notifies the simulation we have finished adding
    *             vertices.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern void MAS_EndAddObstacle();

    /**
	* \brief      Processes the obstacles that have been added so that they
	*             are accounted for in the simulation.
	* \note       Obstacles added to the simulation after this function has
	*             been called are not accounted for in the simulation.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern void MAS_ProcessObstacles();

    /**
	* \brief      Performs a visibility query between the two specified
	*             points with respect to the obstacles
	* \param      point1          The first point of the query.
	* \param      point2          The second point of the query.
	* \param      radius          The minimal distance between the line
	*                             connecting the two points and the obstacles
	*                             in order for the points to be mutually
	*                             visible (optional). Must be non-negative.
	* \return     A boolean specifying whether the two points are mutually
	*             visible. Returns true when the obstacles have not been
	*             processed.
	*/
    [DllImport("MASPlugin", CallingConvention = CallingConvention.Cdecl)]
    private static extern bool MAS_QueryVisibility(MASVec2 point1, MASVec2 point2, float radius = 0.0f);
    #endregion

    public static void Initialise()
    {
        MAS_Initialise();
    }

    public static void Shutdown()
    {
        MAS_Shutdown();
    }

    public static void StepSimulation()
    {
        MAS_StepSimulation();
    }        

    public static System.UInt64 AddAgentEx(MASVec2 position, float neighborDist, int maxNeighbors, float timeHorizon, float timeHorizonObst, float radius, float maxSpeed, MASVec2 velocity)
    {
        return MAS_AddAgentEx(
            position,
            neighborDist,
            maxNeighbors,
            timeHorizon,
            timeHorizonObst,
            radius,
            maxSpeed,
            velocity);
    }   

    public static System.UInt64 GetNumAgents()
    {
        return MAS_GetNumAgents();
    }

    public static MASVec2 GetAgentPosition(System.UInt64 agentNo)
    {
        return MAS_GetAgentPosition(agentNo);
    }

    public static MASVec2 GetAgentVelocity(System.UInt64 agentNo)
    {
        return MAS_GetAgentVelocity(agentNo);
    }

    public static void SetAgentPrefVelocity(System.UInt64 agentNo, MASVec2 prefVelocity)
    {
        MAS_SetAgentPrefVelocity(agentNo, prefVelocity);
    }

    public static void SetAgentVelocity(System.UInt64 agentNo, MASVec2 velocity)
    {
        MAS_SetAgentVelocity(agentNo, velocity);
    }

    public static MASVec2 GetAgentPrefVelocity(System.UInt64 agentNo)
    {
        return MAS_GetAgentPrefVelocity(agentNo);
    }

    public static void SetTimeStep(float timeStep)
    {
        MAS_SetTimeStep(timeStep);
    }

    public static void BeginAddObstacle()
    {
        MAS_BeginAddObstacle();
    }

    public static void AddObstacleVertex(float x, float y)
    {
        MAS_AddObstacleVertex(x, y);
    }


    public static void EndAddObstacle()
    {
        MAS_EndAddObstacle();
    }

    public static void ProcessObstacles()
    {
        MAS_ProcessObstacles();
    }   

    public static bool QueryVisibility(MASVec2 point1, MASVec2 point2, float radius = 0.0f)
    {
        return MAS_QueryVisibility(point1, point2, radius);
    }
}