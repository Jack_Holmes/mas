﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class MASFirstPersonPlayerController : MonoBehaviour
{
    System.UInt64 m_agentNo;
    bool m_masControllerPresent = true;

    // Use this for initialization
    void Start()
    {
        GameObject masControllerGO =  GameObject.Find("MAS_Controller");
        if (!masControllerGO)
        {
            Debug.LogError("MASFirstPersonPlayerController requires MAS Controller to be active in scene.");
            m_masControllerPresent = false;
            return;
        }

        MASVec2 pos;
        pos.x = transform.position.x;
        pos.y = transform.position.z;

        MASVec2 vel;
        vel.x = 0;
        vel.y = 0;

        m_agentNo = MASPlugin.AddAgentEx(pos, 0, 0, 0, 0.1f, GetComponent<CharacterController>().radius, 10, vel);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!m_masControllerPresent)
        {
            return;
        }

        var velocity = GetComponent<CharacterController>().velocity;
        //velocity.Normalize();

        MASVec2 vel;
        vel.x = velocity.x;
        vel.y = velocity.z;
        MASPlugin.SetAgentPrefVelocity(m_agentNo, vel);

        MASVec2 agentPos = MASPlugin.GetAgentPosition(m_agentNo);
        gameObject.transform.position = new Vector3(agentPos.x, gameObject.transform.position.y, agentPos.y);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        UnityEditor.Handles.color = Color.green;
        UnityEditor.Handles.DrawSolidDisc(transform.position, Vector3.up, GetComponent<CharacterController>().radius);       
    }
#endif
}
