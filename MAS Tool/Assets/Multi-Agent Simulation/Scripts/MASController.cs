﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Main interface to communicate between the editor and the plugin.
/// </summary>
public class MASController : MonoBehaviour
{
    /// <summary>
    /// Simualtion settings.
    /// </summary>
    [SerializeField]
    MASSimulationSettings m_simulationSettings = new MASSimulationSettings();

    /// <summary>
    /// Add an agent to the simulation.
    /// </summary>
    /// <param name="agentProxy">Proxy representation of the agent to add.</param>
    /// <returns>The agent gameObject.</returns>
#if UNITY_EDITOR
    public GameObject AddAgent(MASAgentProxy agentProxy, bool frameSelected=true)
    {
        // Adding agents at runtime is not supported.
        if (Application.isPlaying)
        {
            return null;
        }

        // Create a concrete gameobject to represent the agent.
        GameObject agentGameObject = new GameObject();
        agentGameObject.transform.parent = transform;        
        agentGameObject.transform.position = agentProxy.Position;        
        agentGameObject.name = "MAS_Agent";

        MASVec2 pos;
        pos.x = agentGameObject.transform.position.x;
        pos.y = agentGameObject.transform.position.z;

        MASVec2 vel;
        vel.x = 0;
        vel.y = 0;

        // Add the agent script and initialise the agent properties.
        MASAgent agent = agentGameObject.AddComponent<MASAgent>();
        agent.NeighborDist = agentProxy.NeighborDist;
        agent.MaxNeighbors = agentProxy.MaxNeighbors;
        agent.TimeHorizon = agentProxy.TimeHorizon;
        agent.TimeHorizonObst = agentProxy.TimeHorizonObst;
        agent.Radius = agentProxy.Radius;
        agent.MaxSpeed = agentProxy.MaxSpeed;
        agent.Velocity = vel;
        agent.RecalculateGoalPositions(agentProxy.GoalPositions);
        agent.LoopGoalPositions = agentProxy.LoopGoalPositions;        
        agent.CharacterPrefab = agentProxy.CharacterPrefab;

        // Default the selection to the newly added agent in the scene view.
        Selection.activeGameObject = agentGameObject;
        if (frameSelected)
        {
            EditorApplication.ExecuteMenuItem("Edit/Frame Selected");
        }

        return agentGameObject;
    }
#endif

#if UNITY_EDITOR
    /// <summary>
    /// Add a crowd to the simulation.
    /// </summary>
    /// <param name="crowdProxy">Proxy representation of the crowd to add.</param>
    /// <returns></returns>
    public GameObject AddCrowd(MASCrowdProxy crowdProxy)
    {
        // Adding crowds at runtime is not supported.
        if (Application.isPlaying)
        {
            return null;
        }

        // Create a concrete gameobject to represent the crowd.
        GameObject crowdGameObject = new GameObject();
        crowdGameObject.transform.position = crowdProxy.Position;
        crowdGameObject.transform.parent = transform;
        crowdGameObject.name = "MAS_Crowd";       

        // Add the crowd script and initialise add the crowd destinations.
        MASCrowd crowd = crowdGameObject.AddComponent<MASCrowd>();
        crowd.Spread = crowdProxy.Spread;
        crowd.AgentProxies = crowdProxy.AgentProxies;

        int goalCount = 0;
        foreach (Vector3 goalPosition in crowdProxy.GoalPositions)
        {
            MASCrowdGoalPositionHandle handle = crowdGameObject.AddComponent<MASCrowdGoalPositionHandle>();
            handle.Index = goalCount;
            handle.Radius = crowdProxy.Spread;
            handle.GoalPosition = goalPosition;
            ++goalCount;
        }

        crowd.AddAgents(crowdProxy);

        // Default the selection to the newly added crowd in the scene view.
        Selection.activeGameObject = crowdGameObject;
        EditorApplication.ExecuteMenuItem("Edit/Frame Selected");

        return crowdGameObject;
    }
#endif

    /// <summary>
    /// Add a block obstacle to the simulation.
    /// </summary>
    /// <param name="pos">The position of the block in world space.</param>
    /// <param name="size">The x, y, z size of the block.</param>
    /// <param name="navMeshBake">The NavMesh bake flag.</param>
#if UNITY_EDITOR
    public void AddBlockObstacle(Vector3 pos, Vector3 size, bool navMeshBake)
    {
        // Adding block obstacles at runtime is not supported.
        if (Application.isPlaying)
        {
            return;
        }

        // Create a concrete gameobject to represent the block obstacle.
        GameObject block = GameObject.CreatePrimitive(PrimitiveType.Cube);
        block.name = "MAS_Block_Obstacle";
        block.transform.parent = transform;        
        
        Collider collider = block.GetComponent<Collider>();
        if (collider)
        {
            DestroyImmediate(collider);
        }

        // Update the mesh renderer and add a custom material to the obstacle.
        MeshRenderer renderer = block.GetComponent<MeshRenderer>();
        if (renderer)
        {
            renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            renderer.receiveShadows = false;
            Material mat = Resources.Load("Materials/Obstacle", typeof(Material)) as Material;
            renderer.material = mat;
        }

        // Add the block obstcle script and initialise its properties.
        MASBlockObstacle obstacle = block.AddComponent<MASBlockObstacle>();
        obstacle.transform.position = pos;
        obstacle.transform.localScale = size;
        obstacle.NavMeshBake = navMeshBake;

        // Set the newly added block obstacle as the currently selected object in the scene view.
        Selection.activeGameObject = block;
        EditorApplication.ExecuteMenuItem("Edit/Frame Selected");
    }
#endif

    /// <summary>
    /// Add a cylinder obstacle to the simulation.
    /// </summary>
    /// <param name="pos">The position of the cylinder in world space.</param>
    /// <param name="scale">The x, y, z size of the cylinder.</param>
    /// <param name="navMeshBake">The NavMesh bake flag.</param>
#if UNITY_EDITOR
    public void AddCylinderObstacle(Vector3 pos, Vector3 scale, bool navMeshBake)
    {
        // Adding cylinder obstacles at runtime is not supported.
        if (Application.isPlaying)
        {
            return;
        }

        // Create a concrete gameobject to represent the cylinder obstacle.
        GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        cylinder.name = "MAS_Cylinder_Obstacle";
        cylinder.transform.parent = transform;

        Collider collider = cylinder.GetComponent<Collider>();
        if (collider)
        {
            DestroyImmediate(collider);
        }

        // Update the mesh renderer and add a custom material to the obstacle.
        MeshRenderer renderer = cylinder.GetComponent<MeshRenderer>();
        if (renderer)
        {
            renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            renderer.receiveShadows = false;
            Material mat = Resources.Load("Materials/Obstacle", typeof(Material)) as Material;
            renderer.material = mat;
        }

        // Add the cylinder obstacle script and initialise its properties.
        MASCylinderObstacle obstacle = cylinder.AddComponent<MASCylinderObstacle>();
        obstacle.transform.position = pos;
        obstacle.transform.localScale = scale;
        obstacle.NavMeshBake = navMeshBake;

        // Set the newly added cylinder obstacle as the currently selected object in the scene view.
        Selection.activeGameObject = cylinder;
        EditorApplication.ExecuteMenuItem("Edit/Frame Selected");
    }
#endif

    /// <summary>
    /// Set the rate at which the agents simualte.
    /// </summary>
    /// <param name="timeStep">The simulation rate.</param>
#if UNITY_EDITOR
    public void SetSimulationTimeStep(float timeStep)
    {
        m_simulationSettings.TimeStep = timeStep;

        // We can only set the time step once the dll has loaded.
        if (Application.isPlaying)
        {
            MASPlugin.SetTimeStep(m_simulationSettings.TimeStep);
        }
    }
#endif

    private void Start()
    {
        SetupSimulation();
    }

    private void SetupSimulation()
    {
        // Add block obstacles
        foreach (MASBlockObstacle block in gameObject.GetComponentsInChildren<MASBlockObstacle>())
        {
            MASPlugin.BeginAddObstacle();

            // Create the 4 points that define the block obstacle in world space.
            Vector3 size = new Vector3(1, 1, 1);
            Vector3 v1 = block.transform.TransformPoint(((-size) * 0.5f));
            Vector3 v2 = block.transform.TransformPoint((new Vector3(size.x, -size.y, -size.z) * 0.5f));
            Vector3 v3 = block.transform.TransformPoint((new Vector3(size.x, -size.y, size.z) * 0.5f));
            Vector3 v4 = block.transform.TransformPoint((new Vector3(-size.x, -size.y, size.z) * 0.5f));

            // Add the block vertices to the simulation.
            MASPlugin.AddObstacleVertex(v1.x, v1.z);
            MASPlugin.AddObstacleVertex(v2.x, v2.z);
            MASPlugin.AddObstacleVertex(v3.x, v3.z);
            MASPlugin.AddObstacleVertex(v4.x, v4.z);

            MASPlugin.EndAddObstacle();
        }

        // Add cylinder obstacles
        foreach (MASCylinderObstacle cylinder in gameObject.GetComponentsInChildren<MASCylinderObstacle>())
        {
            MASPlugin.BeginAddObstacle();

            // Add the cylinder vertices to the simulation.
            for (int i = 0; i < cylinder.Points.Length; ++i)
            {
                MASPlugin.AddObstacleVertex(cylinder.Points[i].x, cylinder.Points[i].z);
            }

            MASPlugin.EndAddObstacle();
        }

        // Allow the simulation to process all obstacles once they have been added.
        MASPlugin.ProcessObstacles();

        // Add agents
        foreach (MASAgent agent in gameObject.GetComponentsInChildren<MASAgent>())
        {
            MASVec2 pos;
            pos.x = agent.transform.position.x;
            pos.y = agent.transform.position.z;

            MASVec2 vel;
            vel.x = agent.Velocity.x;
            vel.y = agent.Velocity.y;            

            agent.AgentNo = MASPlugin.AddAgentEx(
                pos,
                agent.NeighborDist,
                agent.MaxNeighbors,
                agent.TimeHorizon,
                agent.TimeHorizonObst,
                agent.Radius,
                agent.MaxSpeed,
                vel);
        }
    }

    private void Awake()
    {
        m_simulationSettings.TimeStep = PlayerPrefs.GetFloat("MAS_TimeStep", MASSimulationSettings.TimeStepDefault);

        MASPlugin.Initialise();

        MASPlugin.SetTimeStep(m_simulationSettings.TimeStep);
    }

    private void FixedUpdate()
    {
        foreach (MASAgent agent in gameObject.GetComponentsInChildren<MASAgent>())
        {
            // Update the agent gameobject position, velocity and preferred velocity
            // using the calculated values from the simulation.
            MASVec2 agentPos = MASPlugin.GetAgentPosition(agent.AgentNo);
            agent.gameObject.transform.position = new Vector3(agentPos.x, agent.gameObject.transform.position.y, agentPos.y);

            agent.Velocity = MASPlugin.GetAgentVelocity(agent.AgentNo);
            agent.PrefVelocity = MASPlugin.GetAgentPrefVelocity(agent.AgentNo);
        }        

        // Let the simulator update the position and velocities of the agents.
        MASPlugin.StepSimulation();
    }

    private void OnApplicationQuit()
    {
        // Destroy the simulator.
        MASPlugin.Shutdown();
    }
}