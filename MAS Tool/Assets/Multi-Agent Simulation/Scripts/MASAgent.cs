﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Represents an indivual agent in the simulation.
/// </summary>
public class MASAgent : MonoBehaviour
{
    /// <summary>
    /// Supported navigation types.
    /// </summary>
    public enum MASNavigationType
    {
        NavMesh
    }

    // Default agent properties.
    public const float NeighborDistDefault     = 15.0f;
    public const int MaxNeighborsDefault       = 10;
    public const float TimeHorizonDefault      = 5.0f;
    public const float TimeHorizonObstDefault  = 5.0f;
    public const float RadiusDefault           = 0.6f;
    public const float MaxSpeedDeault          = 0.4f;
    public const bool LoopGoalPositionsDefault = true;    

    /// <summary>
    /// A unique number assigned to the agent for the current simulation.
    /// </summary>
    [HideInInspector]    
    public System.UInt64 AgentNo;

    /// <summary>
    /// The maximum distance to other agents this agent takes into account in the navigation. 
    /// The larger this number, the longer the running time of the simulation. If the number
    /// is too low, the simulation will not be safe.
    /// </summary>
    [HideInInspector]
    public float NeighborDist = NeighborDistDefault;

    /// <summary>
    /// The maximum number of other agents this agent takes into account in the navigation. 
    /// The larger this number, the longer the running time of the simulation. If the number
    /// is too low, the simulation will not be safe.
    /// </summary>
    [HideInInspector]
    public int MaxNeighbors = MaxNeighborsDefault;

    /// <summary>
    /// The minimal amount of time for which this agent's velocities that are computed by the
    /// simulation are safe with respect to other agents. The larger this number, the sooner
    /// this agent will respond to the presence of other agents, but the less freedom this
    /// agent has in choosing its velocities.
    /// </summary>
    [HideInInspector]
    public float TimeHorizon = TimeHorizonDefault;

    /// <summary>
    /// The minimal amount of time for which this agent's velocities that are computed by the
    /// simulation are safe with respect to obstacles. The larger this number, the sooner this
    /// agent will respond to the presence of obstacles, but the less freedom this agent has
    /// in choosing its velocities.
    /// </summary>
    [HideInInspector]
    public float TimeHorizonObst = TimeHorizonObstDefault;

    /// <summary>
    /// The radius of the circular constraint that represents the bounds of the agent. 
    /// </summary>
    [HideInInspector]
    public float Radius = RadiusDefault;

    /// <summary>
    /// The maxium speed an agent can reach during the simulation.
    /// </summary>
    [HideInInspector]
    public float MaxSpeed = MaxSpeedDeault;

    /// <summary>
    /// The current velocity of the agent.
    /// </summary>
    [HideInInspector]
    public MASVec2 Velocity;

    /// <summary>
    /// The preferred velocity of the agent determined by the chosen navigation system.
    /// </summary>
    [HideInInspector]
    public MASVec2 PrefVelocity;

    /// <summary>
    /// A list of destinations the agent will travel to during the simulation.
    /// </summary>
    //[HideInInspector]
    //public List<Vector3> GoalPositions = new List<Vector3>();

    /// <summary>
    /// An index of the current destination the agent is travelling to.
    /// </summary>
    [HideInInspector]
    public int GoalIndex = 0;

    /// <summary>
    /// Flag which determines if the agent should repeat travelling to its destinations.
    /// </summary>
    [HideInInspector]
    public bool LoopGoalPositions = LoopGoalPositionsDefault;    

    /// <summary>
    /// An optional character prefab associated (as a child) to the agent.
    /// </summary>
    [SerializeField, HideInInspector]
    private GameObject m_characterPrefab = null;

    /// <summary>
    /// Instantiated gameObject from the character prefab.
    /// </summary>
    [SerializeField]
    public GameObject m_character = null;
    [HideInInspector]
    public GameObject CharacterPrefab
    {
        get
        {
            return m_characterPrefab;
        }
        set
        {
            m_characterPrefab = value;

            if (m_character != null)
            {
                DestroyImmediate(m_character);
            }

            if (m_characterPrefab != null)
            {
                m_character = Instantiate(m_characterPrefab, transform);
                m_character.name = m_characterPrefab.name;
            }
        }
    }

    /// <summary>
    /// The path calculated by the NavMesh system.     
    /// </summary>    
    private NavMeshPath m_path;

    /// <summary>
    /// The NavMesh agent used to calcualte paths on the NavMesh.    
    /// </summary>    
    private NavMeshAgent m_navMeshAgent;

    /// <summary>
    /// The area mask that defines walkable regions.    
    /// </summary>    
    private int m_areaMask;

    public void Start()
    {
        // Setup the area mask and a path to its first destination.        
        m_path = new NavMeshPath();
        m_areaMask = 1 << NavMesh.GetAreaFromName("Walkable");        

        m_navMeshAgent = gameObject.AddComponent<NavMeshAgent>();
        m_navMeshAgent.areaMask = m_areaMask;
        m_navMeshAgent.radius = Radius;        
        m_navMeshAgent.baseOffset = -0.1f;

        MASGoalPositionHandle[] goalPositions = GetComponentsInChildren<MASGoalPositionHandle>();

        FixGoalPositions();

        if (goalPositions.Length > 0 && GoalIndex < (goalPositions.Length))
        {
            m_navMeshAgent.destination = goalPositions[GoalIndex].GoalPosition;
            NavMesh.CalculatePath(transform.position, goalPositions[GoalIndex].GoalPosition, m_areaMask, m_path);
        }

        m_navMeshAgent.angularSpeed = 0;        
    }

    public void Update()
    {
        if (ReachedGoal())
        {
            MASGoalPositionHandle[] goalPositions = GetComponentsInChildren<MASGoalPositionHandle>();
            if (goalPositions.Length > 0 && GoalIndex < goalPositions.Length - 1)
            {
                // Move to next destination and calculate new path
                ++GoalIndex;
                NavMesh.CalculatePath(transform.position, goalPositions[GoalIndex].GoalPosition, m_areaMask, m_path);
                m_navMeshAgent.destination = goalPositions[GoalIndex].GoalPosition;
            }
            else if (LoopGoalPositions && goalPositions.Length > 1)
            {
                // Agent looping: Loop back to first destination and calculate new path
                GoalIndex = 0;
                NavMesh.CalculatePath(transform.position, goalPositions[GoalIndex].GoalPosition, m_areaMask, m_path);
                m_navMeshAgent.destination = goalPositions[GoalIndex].GoalPosition;
            }
            else
            {
                // Reached final destination and not looping. Bring agent to complete stop.
                MASVec2 prefVel;
                prefVel.x = 0;
                prefVel.y = 0;
                MASPlugin.SetAgentPrefVelocity(AgentNo, prefVel);
                m_navMeshAgent.velocity = Vector3.zero;
            }
        }
        else
        {
            // Agent still moving towards destination. Update the simulation agent preferred velocity
            // with the calculated desired velocity from the NavMesh.
            Vector3 v = m_navMeshAgent.desiredVelocity;
            v.Normalize();
            MASVec2 prefVel;
            prefVel.x = v.x;
            prefVel.y = v.z;
            m_navMeshAgent.speed = MaxSpeed; // new Vector2(Velocity.x, Velocity.y).magnitude;
            MASPlugin.SetAgentPrefVelocity(AgentNo, prefVel);
        }

        // Rotate towards current destination
        Vector3 vel = new Vector3(Velocity.x, 0, Velocity.y);
        float step = MaxSpeed * 20.0f * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, vel, step, 0.0f);
        transform.rotation = Quaternion.LookRotation(newDir);
    }

    /// <summary>
    /// Check if an agent has reached its current goal.
    /// </summary>
    /// <returns>true if agent has reached current goal, false otherwise.</returns>
    public bool ReachedGoal()
    {
        MASGoalPositionHandle[] goalPositions = GetComponentsInChildren<MASGoalPositionHandle>();
        if (GoalIndex >= goalPositions.Length || goalPositions.Length <= 0)
        {
            return true;
        }

        var vec = transform.position - goalPositions[GoalIndex].GoalPosition;
        return ((vec.x * vec.x + vec.z * vec.z) < (Radius * Radius));
    }

#if UNITY_EDITOR
    /// <summary>
    /// Calculate the positions of the editor scene view position handles for the agent destinations.
    /// </summary>
    public void RecalculateGoalPositions(List<Vector3> goalPositions)
    {
        MASGoalPositionHandle[] handles = gameObject.GetComponents<MASGoalPositionHandle>();
        foreach (MASGoalPositionHandle handle in handles)
        {
            DestroyImmediate(handle);
        }

        for (int i = 0; i < goalPositions.Count; ++i)
        {
            var handle = gameObject.AddComponent<MASGoalPositionHandle>();
            handle.GoalPosition = goalPositions[i];
            handle.Index = i;
        }

        UnityEditor.SceneView.RepaintAll();
    }
#endif

#if UNITY_EDITOR
    /// <summary>
    /// Add a goal position.
    /// </summary>
    public void AddGoalPosition(Vector3 goalPosition)
    {
        MASGoalPositionHandle[] handles = gameObject.GetComponents<MASGoalPositionHandle>();
        int handleIndex = handles.Length;
        var handle = gameObject.AddComponent<MASGoalPositionHandle>();
        handle.GoalPosition = goalPosition;
        handle.Index = handleIndex;        

        UnityEditor.SceneView.RepaintAll();
    }
#endif

#if UNITY_EDITOR
    /// <summary>
    /// Remove a goal position.
    /// </summary>
    public void RemoveGoalPosition(int handleIndex)
    {
        MASGoalPositionHandle[] handles = gameObject.GetComponents<MASGoalPositionHandle>();
        foreach (var handle in handles)
        {
            if (handle.Index == handleIndex)
            {
                DestroyImmediate(handle);
                return;
            }
        }

        UnityEditor.SceneView.RepaintAll();
    }
#endif

#if UNITY_EDITOR
    /// <summary>
    /// Update a goal position.
    /// </summary>
    public void UpdateGoalPosition(int handleIndex, Vector3 goalPosition)
    {
        MASGoalPositionHandle[] handles = gameObject.GetComponents<MASGoalPositionHandle>();
        foreach (var handle in handles)
        {
            if (handle.Index == handleIndex)
            {
                handle.GoalPosition = goalPosition;
                return;
            }
        }

        UnityEditor.SceneView.RepaintAll();
    }
#endif

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);

        if (Application.isPlaying)
        {
            // Draw the path of a NavMesh agent
            for (int i = 0; i < m_path.corners.Length - 1; i++)
            {
                Debug.DrawLine(m_path.corners[i], m_path.corners[i + 1], new Color(0, 0, 1, 0.5f));
                Gizmos.color = new Color(0.0f, 1.0f, 0.0f);
                Gizmos.DrawSphere(m_path.corners[i], 0.05f);
            }
        }        

        // Draw gizmos to allow the agent cylinder to be selectable
        Gizmos.color = new Color(0, 0, 0, 0);
        Gizmos.DrawSphere(transform.position, Radius);
        Gizmos.DrawCube(new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z), new Vector3(Radius, 3, Radius));        

        // Draw the cylinder that represents the agent
        DebugExtension.DrawCylinder(transform.position, new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), new Color(0, 0.494f, 1), Radius);

        Gizmos.color = new Color(0, 0.494f, 1);
        Gizmos.DrawRay(new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), transform.forward * Radius * 1.5f);        
       
        DrawGizmos();
    }
#endif

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {        
        UnityEditor.Handles.color = new Color(0.8f, 0.494f, 0.7f, 0.1f);
        UnityEditor.Handles.DrawSolidDisc(transform.position, Vector3.up, Radius);
        UnityEditor.Handles.DrawSolidDisc(new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), Vector3.up, Radius);
        DebugExtension.DrawCylinder(transform.position, new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), new Color(0.8f, 0.494f, 1), Radius);

        MASGoalPositionHandle[] goalPositions = GetComponentsInChildren<MASGoalPositionHandle>();
        if (GoalIndex >= goalPositions.Length || goalPositions.Length <= 0)
        {
            return;
        }

        // Draw the destinations of the agents
        UnityEditor.Handles.color = Color.magenta;
        UnityEditor.Handles.DrawDottedLine(transform.position, goalPositions[GoalIndex].GoalPosition, 5.0f);
        for (int i = 0; i < goalPositions.Length; ++i)
        {
            UnityEditor.Handles.color = new Color(0.176f, 0.892f, 0.823f);
            UnityEditor.Handles.DrawSolidDisc(goalPositions[i].GoalPosition, Vector3.up, Radius);

            Gizmos.color = new Color(0, 0, 0, 0);
            Gizmos.DrawSphere(goalPositions[i].GoalPosition, Radius);

            if (i < goalPositions.Length - 1)
            {
                UnityEditor.Handles.color = Color.magenta;
                UnityEditor.Handles.DrawDottedLine(goalPositions[i].GoalPosition, goalPositions[i + 1].GoalPosition, 5.0f);
            }

            // Draw destination label
            GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
            labelStyle.normal.textColor = new Color(0.6f, 0.6f, 0.6f);
            labelStyle.fontStyle = FontStyle.Italic;
            Vector3 pos = goalPositions[i].GoalPosition;
            pos.x += 0.3f;
            pos.y += 0.6f;            
            UnityEditor.Handles.Label(pos, "(" + (i+1).ToString() + ")", labelStyle);
        }
    }
#endif

#if UNITY_EDITOR
    private void DrawGizmos()
    {
        if (!ReachedGoal())
        {
            Vector3 drawPos = new Vector3(transform.position.x, transform.position.y + 3, transform.position.z);

            // Draw the ray that represents the current velocity of the agent
            Vector3 vel = new Vector3();
            Gizmos.color = Color.red;
            vel.x = Velocity.x;
            vel.y = 0;
            vel.z = Velocity.y;
            Gizmos.DrawRay(drawPos, vel.normalized * Radius);

            // Draw the ray that represents the preferred velocity of the agent
            Vector3 prefVel = new Vector3();
            Gizmos.color = Color.green;
            prefVel.x = PrefVelocity.x;
            prefVel.y = 0;
            prefVel.z = PrefVelocity.y;
            Gizmos.DrawRay(drawPos, prefVel.normalized * Radius);

            // Draw an arc between actual vs preferrd velocity and set its colour to show how close
            // they are. green = good, red = bad.
            UnityEditor.Handles.color = new Color(0.176f, 0.592f, 0.823f, 0.1f);
            int sign = Vector3.Cross(prefVel, vel).y < 0 ? -1 : 1;
            float angle = Vector3.Angle(prefVel, vel);
            Color arcColor = MASUtils.BlendColours(Color.red, Color.green, Mathf.Abs(angle * (Mathf.PI / 180)) / Mathf.PI);
            arcColor.a = 1;
            UnityEditor.Handles.color = arcColor;
            UnityEditor.Handles.DrawSolidArc(drawPos, Vector3.up, prefVel, sign * angle, Radius);
        }
    }
#endif

    void FixGoalPositions()
    {
        MASGoalPositionHandle[] goalPositions = GetComponentsInChildren<MASGoalPositionHandle>();

        foreach (var goalPos in goalPositions)
        {
            NavMeshHit hit;
            if (NavMesh.SamplePosition(goalPos.GoalPosition, out hit, 10.0f, 1 << NavMesh.GetAreaFromName("Walkable")))
            {
                goalPos.GoalPosition = hit.position;
            }
        }
    }
}