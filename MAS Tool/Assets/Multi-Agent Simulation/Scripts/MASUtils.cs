﻿using UnityEngine;

/// <summary>
/// Provides some utility functions.
/// </summary>
public class MASUtils
{
    /// <summary>
    /// Blend between two provided colours relative to a blend amount.
    /// </summary>
    /// <param name="c1">The first colour.</param>
    /// <param name="c2">The second colour.</param>
    /// <param name="amount">The blend amount between 0 and 1.</param>
    /// <returns>Blended colour.</returns>
    public static Color BlendColours(Color c1, Color c2, float amount)
    {
        float r = ((c1.r * amount) + c2.r * (1 - amount));
        float g = ((c1.g * amount) + c2.g * (1 - amount));
        float b = ((c1.b * amount) + c2.b * (1 - amount));

        return new Color(r, g, b);
    }

    /// <summary>
    /// Calculate a position based on sunflower seed arrangement model.
    /// </summary>
    /// <see cref="http://demonstrations.wolfram.com/SunflowerSeedArrangements/"/>
    /// <param name="seedNumber">The seed number.</param>
    /// <param name="seedCount">The number of seeds.</param>
    /// <param name="offset">An multiplier offset x, y position calculation.</param>
    /// <returns>A 2D position.</returns>
    public static Vector2 SunflowerSeedPosition(int seedNumber, int seedCount, float offset)
    {
        float r = 1;
        if (seedNumber <= seedCount)
        {
            r = Mathf.Sqrt(seedNumber - 0.5f) / Mathf.Sqrt(seedCount - 1 / 2); ;
        }

        float theta = 2 * Mathf.PI * seedNumber / Mathf.Pow(GoldenRatio, 2);
        float x = offset * r * Mathf.Cos(theta);
        float y = offset * r * Mathf.Sin(theta);

        return new Vector3(x, y);
    }

    /// <summary>
    /// Golden ratio math constant, phi.
    /// </summary>
    private static readonly float GoldenRatio = (Mathf.Sqrt(5) + 1) / 2;
}
