﻿using UnityEngine;
using System.Collections;

public class MASRegion : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
        if (meshRenderer)
        {
            meshRenderer.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        transform.localPosition = Vector3.zero;

        Gizmos.color = Color.magenta;
        Gizmos.DrawWireCube(transform.position, new Vector3(transform.localScale.x, 1, transform.localScale.y));
    }
}
