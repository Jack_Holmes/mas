﻿/// <summary>
/// The simulation settings.
/// </summary>
public class MASSimulationSettings
{
    public const float TimeStepDefault = 0.1f;

    public float TimeStep { get; set; }
}
