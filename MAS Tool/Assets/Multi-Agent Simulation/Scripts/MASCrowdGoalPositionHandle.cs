﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// The position handle for a crowds destinations. This allows them to be
/// manipulated in the scene view.
/// </summary>
[ExecuteInEditMode]
public class MASCrowdGoalPositionHandle : MonoBehaviour
{
    /// <summary>
    /// The world space position of this handle.
    /// </summary>
    [HideInInspector]
    public Vector3 GoalPosition
    {
        get
        {
            return m_goalPosition;
        }
        set
        {
            m_goalPosition = value;
        }
    }

    [SerializeField]
    private Vector3 m_goalPosition = new Vector3(0.0f, 0.0f, 0.0f);

    /// <summary>
    /// The index of the current crowd destination.
    /// </summary>
    [HideInInspector]
    public int Index;

    /// <summary>
    /// The radius of the handle.
    /// </summary>
    [HideInInspector]
    public float Radius;

    /// <summary>
    /// Translate all agent destination editor handles that are associated with 
    /// this crowd destination handle.
    /// </summary>
    /// <param name="translation">The amount to translate by.</param>
    public virtual void Translate(Vector3 translation)
    {
        foreach (MASAgent agent in GetComponentsInChildren<MASAgent>())
        {
            foreach (MASGoalPositionHandle handle in agent.GetComponents<MASGoalPositionHandle>())
            {
                if (handle && handle.Index == Index)
                {                    
                    handle.Translate(translation);
                }
            }            
        }
    }

    /// <summary>
    /// Calculate the position of the editor handles based on the current crowd destination handle.
    /// </summary>
#if UNITY_EDITOR
    public void RecalculateAgentDestinationHandles()
    {
        MASCrowd crowd = GetComponent<MASCrowd>();
        if (!crowd)
        {
            return;
        }

        // Recalculate positions
        int agentIndex = 1;
        foreach (MASAgent agent in GetComponentsInChildren<MASAgent>())
        {
            // Calculate the agent position in the crowd
            float seedOffset = agent.Radius * crowd.Spread * 2;
            Vector2 seedPos = MASUtils.SunflowerSeedPosition(agentIndex, crowd.Density, seedOffset);
            Vector3 agentPos = new Vector3(seedPos.x + transform.position.x, transform.position.y, seedPos.y + transform.position.z);

            agent.transform.position = agentPos;

            // Recalculate goal positions
            MASCrowdGoalPositionHandle[] goalHandles = GetComponentsInChildren<MASCrowdGoalPositionHandle>();
            List<Vector3> goalPositions = new List<Vector3>();
            foreach (MASCrowdGoalPositionHandle handle in goalHandles)
            {
                goalPositions.Add(new Vector3(seedPos.x + handle.GoalPosition.x, handle.GoalPosition.y, seedPos.y + handle.GoalPosition.z));
            }

            agent.RecalculateGoalPositions(goalPositions);


            ++agentIndex;
        }
    }
#endif

#if UNITY_EDITOR
    public void OnDrawGizmosSelected()
    {
        UnityEditor.Handles.color = Color.yellow;
        UnityEditor.Handles.DrawWireDisc(GoalPosition, Vector3.up, Radius);
    }
#endif
}