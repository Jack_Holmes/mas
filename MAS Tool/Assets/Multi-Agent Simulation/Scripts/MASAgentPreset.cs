﻿using UnityEngine;

/// <summary>
/// The agent preset wraps an agent proxy in monobehavior so that it can be
/// saved into a prefab.
/// </summary>
[System.Serializable]
public class MASAgentPreset : MonoBehaviour
{    
    public MASAgentProxy AgentProxy = new MASAgentProxy();
}
