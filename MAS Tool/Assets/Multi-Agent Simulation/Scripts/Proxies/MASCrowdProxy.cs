﻿using UnityEngine;
using System.Collections.Generic;

public class MASCrowdProxy
{
    public Vector3 Position                 = new Vector3(0, 0, 0);
    public List<Vector3> GoalPositions      = new List<Vector3>();
    public bool LoopGoalPositions           = MASAgent.LoopGoalPositionsDefault;
    public int Density                      = 15;
    public float Spread                     = 5;
    public List<MASAgentProxy> AgentProxies = new List<MASAgentProxy>();
}
