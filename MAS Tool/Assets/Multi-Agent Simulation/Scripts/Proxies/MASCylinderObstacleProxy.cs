﻿using UnityEngine;

/// <summary>
/// Holds data necessary to represent a cylinder obstacle in the editor window.
/// </summary>
public class MASCylinderObstacleProxy
{
    public Vector3 Position = new Vector3(0, 0, 0);
    public Vector3 Size = new Vector3(1, 1, 1);
    public bool NavMeshBake = true;
};