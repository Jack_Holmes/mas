﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Holds data necessary to represent an agent in the editor window.
/// </summary>
[System.Serializable]
public class MASAgentProxy
{
    public bool ShowAdvancedSettings                 = false;
    public Vector3 Position                          = new Vector3(0, 0, 0);
    public List<Vector3> GoalPositions               = new List<Vector3>();
    public bool LoopGoalPositions                    = MASAgent.LoopGoalPositionsDefault;
    public float Radius                              = MASAgent.RadiusDefault;
    public float MaxSpeed                            = MASAgent.MaxSpeedDeault;
    public GameObject CharacterPrefab                = null;    
    public float NeighborDist                        = MASAgent.NeighborDistDefault;
    public int MaxNeighbors                          = MASAgent.MaxNeighborsDefault;
    public float TimeHorizon                         = MASAgent.TimeHorizonDefault;
    public float TimeHorizonObst                     = MASAgent.TimeHorizonObstDefault;
    public string PresetName                         = string.Empty; 
};