﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PerformanceTest : MonoBehaviour
{
    // UI
    public Dropdown TestTypeDropDown;
    public InputField NumberOfAgentsInput;
    public Dropdown GroupSizeDropDown;
    public Text ProgressText;
    public Text AverageText;
    public Toggle RenderCharacterToggle;
    public Button StartButton;
    public Button OpenButton;

    public GameObject CharacterPrefab;
    List<GameObject> m_characters;

#if UNITY_EDITOR
    public enum DebugMode
    {
        Off, 
        On
    }    
    public DebugMode debugMode = DebugMode.On;
#endif

    int m_numberOfAgents = 50;
    int m_circleTestRadius = 40;

    float m_agentRadius = 0.5f;

    List<System.UInt64> m_agents;
    List<Vector2> m_goals;

    const int RAND_MAX = 32767;

    bool m_runningTest = false;

    List<double> m_timings = new List<double>(2000);
    int m_testNumber = 1;
    int m_numberOfTests = 15;

    List<double> m_averages = new List<double>();
    string m_filePath;

    int m_groupLength = 3;
    float m_obstMin = 4.5f;
    float m_obstMax = 12.5f;

    private void Start()
    {
        MASPlugin.Initialise();
        MASPlugin.SetTimeStep(0.25f);

        m_agents = new List<System.UInt64>();
        m_goals = new List<Vector2>();
        m_characters = new List<GameObject>();
    }

    public void BeginPerformaceTest()
    {
        if (TestTypeDropDown.value == 0)
        {
            m_numberOfAgents = int.Parse(NumberOfAgentsInput.text);
            m_circleTestRadius = (int)(m_numberOfAgents * 0.8f);

            SetupCircleTest();
        }
        else if (TestTypeDropDown.value == 1)
        {
            m_numberOfAgents = int.Parse(NumberOfAgentsInput.text);

            if (GroupSizeDropDown.value == 0)
            {
                m_groupLength = 3;
            }
            else if (GroupSizeDropDown.value == 1)
            {
                m_groupLength = 5;
            }
            else if (GroupSizeDropDown.value == 2)
            {
                m_groupLength = 7;
            }

            SetupBlocksTest();
        }
        else
        {
            return;
        }

        ProgressText.gameObject.SetActive(true);
        ProgressText.text = "Running...";        
        StartButton.gameObject.SetActive(false);

        TestTypeDropDown.enabled = false;
        NumberOfAgentsInput.enabled = false;
        RenderCharacterToggle.enabled = false;
        
        m_runningTest = true;
    }

    void SetPreferredVelocities()
    {
        for (int i = 0; i < m_agents.Count; ++i)
        {
            MASVec2 pos = MASPlugin.GetAgentPosition(m_agents[i]);
            Vector2 agentPos = new Vector2(pos.x, pos.y);
            Vector2 agentGaol = m_goals[i];

            Vector2 goalVec = agentGaol - agentPos;
            if (goalVec.sqrMagnitude > 1)
            {
                goalVec.Normalize();
            }

            MASVec2 prefVel;
            prefVel.x = goalVec.x;
            prefVel.y = goalVec.y;
            MASPlugin.SetAgentPrefVelocity(m_agents[i], prefVel);

            float angle = Random.Range(0, RAND_MAX) * 2.0f * Mathf.PI / RAND_MAX;
            float dist = Random.Range(0, RAND_MAX) * 0.0001f / RAND_MAX;

            // Perturb a little to avoid deadlocks due to perfect symmetry.
            prefVel = MASPlugin.GetAgentPrefVelocity(m_agents[i]);
            Vector2 agentPrefVel = new Vector2(prefVel.x, prefVel.y);
            agentPrefVel = agentPrefVel + dist * new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
            prefVel.x = agentPrefVel.x;
            prefVel.y = agentPrefVel.y;

            MASPlugin.SetAgentPrefVelocity(m_agents[i], prefVel);

            if (RenderCharacterToggle.isOn)
            {
                m_characters[i].transform.position = new Vector3(pos.x, 0, pos.y);

                MASVec2 vel = MASPlugin.GetAgentVelocity(m_agents[i]);

                Vector2 characterVel = new Vector2(vel.x, vel.y);
                float speed = characterVel.magnitude;
                m_characters[i].GetComponent<Animator>().SetFloat("speed", speed);

                float step = 200.0f * Time.deltaTime;
                Vector3 newDir = Vector3.RotateTowards(transform.forward, new Vector3(m_goals[i].x, 0, m_goals[i].y), step, 0.0f);
                m_characters[i].transform.rotation = Quaternion.LookRotation(newDir);
            }
        }
    }

    // Check if all agents have reached their goal
    bool ReachedGoal()
    {
        for (int i = 0; i < m_agents.Count; ++i)
        {
            MASVec2 pos = MASPlugin.GetAgentPosition(m_agents[i]);
            Vector2 agentPos = new Vector2(pos.x, pos.y);
            Vector2 agentGaol = m_goals[i];

            if ((agentPos - agentGaol).sqrMagnitude > (m_agentRadius * m_agentRadius))
            {
                return false;
            }
        }

        return true;
    }

    void SetupCircleTest()
    {
        float angle = (2 * Mathf.PI) / m_numberOfAgents;
        for (int i = 0; i < m_numberOfAgents; ++i)
        {
            MASVec2 pos;
            pos.x = m_circleTestRadius * Mathf.Cos(i * angle);
            pos.y = m_circleTestRadius * Mathf.Sin(i * angle);

            MASVec2 vel;
            vel.x = 0;
            vel.y = 0;

            System.UInt64 agentNo = MASPlugin.AddAgentEx(pos, 15.0f, 10, 10.0f, 10.0f, m_agentRadius, 2.0f, vel);
            m_agents.Add(agentNo);

            m_goals.Add(new Vector2(-pos.x, -pos.y));

            if (RenderCharacterToggle.isOn)
            {
                GameObject agentObject = Instantiate(CharacterPrefab);
                agentObject.name = "agent";
                agentObject.transform.position = new Vector3(pos.x, 0, pos.y);
                m_characters.Add(agentObject);
            }
        }
    }

    void SetupBlocksTest()
    {
        MASVec2 vel;
        vel.x = 0;
        vel.y = 0;

        float offset = 15.0f;
        float spacing = 3.0f;

        for (int i = 0; i < m_groupLength; ++i)
        {
            for (int j = 0; j < m_groupLength; ++j)
            {
                MASVec2 pos;
                pos.x = offset + i * spacing;
                pos.y = offset + j * spacing;
                System.UInt64 agentNo = MASPlugin.AddAgentEx(pos, 15.0f, 10, 5.0f, 5.0f, m_agentRadius, 2.0f, vel);
                m_agents.Add(agentNo);
                m_goals.Add(new Vector2(-pos.x, -pos.y));
                if (RenderCharacterToggle.isOn)
                {
                    GameObject agentObject = Instantiate(CharacterPrefab);
                    agentObject.name = "agent";
                    agentObject.transform.position = new Vector3(pos.x, 0, pos.y);
                    m_characters.Add(agentObject);
                }

                pos.x = -offset - i * spacing;
                pos.y = offset + j * spacing;
                agentNo = MASPlugin.AddAgentEx(pos, 15.0f, 10, 5.0f, 5.0f, m_agentRadius, 2.0f, vel);
                m_agents.Add(agentNo);
                m_goals.Add(new Vector2(-pos.x, -pos.y));
                if (RenderCharacterToggle.isOn)
                {
                    GameObject agentObject = Instantiate(CharacterPrefab);
                    agentObject.name = "agent";
                    agentObject.transform.position = new Vector3(pos.x, 0, pos.y);
                    m_characters.Add(agentObject);
                }

                pos.x = offset + i * spacing;
                pos.y = -offset - j * spacing;
                agentNo = MASPlugin.AddAgentEx(pos, 15.0f, 10, 5.0f, 5.0f, m_agentRadius, 2.0f, vel);
                m_agents.Add(agentNo);
                m_goals.Add(new Vector2(-pos.x, -pos.y));
                if (RenderCharacterToggle.isOn)
                {
                    GameObject agentObject = Instantiate(CharacterPrefab);
                    agentObject.name = "agent";
                    agentObject.transform.position = new Vector3(pos.x, 0, pos.y);
                    m_characters.Add(agentObject);
                }

                pos.x = -offset - i * spacing;
                pos.y = -offset - j * spacing;
                agentNo = MASPlugin.AddAgentEx(pos, 15.0f, 10, 5.0f, 5.0f, m_agentRadius, 2.0f, vel);
                m_agents.Add(agentNo);
                m_goals.Add(new Vector2(-pos.x, -pos.y));
                if (RenderCharacterToggle.isOn)
                {
                    GameObject agentObject = Instantiate(CharacterPrefab);
                    agentObject.name = "agent";
                    agentObject.transform.position = new Vector3(pos.x, 0, pos.y);
                    m_characters.Add(agentObject);
                }
            }
        }

        float size = m_obstMax - m_obstMin;

        MASPlugin.BeginAddObstacle();         
        MASPlugin.AddObstacleVertex(-m_obstMin, m_obstMax);
        MASPlugin.AddObstacleVertex(-m_obstMax, m_obstMax);
        MASPlugin.AddObstacleVertex(-m_obstMax, m_obstMin);
        MASPlugin.AddObstacleVertex(-m_obstMin, m_obstMin);
        MASPlugin.EndAddObstacle();
        if (RenderCharacterToggle.isOn)
        {
            GameObject agentObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            agentObject.name = "block_obstacle";
            agentObject.transform.position = new Vector3(-(size / 2) - m_obstMin, 2.5f, (size / 2) + m_obstMin);
            agentObject.transform.localScale = new Vector3(size, 5, size);
            m_characters.Add(agentObject);
        }

        MASPlugin.BeginAddObstacle();
        MASPlugin.AddObstacleVertex(m_obstMin, m_obstMax);
        MASPlugin.AddObstacleVertex(m_obstMin, m_obstMin);
        MASPlugin.AddObstacleVertex(m_obstMax, m_obstMin);
        MASPlugin.AddObstacleVertex(m_obstMax, m_obstMax);
        MASPlugin.EndAddObstacle();
        if (RenderCharacterToggle.isOn)
        {
            GameObject agentObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            agentObject.name = "block_obstacle";
            agentObject.transform.position = new Vector3((size / 2) + m_obstMin, 2.5f, (size / 2) + m_obstMin);
            agentObject.transform.localScale = new Vector3(size, 5, size);
            m_characters.Add(agentObject);
        }

        MASPlugin.BeginAddObstacle();
        MASPlugin.AddObstacleVertex(m_obstMin, -m_obstMax);
        MASPlugin.AddObstacleVertex(m_obstMax, -m_obstMax);
        MASPlugin.AddObstacleVertex(m_obstMax, -m_obstMin);
        MASPlugin.AddObstacleVertex(m_obstMin, -m_obstMin);
        MASPlugin.EndAddObstacle();
        if (RenderCharacterToggle.isOn)
        {
            GameObject agentObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            agentObject.name = "block_obstacle";
            agentObject.transform.position = new Vector3((size / 2) + m_obstMin, 2.5f, -(size / 2) - m_obstMin);
            agentObject.transform.localScale = new Vector3(size, 5, size);
            m_characters.Add(agentObject);
        }

        MASPlugin.BeginAddObstacle();
        MASPlugin.AddObstacleVertex(-m_obstMin, -m_obstMax);
        MASPlugin.AddObstacleVertex(-m_obstMin, -m_obstMin);
        MASPlugin.AddObstacleVertex(-m_obstMax, -m_obstMin);
        MASPlugin.AddObstacleVertex(-m_obstMax, -m_obstMax);
        MASPlugin.EndAddObstacle();
        if (RenderCharacterToggle.isOn)
        {
            GameObject agentObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            agentObject.name = "block_obstacle";
            agentObject.transform.position = new Vector3(-(size / 2) - m_obstMin, 2.5f, -(size / 2) - m_obstMin);
            agentObject.transform.localScale = new Vector3(size, 5, size);
            m_characters.Add(agentObject);
        }

        MASPlugin.ProcessObstacles();
    }

    private void Update()
    {
        if (!m_runningTest)
        {
            return;
        } 

        if (!ReachedGoal())
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            MASPlugin.StepSimulation();
            SetPreferredVelocities();
            stopwatch.Stop();
            m_timings.Add(stopwatch.Elapsed.TotalMilliseconds);
        }
        else if (m_testNumber <= m_numberOfTests)
        {
            // Completed single test -> Get average and start the next next
            ProgressText.text = "Test [" + m_testNumber + "/" + m_numberOfTests + "] Complete!";

            double total = 0;
            foreach (var timing in m_timings)
	        {
                total += timing;
            }

            m_averages.Add(total / m_timings.Count);

            MASPlugin.Shutdown();
            MASPlugin.Initialise();
            MASPlugin.SetTimeStep(0.25f);

            m_agents = new List<System.UInt64>();
            m_goals = new List<Vector2>();

            foreach (var character in m_characters)
            {
                Destroy(character);
            }
            m_characters = new List<GameObject>();

            if (TestTypeDropDown.value == 0)
            {
                SetupCircleTest();
            }
            else if (TestTypeDropDown.value == 1)
            {
                SetupBlocksTest();
            }

            ++m_testNumber;

            // Completed all tests -> Write the results to file
            if (m_testNumber >= (m_numberOfTests + 1))
            {
                m_runningTest = false;

                OnAllTestsCompleted();        
            }
        }        
    }

    void OnAllTestsCompleted()
    {
        if (TestTypeDropDown.value == 0)
        {
            m_filePath = Application.dataPath
                + "/N" + m_numberOfAgents + "_"
                + "R" + m_circleTestRadius + "_"
                + (RenderCharacterToggle.isOn ? "RenderCharacters_" : "")
                + System.String.Format("{0:d-M-yyyy}", System.DateTime.Today)
                + ".csv";
        }
        else
        {
            m_filePath = Application.dataPath
                + "/N" + (4 * m_groupLength * m_groupLength) + "_"
                + "G" + (m_groupLength * m_groupLength) + "_"
                + (RenderCharacterToggle.isOn ? "RenderCharacters_" : "")
                + System.String.Format("{0:d-M-yyyy}", System.DateTime.Today)
                + ".csv";
        }

        ProgressText.text = "Results output to: " + m_filePath;

        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(m_filePath, false))
        {
            sw.WriteLine("Test No,Avg. Time Per Step");
            for (int i = 0; i < m_numberOfTests; ++i)
            {
                sw.WriteLine((i + 1).ToString() + "," + m_averages[i]);
            }
        }

        double total = 0;
        double average = 0;
        foreach (var avg in m_averages)
        {
            total += avg;
        }
        average = total / m_averages.Count;
        AverageText.gameObject.SetActive(true);
        AverageText.text = "Avg. time per step for " + m_numberOfAgents.ToString() + " agents:\n" + average.ToString() + "ms\n";

        OpenButton.gameObject.SetActive(true);
    }

    public void OnTestTypeChanged()
    {
        if (TestTypeDropDown.value == 0)
        {
            NumberOfAgentsInput.transform.parent.gameObject.SetActive(true);
            GroupSizeDropDown.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            NumberOfAgentsInput.transform.parent.gameObject.SetActive(false);
            GroupSizeDropDown.transform.parent.gameObject.SetActive(true);
        }
    }

    public void OnOpenClicked()
    {
        if (m_filePath == null || m_filePath == string.Empty)
        {
            return;
        }

        System.Diagnostics.Process.Start(m_filePath);
    }

    private void OnApplicationQuit()
    {
        MASPlugin.Shutdown();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!Application.isPlaying)
        {
            return;
        }

        if (debugMode == DebugMode.Off)
        {
            return;
        }

        for (int i = 0; i < m_agents.Count; ++i)
        {
            MASVec2 pos = MASPlugin.GetAgentPosition(m_agents[i]);

            float r = 0.5f + ((float)i / m_agents.Count) * 0.5f;
            UnityEditor.Handles.color = new Color(r, 0, 0);
            UnityEditor.Handles.DrawSolidDisc(new Vector3(pos.x, 0, pos.y), Vector3.up, m_agentRadius);
        }


         Vector3[] obst1 = new Vector3[4] {
            new Vector3(-m_obstMin, 0.0f, m_obstMax),
            new Vector3(-m_obstMax, 0.0f, m_obstMax),
            new Vector3(-m_obstMax, 0.0f, m_obstMin),
            new Vector3(-m_obstMin, 0.0f, m_obstMin)
        };

        Vector3[] obst2 = new Vector3[4] {
            new Vector3(m_obstMin, 0.0f, m_obstMax),
            new Vector3(m_obstMin, 0.0f, m_obstMin),
            new Vector3(m_obstMax, 0.0f, m_obstMin),
            new Vector3(m_obstMax, 0.0f, m_obstMax)
        };


        Vector3[] obst3 = new Vector3[4] {
            new Vector3(m_obstMin, 0.0f, -m_obstMax),
            new Vector3(m_obstMax, 0.0f, -m_obstMax),
            new Vector3(m_obstMax, 0.0f, -m_obstMin),
            new Vector3(m_obstMin, 0.0f, -m_obstMin)
        };
        
        Vector3[] obst4 = new Vector3[4] {
            new Vector3(-m_obstMin, 0.0f, -m_obstMax),
            new Vector3(-m_obstMin, 0.0f, -m_obstMin),
            new Vector3(-m_obstMax, 0.0f, -m_obstMin),
            new Vector3(-m_obstMax, 0.0f, -m_obstMax)
        };

        UnityEditor.Handles.color = Color.white;
        UnityEditor.Handles.DrawAAConvexPolygon(obst1);
        UnityEditor.Handles.DrawAAConvexPolygon(obst2);
        UnityEditor.Handles.DrawAAConvexPolygon(obst3);
        UnityEditor.Handles.DrawAAConvexPolygon(obst4);
    }
#endif
}
