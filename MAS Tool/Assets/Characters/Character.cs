﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

	private Animator m_animator;

	// Use this for initialization
	void Start () {
		m_animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		MASAgent agent = GetComponentInParent<MASAgent> ();
		if (agent)
		{
			Vector2 vel = new Vector2 (agent.Velocity.x, agent.Velocity.y);
			float speed = vel.magnitude;
			m_animator.SetFloat ("speed", speed);		
		}
	}
}
