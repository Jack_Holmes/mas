#pragma once

#include <cstddef>

#define MAS_PLUGIN_API __declspec(dllexport)
#define MAS_CALL_TYPE __cdecl

extern "C"
{
	typedef struct MAS_Vec2
	{
		float x;
		float y;
	} MAS_Vec2;

	typedef std::size_t AgentNo;

	MAS_PLUGIN_API void MAS_CALL_TYPE MAS_Initialise();

	MAS_PLUGIN_API void MAS_CALL_TYPE MAS_Shutdown();

	MAS_PLUGIN_API void MAS_CALL_TYPE MAS_StepSimulation();	

	MAS_PLUGIN_API AgentNo MAS_CALL_TYPE MAS_AddAgentEx(
		MAS_Vec2 position,
		float neighborDist,
		int maxNeighbors,
		float timeHorizon,
		float timeHorizonObst,
		float radius,
		float maxSpeed,
		MAS_Vec2 velocity);	

	MAS_PLUGIN_API size_t MAS_CALL_TYPE MAS_GetNumAgents();

	MAS_PLUGIN_API MAS_Vec2 MAS_CALL_TYPE MAS_GetAgentPosition(AgentNo agentNo);

	MAS_PLUGIN_API MAS_Vec2 MAS_CALL_TYPE MAS_GetAgentVelocity(AgentNo agentNo);

	MAS_PLUGIN_API void MAS_CALL_TYPE MAS_SetAgentPrefVelocity(AgentNo agentNo, MAS_Vec2 prefVelocity);

	MAS_PLUGIN_API void MAS_CALL_TYPE MAS_SetAgentVelocity(AgentNo agentNo, MAS_Vec2 velocity);

	MAS_PLUGIN_API MAS_Vec2 MAS_CALL_TYPE MAS_GetAgentPrefVelocity(AgentNo agentNo);

	MAS_PLUGIN_API void MAS_CALL_TYPE MAS_SetTimeStep(float timeStep);

	MAS_PLUGIN_API void MAS_CALL_TYPE MAS_BeginAddObstacle();

	MAS_PLUGIN_API void MAS_CALL_TYPE MAS_AddObstacleVertex(float x, float y);

	MAS_PLUGIN_API void MAS_CALL_TYPE MAS_EndAddObstacle();

	MAS_PLUGIN_API void MAS_CALL_TYPE MAS_ProcessObstacles();	

	MAS_PLUGIN_API bool MAS_CALL_TYPE MAS_QueryVisibility(MAS_Vec2 point1, MAS_Vec2 point2, float radius = 0.0f);
}
