#include "MASPlugin.h"
#include "../External/RVO2-2.0.2/src/RVO.h"
#include <memory>
#include <map>
#include <vector>
#include <algorithm>

#ifndef M_PI
const float M_PI = 3.14159265358979323846f;
#endif

namespace MASPlugin
{
	RVO::RVOSimulator* simulator = nullptr;

	std::vector<RVO::Vector2> vertices;

	RVO::Vector2 ConvertVector(const MAS_Vec2& vec)
	{
		return RVO::Vector2(vec.x, vec.y);
	}

	MAS_Vec2 ConvertVector(const RVO::Vector2& vec)
	{
		MAS_Vec2 v;
		v.x = vec.x();
		v.y = vec.y();
		return v;
	}
}

using namespace MASPlugin;

MAS_PLUGIN_API void MAS_CALL_TYPE MAS_Initialise()
{
	simulator = new RVO::RVOSimulator();
	simulator->setTimeStep(0.125f);
	simulator->setAgentDefaults(15.0f, 10, 5.0f, 5.0f, 2.0f, 2.0f);
}

MAS_PLUGIN_API void MAS_CALL_TYPE MAS_Shutdown()
{
	if (simulator)
	{
		delete simulator;
		simulator = nullptr;
	}
}

MAS_PLUGIN_API void MAS_CALL_TYPE MAS_StepSimulation()
{
	simulator->doStep();
}

MAS_PLUGIN_API AgentNo MAS_CALL_TYPE MAS_AddAgentEx(
	MAS_Vec2 position,
	float neighborDist,
	int maxNeighbors,
	float timeHorizon,
	float timeHorizonObst,
	float radius,
	float maxSpeed,
	MAS_Vec2 velocity)
{
	AgentNo agentNo = simulator->addAgent(
		ConvertVector(position),
		neighborDist,
		maxNeighbors,
		timeHorizon,
		timeHorizonObst,
		radius,
		maxSpeed,
		ConvertVector(velocity));

	return agentNo;
}

MAS_PLUGIN_API size_t MAS_CALL_TYPE MAS_GetNumAgents()
{
	return simulator->getNumAgents();
}

MAS_PLUGIN_API MAS_Vec2 MAS_CALL_TYPE MAS_GetAgentPosition(AgentNo agentNo)
{
	auto pos = simulator->getAgentPosition(agentNo);
	return ConvertVector(pos);
}

MAS_PLUGIN_API MAS_Vec2 MAS_CALL_TYPE MAS_GetAgentVelocity(AgentNo agentNo)
{
	auto vel = simulator->getAgentVelocity(agentNo);
	return ConvertVector(vel);
}

MAS_PLUGIN_API void MAS_CALL_TYPE MAS_SetAgentPrefVelocity(
	AgentNo agentNo,
	MAS_Vec2 prefVelocity)
{
	simulator->setAgentPrefVelocity(agentNo, ConvertVector(prefVelocity));
}

MAS_PLUGIN_API void MAS_CALL_TYPE MAS_SetAgentVelocity(
	AgentNo agentNo,
	MAS_Vec2 velocity)
{
	simulator->setAgentVelocity(agentNo, ConvertVector(velocity));
}

MAS_PLUGIN_API MAS_Vec2 MAS_CALL_TYPE MAS_GetAgentPrefVelocity(AgentNo agentNo)
{
	auto vel = simulator->getAgentPrefVelocity(agentNo);
	return ConvertVector(vel);
}

MAS_PLUGIN_API void MAS_CALL_TYPE MAS_SetTimeStep(float timeStep)
{
	simulator->setTimeStep(timeStep);
}

MAS_PLUGIN_API void MAS_CALL_TYPE MAS_BeginAddObstacle()
{
	vertices.clear();
}

MAS_PLUGIN_API void MAS_CALL_TYPE MAS_AddObstacleVertex(float x, float y)
{
	vertices.push_back(RVO::Vector2(x, y));
}

MAS_PLUGIN_API void MAS_CALL_TYPE MAS_EndAddObstacle()
{
	simulator->addObstacle(vertices);
	vertices.clear();
}

MAS_PLUGIN_API void MAS_CALL_TYPE MAS_ProcessObstacles()
{
	simulator->processObstacles();
}

MAS_PLUGIN_API bool MAS_CALL_TYPE MAS_QueryVisibility(MAS_Vec2 point1, MAS_Vec2 point2, float radius)
{
	return simulator->queryVisibility(
		ConvertVector(point1),
		ConvertVector(point2),
		radius);
}