#include <cmath>
#include <cstdlib>

#include <vector>


#include <iostream>
#include <ctime>
#include <chrono>
#include <string>
#include <fstream>

#if _OPENMP
#include <omp.h>
#endif

#include "../../External/RVO2-2.0.2/src/RVO.h"

#ifndef M_PI
const float M_PI = 3.14159265358979323846f;
#endif

/* Store the goals of the agents. */
std::vector<RVO::Vector2> goals;

void SetupBlocksTest(RVO::RVOSimulator *sim, int groupLength)
{
	std::srand(static_cast<unsigned int>(std::time(NULL)));

	sim->setTimeStep(0.25f);
	sim->setAgentDefaults(15.0f, 10, 5.0f, 5.0f, 0.5f, 2.0f);
	
	float offset = 15.0f;
	float spacing = 3.0f;
	for (size_t i = 0; i < groupLength; ++i) {
		for (size_t j = 0; j < groupLength; ++j) {
			float x = offset + i * spacing;
			float y = offset + j * spacing;
			sim->addAgent(RVO::Vector2(x, y));
			goals.push_back(RVO::Vector2(-x, -y));

			x = -offset - i * spacing;
			y = offset + j * spacing;		
			sim->addAgent(RVO::Vector2(x, y));
			goals.push_back(RVO::Vector2(-x, -y));

			x = offset + i * spacing;
			y = -offset - j * spacing;		
			sim->addAgent(RVO::Vector2(x, y));
			goals.push_back(RVO::Vector2(-x, -y));

			x = -offset - i * spacing;
			y = -offset - j * spacing;			
			sim->addAgent(RVO::Vector2(x, y));
			goals.push_back(RVO::Vector2(-x, -y));
		}
	}

	float obstMin = 4.5f;
	float obstMax = 12.5f;

	std::vector<RVO::Vector2> obstacle1, obstacle2, obstacle3, obstacle4;

	obstacle1.push_back(RVO::Vector2(-obstMin, obstMax));
	obstacle1.push_back(RVO::Vector2(-obstMax, obstMax));
	obstacle1.push_back(RVO::Vector2(-obstMax, obstMin));
	obstacle1.push_back(RVO::Vector2(-obstMin, obstMin));

	obstacle2.push_back(RVO::Vector2(obstMin, obstMax));
	obstacle2.push_back(RVO::Vector2(obstMin, obstMin));
	obstacle2.push_back(RVO::Vector2(obstMax, obstMin));
	obstacle2.push_back(RVO::Vector2(obstMax, obstMax));

	obstacle3.push_back(RVO::Vector2(obstMin, -obstMax));
	obstacle3.push_back(RVO::Vector2(obstMax, -obstMax));
	obstacle3.push_back(RVO::Vector2(obstMax, -obstMin));
	obstacle3.push_back(RVO::Vector2(obstMin, -obstMin));

	obstacle4.push_back(RVO::Vector2(-obstMin, -obstMax));
	obstacle4.push_back(RVO::Vector2(-obstMin, -obstMin));
	obstacle4.push_back(RVO::Vector2(-obstMax, -obstMin));
	obstacle4.push_back(RVO::Vector2(-obstMax, -obstMax));

	sim->addObstacle(obstacle1);
	sim->addObstacle(obstacle2);
	sim->addObstacle(obstacle3);
	sim->addObstacle(obstacle4);
	
	sim->processObstacles();
}

void SetPreferredVelocities(RVO::RVOSimulator *sim)
{
	/*
	* Set the preferred velocity to be a vector of unit magnitude (speed) in the
	* direction of the goal.
	*/
#ifdef _OPENMP
#pragma omp parallel for
#endif
	for (int i = 0; i < static_cast<int>(sim->getNumAgents()); ++i) {
		RVO::Vector2 goalVector = goals[i] - sim->getAgentPosition(i);

		if (RVO::absSq(goalVector) > 1.0f) {
			goalVector = RVO::normalize(goalVector);
		}

		sim->setAgentPrefVelocity(i, goalVector);

		/*
		* Perturb a little to avoid deadlocks due to perfect symmetry.
		*/
		float angle = std::rand() * 2.0f * M_PI / RAND_MAX;
		float dist = std::rand() * 0.0001f / RAND_MAX;

		sim->setAgentPrefVelocity(i, sim->getAgentPrefVelocity(i) +
			dist * RVO::Vector2(std::cos(angle), std::sin(angle)));
	}
}

bool ReachedGoal(RVO::RVOSimulator *sim)
{
	/* Check if all agents have reached their goals. */
	for (size_t i = 0; i < sim->getNumAgents(); ++i) {
		if (RVO::absSq(sim->getAgentPosition(i) - goals[i]) > 20.0f * 20.0f) {
			return false;
		}
	}

	return true;
}

using Clock = std::chrono::high_resolution_clock;
std::vector<double> averages;

void PerformBlocksTest(int groupLength)
{
	/* Create a new simulator instance. */
	RVO::RVOSimulator *sim = new RVO::RVOSimulator();

	/* Set up the scenario. */
	SetupBlocksTest(sim, groupLength);

	std::vector<double> timings;

	/* Perform (and manipulate) the simulation. */
	do
	{
		auto clock1 = Clock::now();
		SetPreferredVelocities(sim);
		sim->doStep();
		auto clock2 = Clock::now();
		auto duration = std::chrono::duration<double, std::milli>(clock2 - clock1).count();
		timings.push_back(duration);
	} while (!ReachedGoal(sim));

	double total = 0;
	for (const auto timing : timings)
	{
		total += timing;
	}

	averages.push_back(total / timings.size());

	delete sim;
}

int main()
{
	const int timesToRunTest = 15;
	int groupLength = 3;

	std::cout << "Enter group length: ";
	std::cin >> groupLength;

	time_t t = time(0);
	struct tm * now = localtime(&t);
	std::string fileName =
		"N" + std::to_string(4 * groupLength * groupLength) + "_"
		+ "G" + std::to_string(groupLength * groupLength) + "_"
		+ std::to_string(now->tm_mday) + "-"
		+ std::to_string(now->tm_mon + 1) + "-"
		+ std::to_string(now->tm_year + 1900) +
		+".csv";

	std::ofstream file(fileName);
	file << "Test No,Avg. Time Per Step\n";

	for (int i = 0; i < timesToRunTest; ++i)
	{
		PerformBlocksTest(groupLength);
		std::cout << "Test [" << (i + 1) << "/" << timesToRunTest << "] Complete! ~ Avg. time per step: " << averages[i] << "ms" << std::endl;
		file << (i + 1) << "," << averages[i] << "\n";
	}

	file.close();

	double total = 0;
	double average = 0;
	for (const auto avg : averages)
	{
		total += avg;
	}
	average = total / averages.size();
	std::cout << "Overall Avg. time per step for " << (groupLength * groupLength) << " agents: " << average << "ms\n";

	std::cin.get();
	return 0;
}
