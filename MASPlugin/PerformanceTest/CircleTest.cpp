//#include <cmath>
//#include <cstddef>
//#include <vector>
//
//#include <iostream>
//#include <chrono>
//#include <fstream>
//#include <ctime>
//#include <string>
//
//#if _OPENMP
//#include <omp.h>
//#endif
//
//#include "../../External/RVO2-2.0.2/src/RVO.h"
//
//#ifndef M_PI
//const float M_PI = 3.14159265358979323846f;
//#endif
//
///* Store the goals of the agents. */
//std::vector<RVO::Vector2> goals;
//
//void SetupCircleTest(RVO::RVOSimulator *sim, int numberOfAgents, int circleTestRadius)
//{
//	// Setup the simulation parameters
//	sim->setTimeStep(0.25f);
//	sim->setAgentDefaults(15.0f, 10, 10.0f, 10.0f, 0.5f, 2.0f);
//
//	// Set agent positions
//	float x = 0;
//	float y = 0;
//	float angle = (2 * M_PI) / numberOfAgents;
//	for (int i = 0; i < numberOfAgents; ++i)
//	{
//		x = circleTestRadius * std::cos(i * angle);
//		y = circleTestRadius * std::sin(i * angle);
//		sim->addAgent(RVO::Vector2(x, y));
//		goals.push_back(-sim->getAgentPosition(i));
//	}
//}
//
//void SetPreferredVelocities(RVO::RVOSimulator *sim)
//{
//	/*
//	* Set the preferred velocity to be a vector of unit magnitude (speed) in the
//	* direction of the goal.
//	*/
//#ifdef _OPENMP
//#pragma omp parallel for
//#endif
//	for (int i = 0; i < static_cast<int>(sim->getNumAgents()); ++i) {
//		RVO::Vector2 goalVector = goals[i] - sim->getAgentPosition(i);
//
//		if (RVO::absSq(goalVector) > 1.0f) {
//			goalVector = RVO::normalize(goalVector);
//		}
//
//		sim->setAgentPrefVelocity(i, goalVector);
//
//		/*
//		* Perturb a little to avoid deadlocks due to perfect symmetry.
//		*/
//		float angle = std::rand() * 2.0f * M_PI / RAND_MAX;
//		float dist = std::rand() * 0.0001f / RAND_MAX;
//
//		sim->setAgentPrefVelocity(i, sim->getAgentPrefVelocity(i) +
//			dist * RVO::Vector2(std::cos(angle), std::sin(angle)));
//	}
//}
//
//bool ReachedGoal(RVO::RVOSimulator *sim)
//{
//	/* Check if all agents have reached their goals. */
//	for (size_t i = 0; i < sim->getNumAgents(); ++i) {
//		if (RVO::absSq(sim->getAgentPosition(i) - goals[i]) > sim->getAgentRadius(i) * sim->getAgentRadius(i)) {
//			return false;
//		}
//	}
//
//	return true;
//}
//
//using Clock = std::chrono::high_resolution_clock;
//std::vector<double> averages;
//
//void PerformCircleTest(int numberOfAgents, int circleTestRadius)
//{
//	/* Create a new simulator instance. */
//	RVO::RVOSimulator *sim = new RVO::RVOSimulator();
//
//	/* Set up the scenario. */
//	SetupCircleTest(sim, numberOfAgents, circleTestRadius);
//
//	std::vector<double> timings;
//
//	/* Perform (and manipulate) the simulation. */
//	do
//	{
//		auto clock1 = Clock::now();
//		SetPreferredVelocities(sim);		
//		sim->doStep();
//		auto clock2 = Clock::now();
//		auto duration = std::chrono::duration<double, std::milli>(clock2 - clock1).count();
//		timings.push_back(duration);
//	} while (!ReachedGoal(sim));
//
//	double total = 0;
//	for (const auto timing : timings)
//	{
//		total += timing;
//	}
//
//	averages.push_back(total / timings.size());
//
//	delete sim;
//}
//
//int main()
//{
//	const int timesToRunTest = 15;
//	int numberOfAgents = 25;
//
//	std::cout << "Enter number of agents: ";
//	std::cin >> numberOfAgents;
//
//	int circleTestRadius = numberOfAgents * 0.8f;
//
//	time_t t = time(0);
//	struct tm * now = localtime(&t);
//	std::string fileName = 
//		"N" + std::to_string(numberOfAgents) + "_"
//		+ "R" + std::to_string(circleTestRadius) + "_"
//		+ std::to_string(now->tm_mday) + "-"
//		+ std::to_string(now->tm_mon + 1) + "-"
//		+ std::to_string(now->tm_year + 1900) +
//		+ ".csv";
//
//	std::ofstream file(fileName);
//	file << "Test No,Avg. Time Per Step\n";
//	
//	for (int i = 0; i < timesToRunTest; ++i)
//	{	
//		PerformCircleTest(numberOfAgents, circleTestRadius);
//		std::cout << "Test [" << (i + 1) << "/" << timesToRunTest << "] Complete! ~ Avg. time per step: " << averages[i]  << "ms" << std::endl;
//		file << (i + 1) << "," << averages[i] << "\n";
//	}
//
//	file.close();
//
//	double total = 0;
//	double average = 0;
//	for (const auto avg : averages)
//	{
//		total += avg;
//	}
//	average = total / averages.size();
//	std::cout << "Overall Avg. time per step for " << numberOfAgents << " agents: " << average << "ms\n";
//
//	std::cin.get();
//	return 0;
//}
